package com.techelevator;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.model.Skill;
import com.techelevator.model.JDBC.JDBCSkillDAO;

public class JDBCSkillDAOIntegrationTest extends DAOIntegrationTest {
	
	private JDBCSkillDAO skillDAO;
	private JdbcTemplate jdbcTemplate;
	
	@Before
	public void setup() {
		skillDAO = new JDBCSkillDAO(super.getDataSource());
		jdbcTemplate = new JdbcTemplate(super.getDataSource());
		createFakeGuardianUser();
		createFakeKidUser();
		createFakeSkill();
	}
	
	@Test
	public void get_all_skills_returns_values() {
		List<Skill> skillList = skillDAO.getAllSkills();
		assertTrue(skillList.size() > 0);
	}
	
	@Test
	public void saving_and_getting_skills_by_kid_id_returns_a_list() {
		skillDAO.addSkillBySkillNameKidId("TESTTESTTEST", 999999);
		List<Skill> skillList = skillDAO.getSkillsByKidId(999999);
		assertTrue(skillList.size() > 0);
	}
	
	@Test
	public void ensure_saveNewSkill_correctly_inserts_new_skill() {
		skillDAO.saveNewSkill("TESTSKILL");
		List<Skill> skills = skillDAO.getAllSkills();
		Boolean result = false;
		for (Skill s : skills) {
			if (s.getName().equals("TESTSKILL")) {
				result = true;
			}
		}
		assertTrue(result);
	}
	
	private void createFakeGuardianUser() {
		String sql = "INSERT INTO app_user VALUES(9999999, 'TESTUSER2', 'TESTpassword', 'guardian', 'TESTSALT'); INSERT INTO guardian_users VALUES(9999999, 9999999, 'TEST', 'TEST', '1970-12-12', 'email@example.com');";
		jdbcTemplate.update(sql);
	}
	
	private void createFakeKidUser() {
		String sql = "INSERT INTO app_user VALUES(999999, 'TESTUSER', 'TESTpassword', 'child', 'TESTSALT'); INSERT INTO kid_users VALUES(999999, 999999, 9999999, 'TEST', 'TEST', '2000-01-01', 'TEST BUSINESS', 'logo.png', 'summary', 'email@example.com');";
		jdbcTemplate.update(sql);
	}
	
	private void createFakeSkill() {
		String sql = "INSERT INTO skills VALUES(default, 'TESTTESTTEST')";
		jdbcTemplate.update(sql);
	}
	
}
