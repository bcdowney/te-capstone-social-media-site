package com.techelevator;

import org.junit.*;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.model.Kid;
import com.techelevator.model.JDBC.JDBCKidDAO;

public class JDBCKidDAOIntegrationTest extends DAOIntegrationTest{
	
	private JDBCKidDAO kidDAO;
	private JdbcTemplate jdbcTemplate;
	
	@Before
	public void setup() {
		kidDAO = new JDBCKidDAO(getDataSource());
		jdbcTemplate = new JdbcTemplate(getDataSource());
		createTestKid();
	}
	
	@Test
	public void testCanGetKidByUserId() {
		Kid kid = kidDAO.getKidByUserId(999999);
		Assert.assertNotNull(kid);
		Assert.assertEquals("Did not pull first name correctly", "Testy", kid.getFirstName());
		Assert.assertEquals("Did not pull last name correctly", "McTest Jr", kid.getLastName());
	}
	
	@Test
	public void testCanGetKidByKidId() {
		Kid kid = kidDAO.getKidByKidId(999999);
		Assert.assertNotNull(kid);
		Assert.assertEquals("Did not pull first name correctly", "Testy", kid.getFirstName());
		Assert.assertEquals("Did not pull last name correctly", "McTest Jr", kid.getLastName());
	}
	
	@Test
	public void test_get_transactions_per_day_limit_by_kid_id() {
		Kid kid = kidDAO.getKidByKidId(999999);
		int perDayLimit = kidDAO.getTransactionsPerDayLimitByKidId(kid.getKidId());
		Assert.assertEquals("Did not pull transaction per day limit as expected", 5, perDayLimit);
	}
	
	@Test
	public void test_get_transaction_amount_limit_by_kid_id() {
		Kid kid = kidDAO.getKidByKidId(999999);
		double amountLimit = kidDAO.getTransactionAmountLimitByKidId(kid.getKidId());
		Assert.assertTrue("Did not return correct transaction amount limit", amountLimit == 5.0);
	}
	
	private void createTestKid() {
		String kidSql = "INSERT INTO kid_users (user_id, kid_id, guardian_id, first_name, "
				+ "last_name, date_of_birth, business_name, email, transactions_per_day_limit, transaction_amount_limit) VALUES (999999, 999999, 999999, 'Testy', 'McTest Jr', to_date('01-01-1900','MM-DD-YYYY'), "
				+ "'another test business', 'test@example.com', 5, 5.0)";
		String updateSql = "UPDATE kid_users SET transactions_per_day_limit = 5, transaction_amount_limit = 5.0 WHERE kid_id = 999999";
		String userSql = "INSERT INTO app_user (id, user_name, password, salt) VALUES (999999, 'Test', 'Test123', 'SaltyTest'); INSERT INTO app_user (id, user_name, password, salt) VALUES (9999991, 'TestMOM', 'Test123MOM', 'SaltyTest');";
		String guardianSql = "INSERT INTO guardian_users (user_id, guardian_id, first_name, last_name, date_of_birth, email) VALUES " + 
				"(9999991, 999999, 'Testy', 'McTest Sr', to_date('01-01-1800', 'MM-DD-YYYY'), 'test@example.com');";
		jdbcTemplate.update(userSql);
		jdbcTemplate.update(updateSql);
		jdbcTemplate.update(guardianSql);
		jdbcTemplate.update(kidSql);
	}
	
}
