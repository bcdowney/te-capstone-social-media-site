package com.techelevator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.User;
import com.techelevator.model.JDBC.JDBCUserDAO;
import com.techelevator.security.PasswordHasher;

public class JDBCUserDAOIntegrationTest extends DAOIntegrationTest {

	private JDBCUserDAO userDAO;
	private JdbcTemplate jdbcTemplate;
	private PasswordHasher hashMaster;
	private String childUserName = "TESTUSER1";
	private String childRole = "child";
	private String password = "Password123!";
	private String guardianUserName = "TESTUSER2";
	private String guardianRole = "guardian";
	private String adminUserName = "TESTUSER3";
	private String adminRole = "admin";
	private User newChildUser;
	private User newGuardianUser;
	private User newAdminUser;
	
	@Before
	public void setup() {
		hashMaster = new PasswordHasher();
		userDAO = new JDBCUserDAO(getDataSource(), hashMaster);
		jdbcTemplate = new JdbcTemplate(getDataSource());
		newChildUser = generateTestUserObject(childUserName, childRole);
		newGuardianUser = generateTestUserObject(guardianUserName, guardianRole);
		newAdminUser = generateTestUserObject(adminUserName, adminRole);
	}
	
//	@Test
//	public void save_user_correctly_inserts_new_user() {
//		//Unused Method
//	}
	
	@Test
	public void search_for_username_and_password_returns_results() {
		userDAO.insertUser(newChildUser);
		boolean result = userDAO.searchForUsernameAndPassword(childUserName, password);
		assertTrue(result);
	}
	
	@Test
	public void update_password_changes_password_value() {
		userDAO.insertUser(newChildUser);
		userDAO.updatePassword(childUserName, "AnotherPassw0rd123!");
		User result = (User) userDAO.getUserByUserName(childUserName);
		assertTrue(result.getPassword() != password);
	}
	
	@Test
	public void get_user_by_username_returns_results() {
		userDAO.insertUser(newChildUser);
		User user = (User) userDAO.getUserByUserName(childUserName);
		assertTrue(user.getUserName().equals(childUserName));
	}
	
	@Test
	public void insert_user_inserts_a_new_user_given_a_user_object() {
		userDAO.insertUser(newChildUser);
		User user = (User) userDAO.getUserByUserName(childUserName);
		assertTrue(user.getUserName().equals(childUserName));
	}
	
	@Test
	public void get_userId_by_username_returns_result() {
		userDAO.insertUser(newChildUser);
		int id = userDAO.getUserIdByUsername(newChildUser.getUserName());
		assertEquals(newChildUser.getUserId(), id);
	}
	
	@Test
	public void get_total_users_returns_results() {
		userDAO.insertUser(newChildUser);
		userDAO.insertUser(newGuardianUser);
		userDAO.insertUser(newAdminUser);
		assertTrue(userDAO.getTotalUsers(" ") > 0);
		assertTrue(userDAO.getTotalUsers("child") > 0);
		assertTrue(userDAO.getTotalUsers("guardian") > 0);
		assertTrue(userDAO.getTotalUsers("admin") > 0);
	}
	
	@Test
	public void get_user_name_by_id_returns_result() {
		userDAO.insertUser(newChildUser);
		String result = userDAO.getUserNameById(newChildUser.getUserId());
		assertTrue(result.equals(childUserName));
	}
	
	@Test
	public void search_users_by_user_name_returns_results() {
		List<User> beforeInsert = userDAO.searchUserByUserName(childUserName, childRole);
		assertFalse("Results returned for search when no valid user present", beforeInsert.size() > 0 );
		userDAO.insertUser(newChildUser);
		List<User> afterInsert = userDAO.searchUserByUserName("TEST", childRole);
		assertTrue(afterInsert.size() > 0 );
		
	}
	
	private User generateTestUserObject(String userName, String role) {
		User user = new User();
		Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
		user.setUserId(getNextUserId());
		user.setUserName(userName);
		user.setPassword(password);
		user.setConfirmPassword(password);
		user.setRole(role);
		user.setDateCreated(timeStamp);
		user.setLastModified(timeStamp);
		return user;
	}
	
	private int getNextUserId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_user_id')");
		if (nextIdResult.next() ) {
			//number added to the id to compensate for the multiple users being created to set up testing.
			return nextIdResult.getInt(1) + 3;
		} else {
			throw new RuntimeException("Error returning next user_id value");
		}
	}
}
