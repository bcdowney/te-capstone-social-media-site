package com.techelevator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.Comment;
import com.techelevator.model.JDBC.JDBCCommentDAO;

public class JDBCCommentDAOIntegrationTest extends DAOIntegrationTest {

	private JdbcTemplate jdbcTemplate;
	private JDBCCommentDAO commentDAO;
	private int userIdGuardian;
	private int userIdKid;
	private int guardianId;
	private int kidId;
	private int adId;
	private Comment testComment;
	
	@Before
	public void setup() {
		commentDAO = new JDBCCommentDAO(super.getDataSource());
		jdbcTemplate = new JdbcTemplate(super.getDataSource());
		userIdGuardian = insertFakeUser("fakeUserName1","guardian");
		userIdKid = insertFakeUser("fakeUserName2","kid");
		guardianId = insertFakeGuardian(userIdGuardian);
		kidId = insertFakeKid(userIdKid, guardianId);
		adId = insertFakeAd(kidId);
		testComment = createComment();
		testComment.setApproved(true);
	}
	
	@Test
	public void get_comments_by_ad_id_returns_results() {
		commentDAO.insertComment(testComment);
		List<Comment> results = commentDAO.getCommentsByAdId(adId);
		assertTrue(results.size() > 0);
	}
	
	@Test
	public void get_comments_by_kid_id_returns_results() {
		commentDAO.insertComment(testComment);
		List<Comment> results = commentDAO.getCommentsByKidId(kidId);
		assertTrue(results.size() > 0);
	}
	
	@Test
	public void get_approved_comments_only_returns_approved_comments() {
		commentDAO.insertComment(testComment);
		commentDAO.approveComment(testComment.getCommentId());
		Assert.assertTrue(commentDAO.getApprovedCommentsByKidId(kidId).size() > 0);
	}
	
	@Test
	public void get_unapproved_comments_only_returns_unapproved_comments() {
		testComment.setApproved(false);
		commentDAO.insertComment(testComment);
		Assert.assertTrue(commentDAO.getUnapprovedCommentsByKidId(kidId).size() > 0);
		commentDAO.approveComment(testComment.getCommentId());
		Assert.assertTrue(commentDAO.getUnapprovedCommentsByKidId(kidId).size() == 0);
	}
	
	@Test
	public void delete_comment_by_id_removes_comment_correctly() {
		commentDAO.insertComment(testComment);
		int commentId = testComment.getCommentId();
		List<Comment> resultsOfInsert = commentDAO.getCommentsByAdId(adId);
		assertTrue("getCommentsByAdId failed to return results for test part 1" , resultsOfInsert.size() > 0);
		commentDAO.deleteCommentById(commentId);
		List<Comment> resultsOfDelete = commentDAO.getCommentsByAdId(adId);
		assertFalse("deleteCommentById failed to delete the comment", resultsOfDelete.size() > 0);
	}
	
	@Test
	public void insert_comment_correctly_inserts_a_comment() {
		List<Comment> results = commentDAO.getCommentsByAdId(adId);
		assertFalse(results.size() > 0);
		commentDAO.insertComment(testComment);
		results = commentDAO.getCommentsByAdId(adId);
		assertTrue(results.size() > 0);
	}
	
	@Test
	public void approve_comment_correctly_sets_comment_to_approved() {
		testComment.setApproved(false);
		commentDAO.insertComment(testComment);
		List<Comment> resultsOfInsert = commentDAO.getCommentsByAdId(adId);
		assertTrue("getCommentsByAdId failed to return results for test part 1" , resultsOfInsert.size() == 0);
		for (Comment comment: resultsOfInsert) {
			assertFalse(comment.isApproved());
		}
		commentDAO.approveComment(testComment.getCommentId());
		List<Comment> resultsOfApproval = new ArrayList<Comment>();
		resultsOfApproval = commentDAO.getCommentsByAdId(adId);
		for (Comment comment: resultsOfApproval) {
			assertTrue(comment.isApproved());
		}
		
	}
	
	private Comment createComment() {
		Comment fakeComment = new Comment();
		Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
		fakeComment.setCommentId(getNextCommentId() + 1);
		fakeComment.setAdId(adId);
		fakeComment.setCommenterId(kidId);
		fakeComment.setComment("TEST COMMENT");
		fakeComment.setContactEmail("test@example.com");
		fakeComment.setApproved(false);
		fakeComment.setCreateDate(timeStamp);
		return fakeComment;
	}
	
	private int insertFakeAd(int kidId) {
		String sql = "INSERT INTO ads VALUES(?, ?, 'job', 'test ad title', 'TEST AD TEXT', default, null, null, null, default, 9.99, null, false);";
		int fakeAdId = getNextAdId();
		jdbcTemplate.update(sql, fakeAdId, kidId);
		return fakeAdId;
	}
	
	private int insertFakeUser(String userName, String parentOrKid) {
		String sql = "INSERT INTO app_user VALUES (?, ?, ?, ?, ?)";
		int fakeUserId = getNextUserId();
		jdbcTemplate.update(sql, fakeUserId, userName, "fakePassword123", "parentOrKid", "yes.");
		return fakeUserId;
	}
	
	private int insertFakeGuardian(int userId) {
		String sql = "INSERT INTO guardian_users VALUES (?, ?, 'Andrew', 'LeVan', to_date('01-01-1900','MM-DD-YYYY'), 'opera@example.com')";
		int fakeGuardianId = getNextGaurdianId();
		jdbcTemplate.update(sql, userId, fakeGuardianId);
		return fakeGuardianId;
	}
	
	private int insertFakeKid(int userId, int guardianId) {
		String sql = "INSERT INTO kid_users VALUES (?, ?, ?, 'kiddo', 'junior', to_date('10-20-2008','MM-DD-YYYY'), 'businessName', 'logo', 'I am a kid', 'email@example.com')";
		int kidId = getNextKidId();
		jdbcTemplate.update(sql, userId, kidId, guardianId);
		return kidId;
	}
	
	private int getNextUserId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_user_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next user_id value");
		}
	}
	
	private int getNextGaurdianId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_guardian_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next guardian_id value");
		}
	}
	
	private int getNextKidId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_kid_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next kid_id value");
		}
	}
	
	private int getNextAdId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_ad_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next ad_id value");
		}
	}
	
	private int getNextCommentId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_ad_comment_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next comment_id value");
		}
	}
}
