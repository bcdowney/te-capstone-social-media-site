package com.techelevator;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.PendingProfileChange;
import com.techelevator.model.JDBC.JDBCHistoricalChangeDAO;
import com.techelevator.model.JDBC.JDBCPendingProfileChangeDAO;

public class JDBCPendingProfileChangeDAOIntegrationTest extends DAOIntegrationTest{
	

	private JDBCHistoricalChangeDAO historicalChangeDAO;
	private JDBCPendingProfileChangeDAO pendingChangeDAO;
	private JdbcTemplate jdbcTemplate;
	private int userIdGuardian;
	private int userIdKid;
	private Guardian fakeGuardian;
	private Kid fakeKid;
	private PendingProfileChange fakePendingChange;
	
	@Before
	public void setup() {
		pendingChangeDAO = new JDBCPendingProfileChangeDAO(super.getDataSource());
		historicalChangeDAO = new JDBCHistoricalChangeDAO(super.getDataSource());
		jdbcTemplate = new JdbcTemplate(super.getDataSource());
		userIdGuardian = insertFakeUser("fakeUserName1","guardian");
		userIdKid = insertFakeUser("fakeUserName2","kid");
		fakeGuardian = insertFakeGuardian(userIdGuardian);
		fakeKid = insertFakeKid(userIdKid, fakeGuardian.getGuardianID());
		insertFakeGuardianKidCombo(fakeKid.getKidId(), fakeGuardian.getGuardianID());
		fakePendingChange = createFakePendingChange();
	}
	
	@Test
	public void get_pending_changes_by_guardian_returns_a_list_of_pending_changes_for_guardians_kids() {
		int pendingChangeBefore = pendingChangeDAO.getPendingChangesByGuardian(fakeGuardian).size();
		createFakePendingEntry(fakePendingChange);
		int pendingChangeAfter = pendingChangeDAO.getPendingChangesByGuardian(fakeGuardian).size();
		
		Assert.assertTrue(pendingChangeBefore < pendingChangeAfter);
		Assert.assertTrue(pendingChangeAfter == 1);
	}
	
	@Test
	public void get_pending_changes_by_kid_returns_a_list_of_all_kids_pending_changes() {
		createFakePendingEntry(fakePendingChange);
		
		Assert.assertTrue(pendingChangeDAO.getPendingChangesByKid(fakeKid).size() > 0);
	}
	
	@Test
	public void get_all_pending_changes_returns_all_pending_changes() {
		int sizeBeforeInsert = pendingChangeDAO.getAllPendingChanges().size();
		createFakePendingEntry(fakePendingChange);
		int sizeAfterInsert = pendingChangeDAO.getAllPendingChanges().size();
		
		Assert.assertTrue(sizeBeforeInsert < sizeAfterInsert);
	}
	
	@Test
	public void delete_pending_change_removes_entry_from_pending_change_table() {
		createFakePendingEntry(fakePendingChange);
		int sizeBeforeDelete = pendingChangeDAO.getAllPendingChanges().size();
		pendingChangeDAO.deletePendingChange(fakePendingChange.getPendingRequestId());
		int sizeAfterDelete = pendingChangeDAO.getAllPendingChanges().size();
		
		Assert.assertTrue(sizeBeforeDelete > sizeAfterDelete);
	}
	
	@Test
	public void save_to_pending_changes_saves_pending_changes() {
		int pendingChangeSizeBefore = pendingChangeDAO.getAllPendingChanges().size();
		List<PendingProfileChange> pending = new ArrayList<PendingProfileChange>();
		pending.add(fakePendingChange);
		pendingChangeDAO.saveToPendingChanges(pending);
		int pendingChangeSizeAfter = pendingChangeDAO.getAllPendingChanges().size();
		
		Assert.assertTrue(pendingChangeSizeBefore < pendingChangeSizeAfter);;
	}
	
	@Test
	public void save_to_historical_changes_deletes_off_pending_change_table_and_saves_to_historical_changes_table() {
		List<PendingProfileChange> pending = new ArrayList<PendingProfileChange>();
		pending.add(fakePendingChange);
		pendingChangeDAO.saveToPendingChanges(pending);
		int historicalChangeSizeBefore = historicalChangeDAO.getAllHistoricalChanges().size();
		pendingChangeDAO.saveToHistoricalChanges(fakePendingChange);
		int historicalChangeSizeAfter = historicalChangeDAO.getAllHistoricalChanges().size();
		
		Assert.assertTrue(historicalChangeSizeBefore < historicalChangeSizeAfter);
	}
	
	private PendingProfileChange createFakePendingChange() {
		PendingProfileChange fakePendingChange = new PendingProfileChange();
		Timestamp time = new Timestamp(System.currentTimeMillis());
		fakePendingChange.setPendingRequestId(getNextPendingChangeId());
		fakePendingChange.setUserId(userIdKid);
		fakePendingChange.setKidId(fakeKid.getKidId());
		fakePendingChange.setGuardianId(fakeGuardian.getGuardianID());
		fakePendingChange.setColumnName("logoFileName");
		fakePendingChange.setCurrentValue("old logo");
		fakePendingChange.setRequestedValue("new logo");
		fakePendingChange.setApproved(false);
		fakePendingChange.setCreated(time);
		return fakePendingChange;
	}
	
	private void createFakePendingEntry(PendingProfileChange fakeChange) {
		String sql = "INSERT INTO kid_users_pending_changes VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, fakeChange.getPendingRequestId(), fakeChange.getUserId(), fakeChange.getKidId(), fakeChange.getGuardianId(), fakeChange.getColumnName(), fakeChange.getCurrentValue(), fakeChange.getRequestedValue(), fakeChange.isApproved(), fakeChange.getCreated());
	}
	
	private int insertFakeUser(String userName, String parentOrKid) {
		String sql = "INSERT INTO app_user VALUES (?, ?, ?, ?, ?)";
		int fakeUserId = getNextUserId();
		jdbcTemplate.update(sql, fakeUserId, userName, "fakePassword123", parentOrKid, "yes.");
		return fakeUserId;
	}
	
	private Guardian insertFakeGuardian(int userId) {
		String sql = "INSERT INTO guardian_users VALUES (?, ?, 'Andrew', 'LeVan', to_date('01-01-1900','MM-DD-YYYY'), 'opera@example.com')";
		Guardian guardian = new Guardian();
		guardian.setGuardianID(getNextGaurdianId());
		jdbcTemplate.update(sql, userId, guardian.getGuardianID());
		return guardian;
	}
	
	private Kid insertFakeKid(int userId, int guardianId) {
		String sql = "INSERT INTO kid_users VALUES (?, ?, ?, 'kiddo', 'junior', to_date('10-20-2008','MM-DD-YYYY'), 'businessName', 'logo', 'I am a kid', 'email@example.com')";
		Kid kid = new Kid();
		kid.setKidId(getNextKidId());
		jdbcTemplate.update(sql, userId, kid.getKidId(), guardianId);
		return kid;
	}
	
	private void insertFakeGuardianKidCombo(int kidId, int guardianId) {
		String sql = "INSERT INTO guardians_kids VALUES (?, ?)";
		jdbcTemplate.update(sql, kidId, guardianId);
	}
	
	private int getNextUserId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_user_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next user_id value");
		}
	}
	
	private int getNextGaurdianId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_guardian_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next guardian_id value");
		}
	}
	
	private int getNextKidId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_kid_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next kid_id value");
		}
	}
	
	private int getNextPendingChangeId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_pending_kid_request_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next kid_id value");
		}
	}
	
	
	
	

}
