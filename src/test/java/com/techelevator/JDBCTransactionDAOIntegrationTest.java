package com.techelevator;

import java.sql.Timestamp;
import java.util.List;

import org.junit.*;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.model.Transaction;
import com.techelevator.model.JDBC.JDBCTransactionDAO;

public class JDBCTransactionDAOIntegrationTest extends DAOIntegrationTest{
	
	private JDBCTransactionDAO transactionDAO;
	private JdbcTemplate jdbcTemplate;
	private static Timestamp NOW;
	private static Timestamp BEFORE;
	
	@Before
	public void setup() {
		this.transactionDAO = new JDBCTransactionDAO(super.getDataSource());
		this.jdbcTemplate = new JdbcTemplate(super.getDataSource());
		NOW = new Timestamp(System.currentTimeMillis());
		BEFORE = new Timestamp(System.currentTimeMillis() - (2000L * 60L * 60L * 24L));
		jdbcTemplate.update("DELETE FROM transactions");
		jdbcTemplate.update("INSERT INTO transactions (transaction_id, from_kid_id, to_kid_id, amount, transaction_type, approved, transaction_timestamp) VALUES (100, 1, 2, 100.0, 'Funding', true, '2018-12-19 13:15:48')");
		jdbcTemplate.update("INSERT INTO transactions (transaction_id, from_kid_id, to_kid_id, amount, transaction_type, approved, transaction_timestamp) VALUES (101, 1, 2, 50.50, 'Funding', true, '2018-12-20 13:15:48')");
		jdbcTemplate.update("INSERT INTO transactions (transaction_id, from_kid_id, to_kid_id, amount, transaction_type, approved, transaction_timestamp) VALUES (102, 2, 1, 100.0, 'Funding', true, '2018-12-19 13:15:48')");
		jdbcTemplate.update("INSERT INTO transactions (transaction_id, from_kid_id, to_kid_id, amount, transaction_type, approved, transaction_timestamp) VALUES (103, 2, 1, 50.50, 'Funding', true, '2018-12-20 13:15:48')");
		jdbcTemplate.update("INSERT INTO transactions (transaction_id, from_kid_id, to_kid_id, amount, transaction_type, approved, transaction_timestamp) VALUES (104, 1, 2, 100.0, 'Funding', false, '2018-12-19 13:15:48')");
		jdbcTemplate.update("INSERT INTO transactions (transaction_id, from_kid_id, to_kid_id, amount, transaction_type, approved, transaction_timestamp) VALUES (105, 2, 1, 100.0, 'Funding', false, '2018-12-19 13:15:48')");
	}
	
	@Test
	public void test_get_transaction_by_transaction_number() {
		Transaction transaction = transactionDAO.getTransactionByTransactionNumber(100);
		Assert.assertNotNull(transaction);
		Assert.assertEquals("Did not return correct transaction", "Funding", transaction.getTransactionType());
	}
	
	@Test
	public void test_get_all_transactions_by_fromkidid() {
		List<Transaction> transactions = transactionDAO.getAllTransactionsByFromKidId(1);
		Assert.assertNotNull(transactions);
		Assert.assertFalse(transactions.isEmpty());
		Assert.assertEquals("Returned more transactions than expected", 2, transactions.size());
	}
	
	@Test
	public void test_get_all_transactions_by_tokidid() {
		List<Transaction> transactions = transactionDAO.getAllTransactionsByToKidId(1);
		Assert.assertNotNull(transactions);
		Assert.assertFalse(transactions.isEmpty());
		Assert.assertEquals("Returned more transactions than expected", 2, transactions.size());
	}
	
	@Test
	public void test_get_total_of_transactions_from_beginning_by_fromkidid() {
		double total = transactionDAO.getTotalOfTransactionsFromBeginningByFromKidId(NOW, 1);
		Assert.assertTrue(total != 0.0);
		Assert.assertTrue(total == 50.50);
	}
	
	@Test
	public void test_get_total_of_transactions_from_beginning_by_tokidid() {
		double total = transactionDAO.getTotalOfTransactionsFromBeginningByToKidId(NOW, 1);
		Assert.assertTrue(total != 0.0);
		Assert.assertTrue(total == 50.50);
	}
	
	@Test
	public void test_get_total_of_transactions_between_times_by_fromkidid() {
		double total = transactionDAO.getTotalOfTransactionsBetweenTimesByFromKidId(BEFORE, NOW, 1);
		Assert.assertTrue(total != 0.0);
		Assert.assertTrue(total == 100.00);
	}
	
	@Test
	public void test_get_total_of_transactions_between_times_by_tokidid() {
		double total = transactionDAO.getTotalOfTransactionsBetweenTimesByToKidId(BEFORE, NOW, 1);
		Assert.assertTrue(total != 0.0);
		Assert.assertTrue(total == 100.00);
	}
	
	@Test
	public void test_insert_transaction() {
		transactionDAO.insertTransaction(generateFakeTransaction());
		Transaction fakeTransaction = transactionDAO.getTransactionByTransactionNumber(99999);
		Assert.assertNotNull(fakeTransaction);
		Assert.assertEquals("Did not return fake transaction", false, fakeTransaction.isTransactionApproved());
	}
	
	@Test
	public void test_delete_transaction_by_transaction_number() {
		Transaction transaction = generateFakeTransaction();
		transactionDAO.insertTransaction(transaction);
		Transaction fakeTransaction = transactionDAO.getTransactionByTransactionNumber(99999);
		Assert.assertNotNull(fakeTransaction);
		transactionDAO.deleteTransactionByTransactionNumber(99999);
		Transaction test = transactionDAO.getTransactionByTransactionNumber(99999);
		Assert.assertTrue(test.getTransactionType() == null);
	}
	
	@Test
	public void test_get_all_unapproved_transactions_by_fromkidid() {
		List<Transaction> transactions = transactionDAO.getAllUnapprovedTransactionsByFromKidId(1);
		Assert.assertNotNull(transactions);
		Assert.assertEquals("Returns more transactions than it should have", 1, transactions.size());
	}
	
	@Test
	public void test_approve_transaction() {
		transactionDAO.insertTransaction(generateFakeTransaction());
		transactionDAO.approveTransaction(99999);
		Transaction transaction = transactionDAO.getTransactionByTransactionNumber(99999);
		Assert.assertEquals("Did not return correct transaction", "Funding", transaction.getTransactionType());
		Assert.assertTrue(transaction.isTransactionApproved());
	}
	
	@Test
	public void test_get_number_of_transactions_from_beginning_by_fromkidid() {
		transactionDAO.insertTransaction(generateFakeTransaction());
		int numberOfTransactions = transactionDAO.getNumberOfTransactionsFromBeginningByFromKidId(BEFORE, NOW, 2);
		Assert.assertEquals("Did not return correct number of transactions", 3, numberOfTransactions);
	}
	
	private Transaction generateFakeTransaction() {
		Transaction transaction = new Transaction();
		transaction.setTransactionNumber(99999);
		transaction.setToKid(1);
		transaction.setFromKid(2);
		transaction.setTransactionApproved(false);
		transaction.setAmount(40.00);
		transaction.setTransactionType("Funding");
		transaction.setTransactionTime(BEFORE);
		return transaction;
	}

}
