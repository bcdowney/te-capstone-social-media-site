package com.techelevator;

import java.sql.Timestamp;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.Guardian;
import com.techelevator.model.HistoricalChange;
import com.techelevator.model.Kid;
import com.techelevator.model.PendingProfileChange;
import com.techelevator.model.JDBC.JDBCHistoricalChangeDAO;

public class JDBCHistoricalChangeDAOIntegrationTest extends DAOIntegrationTest {

	private JDBCHistoricalChangeDAO historicalChangeDAO;
	private JdbcTemplate jdbcTemplate;
	private int userIdGuardian;
	private int userIdKid;
	private Guardian fakeGuardian;
	private Kid fakeKid;
	private PendingProfileChange fakePendingChange;
	
	@Before
	public void setup() {
		historicalChangeDAO = new JDBCHistoricalChangeDAO(super.getDataSource());
		jdbcTemplate = new JdbcTemplate(super.getDataSource());
		userIdGuardian = insertFakeUser("fakeUserName1","guardian");
		userIdKid = insertFakeUser("fakeUserName2","kid");
		fakeGuardian = insertFakeGuardian(userIdGuardian);
		fakeKid = insertFakeKid(userIdKid, fakeGuardian.getGuardianID());
	}
	
	@Test
	public void get_all_historical_changes_returns_a_list_of_historical_changes() {
		HistoricalChange hc = createFakeHistoricalChange();
		insertFakeHistoricalChange(hc);
		List<HistoricalChange> hcList = historicalChangeDAO.getAllHistoricalChanges();
		Assert.assertTrue(hcList.size() > 0);
	}
	
	@Test
	public void get_historical_change_by_id_returns_a_historical_change() {
		HistoricalChange hc = createFakeHistoricalChange();
		insertFakeHistoricalChange(hc);
		Assert.assertEquals(hc.getClass(), historicalChangeDAO.getHistoricalChangeById(hc.getHistoricalChangeId()).getClass());
	}
	
	private HistoricalChange createFakeHistoricalChange() {
		HistoricalChange hc = new HistoricalChange();
		Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
		hc.setHistoricalChangeId(getNextHistoricalChangeId());
		hc.setChangeInitiatedById(userIdKid);
		hc.setChangeToUserId(userIdKid);
		hc.setChangedColumn("first_name");
		hc.setOldValue("old value");
		hc.setNewValue("new value");
		hc.setRequestTime(timeStamp);
		hc.setCompleteTime(timeStamp);
		return hc;
	}
	
	private void insertFakeHistoricalChange(HistoricalChange hc) {
		String sql = "INSERT INTO historical_changes VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, hc.getHistoricalChangeId(), hc.getChangeInitiatedById(), hc.getChangeToUserId(), hc.getChangedColumn(), hc.getOldValue(), hc.getNewValue(), hc.getRequestTime(), hc.getCompleteTime());

	}
	
	private int insertFakeUser(String userName, String parentOrKid) {
		String sql = "INSERT INTO app_user VALUES (?, ?, ?, ?, ?)";
		int fakeUserId = getNextUserId();
		jdbcTemplate.update(sql, fakeUserId, userName, "fakePassword123", "parentOrKid", "yes.");
		return fakeUserId;
	}
	
	private Guardian insertFakeGuardian(int userId) {
		String sql = "INSERT INTO guardian_users VALUES (?, ?, 'Andrew', 'LeVan', to_date('01-01-1900','MM-DD-YYYY'), 'opera@example.com')";
		Guardian guardian = new Guardian();
		guardian.setGuardianID(getNextGaurdianId());
		jdbcTemplate.update(sql, userId, guardian.getGuardianID());
		return guardian;
	}
	
	private Kid insertFakeKid(int userId, int guardianId) {
		String sql = "INSERT INTO kid_users VALUES (?, ?, ?, 'kiddo', 'junior', to_date('10-20-2008','MM-DD-YYYY'), 'businessName', 'logo', 'I am a kid', 'email@example.com')";
		Kid kid = new Kid();
		kid.setKidId(getNextKidId());
		jdbcTemplate.update(sql, userId, kid.getKidId(), guardianId);
		return kid;
	}
	
	private int getNextUserId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_user_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next user_id value");
		}
	}
	
	private int getNextGaurdianId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_guardian_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next guardian_id value");
		}
	}
	
	private int getNextKidId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_kid_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next kid_id value");
		}
	}
	
	private int getNextHistoricalChangeId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_history_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next historical change id value");
		}
	}
	
	
}
