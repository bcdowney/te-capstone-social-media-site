package com.techelevator;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.model.Admin;
import com.techelevator.model.Kid;
import com.techelevator.model.JDBC.JDBCAdminDAO;
import com.techelevator.model.JDBC.JDBCKidDAO;
import com.techelevator.model.Guardian;

public class JDBCAdminDAOIntegraionTest extends DAOIntegrationTest {
	private JdbcTemplate jdbcTemplate;
	private JDBCAdminDAO adminDAO;
	private JDBCKidDAO kidDAO;
	
	@Before
	public void setup() {
		adminDAO = new JDBCAdminDAO(getDataSource());
		jdbcTemplate = new JdbcTemplate(getDataSource());
		kidDAO = new JDBCKidDAO(getDataSource());
		jdbcSetup();
	}
	
	@Test
	public void testGetAdminByUserId() {
		Admin admin = adminDAO.getAdminByUserId(8888);
		Assert.assertNotNull(admin);
		Assert.assertEquals("dogpig12@example.com", admin.getEmail());
	}
	
	@Test
	public void testGetAllAdmins() {
		List<Admin> adminList = adminDAO.getAllAdmins();
		
		Assert.assertTrue(adminList.size() > 0);
	}
	
	@Test
	public void testGetKidsByGuardianId() {
		List<Kid> listOfKids = adminDAO.getKidsByGuardianId(1);
		
		Assert.assertTrue(listOfKids.size() > 0);
	}
	
	@Test
	public void testGetGuardianByGuardianId() {
		Guardian guardian = adminDAO.getGuardianByGuardianId(9999);
		
		Assert.assertEquals("Testy", guardian.getFirstName());
		Assert.assertEquals("McTest Sr", guardian.getLastName());

	}
	
	@Test
	public void testGetGuardianByKidId() {
		Guardian guardian = adminDAO.getGuardianByKidId(9999);
		
		Assert.assertEquals("Testy", guardian.getFirstName());
		Assert.assertEquals("McTest Sr", guardian.getLastName());	
	}
	
	@Test
	public void testGetAllGuardians() {
		List<Guardian> listOfGuardians = adminDAO.getAllGuardians();
		
		Assert.assertTrue(listOfGuardians.size() > 0);
	}
	
	@Test
	public void ensureAdminSaveInsertsNewAdmin() {
		List<Admin> startingAdmins = adminDAO.getAllAdmins();
		int startSize = startingAdmins.size();
		adminDAO.saveAdmin(makeAdminForTest());
		List<Admin> endingAdmins = adminDAO.getAllAdmins();
		int endSize = endingAdmins.size();
		Assert.assertTrue(startSize < endSize);
		
	}
	
	private Admin makeAdminForTest() {
		Admin admin = new Admin();
		String sql = "INSERT INTO app_user (id, user_name, password, salt, role) VALUES (99999999, 'ADMININSERTTEST', 'Test1234!', 'SaltyTest', 'admin')";
		jdbcTemplate.update(sql);
		admin.setUserId(99999999);
		admin.setUserName("ADMININSERTTEST");
		admin.setAccessLevel();
		admin.setEmail("test@example.com");
		return admin;
	}

	private void jdbcSetup() {
		String kidSql = "INSERT INTO kid_users (user_id, kid_id, guardian_id, first_name, "
				+ "last_name, date_of_birth, business_name, email) VALUES (9999, 9999, 9999, 'Testy', 'McTest Jr', to_date('01-01-1900','MM-DD-YYYY'), "
				+ "'a business', 'test@example.com')";
		String userSql = "INSERT INTO app_user (id, user_name, password, salt) VALUES (9999, 'Test', 'Test123', 'SaltyTest'); INSERT INTO app_user (id, user_name, password, salt) VALUES (99999, 'Test2', 'Test1234', 'SaltyTest2'); INSERT INTO app_user (id, user_name, password, salt) VALUES (8888, 'Test3', 'Test12345', 'SaltyTest3');";
		String guardianKidSql = "INSERT INTO guardians_kids (guardian_id, kid_id) VALUES (9999, 9999);";
		String guardianSql = "INSERT INTO guardian_users (user_id, guardian_id, first_name, last_name, date_of_birth, email) VALUES " + 
				"(99999, 9999, 'Testy', 'McTest Sr', to_date('01-01-1800', 'MM-DD-YYYY'), 'test@example.com');";
		String adminSql = "INSERT INTO admin_users (user_id, email) VALUES " +
				"(8888, 'dogpig12@example.com');";
		jdbcTemplate.update(userSql);
		jdbcTemplate.update(guardianSql);
		jdbcTemplate.update(kidSql);
		jdbcTemplate.update(guardianKidSql);
		jdbcTemplate.update(adminSql);
	}
}
