package com.techelevator;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.Kid;
import com.techelevator.model.JDBC.JDBCGuardianDAO;
import com.techelevator.model.Guardian;

public class JDBCGuardianDAOIntegrationTest extends DAOIntegrationTest {

	private JDBCGuardianDAO guardianDAO;
	private JdbcTemplate jdbcTemplate;
	private int userIdGuardian;
	private int userIdKid;
	private int guardianId;
	private int kidId;
	
	@Before
	public void setup() {
		guardianDAO = new JDBCGuardianDAO(super.getDataSource());
		jdbcTemplate = new JdbcTemplate(super.getDataSource());
		userIdGuardian = insertFakeUser("fakeUserName1","guardian");
		userIdKid = insertFakeUser("fakeUserName2","kid");
		guardianId = insertFakeGuardian(userIdGuardian);
		kidId = insertFakeKid(userIdKid, guardianId);
		insertFakeGuardianKidCombo(kidId, guardianId);
	}
	
	@Test
	public void ensure_kid_list_returns_when_get_all_kids_is_called() {
		List<Kid> listOfKids = guardianDAO.getAllKids(guardianId);
		Assert.assertTrue(listOfKids.size() > 0);
	}
	
	@Test
	public void test_get_guardian_by_user_id() {
		Guardian guardian = guardianDAO.getGuardianByUserId(userIdGuardian);
		Assert.assertNotNull(guardian);
		Assert.assertEquals("Did not pull correct first name", "Andrew", guardian.getFirstName());
	}
	
	@Test
	public void test_get_guardian_by_guardian_id() {
		Guardian guardian = guardianDAO.getGuardianByGuardianId(guardianId);
		Assert.assertNotNull(guardian);
		Assert.assertEquals("Did not pull correct first name", "Andrew", guardian.getFirstName());
	}
	
	private int insertFakeUser(String userName, String parentOrKid) {
		String sql = "INSERT INTO app_user VALUES (?, ?, ?, ?, ?)";
		int fakeUserId = getNextUserId();
		jdbcTemplate.update(sql, fakeUserId, userName, "fakePassword123", "parentOrKid", "yes.");
		return fakeUserId;
	}
	
	private int insertFakeGuardian(int userId) {
		String sql = "INSERT INTO guardian_users VALUES (?, ?, 'Andrew', 'LeVan', to_date('01-01-1900','MM-DD-YYYY'), 'opera@example.com')";
		int fakeGuardianId = getNextGaurdianId();
		jdbcTemplate.update(sql, userId, fakeGuardianId);
		return fakeGuardianId;
	}
	
	private int insertFakeKid(int userId, int guardianId) {
		String sql = "INSERT INTO kid_users VALUES (?, ?, ?, 'kiddo', 'junior', to_date('10-20-2008','MM-DD-YYYY'), 'businessName', 'logo', 'I am a kid', 'email@example.com')";
		int kidId = getNextKidId();
		jdbcTemplate.update(sql, userId, kidId, guardianId);
		return kidId;
	}
	
	private void insertFakeGuardianKidCombo(int kidId, int guardianId) {
		String sql = "INSERT INTO guardians_kids VALUES (?, ?)";
		jdbcTemplate.update(sql, kidId, guardianId);
	}
	
	private int getNextUserId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_user_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next user_id value");
		}
	}
	
	private int getNextGaurdianId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_guardian_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next guardian_id value");
		}
	}
	
	private int getNextKidId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_kid_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next kid_id value");
		}
	}
	
}
