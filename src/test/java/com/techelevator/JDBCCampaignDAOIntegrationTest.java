package com.techelevator;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.Campaign;
import com.techelevator.model.JDBC.JDBCAdDAO;
import com.techelevator.model.JDBC.JDBCCampaignDAO;

public class JDBCCampaignDAOIntegrationTest extends DAOIntegrationTest {
	
	private JdbcTemplate jdbcTemplate;
	private JDBCCampaignDAO campaignDAO;
	private int userIdGuardian;
	private int userIdKid;
	private int guardianId;
	private int kidId;
	
	@Before
	public void setup() {
		campaignDAO = new JDBCCampaignDAO(super.getDataSource());
		jdbcTemplate = new JdbcTemplate(super.getDataSource());
		userIdGuardian = insertFakeUser("fakeUserName1","guardian");
		userIdKid = insertFakeUser("fakeUserName2","kid");
		guardianId = insertFakeGuardian(userIdGuardian);
		kidId = insertFakeKid(userIdKid, guardianId);
	}
	
	@Test
	public void insert_campaign_inserts_campaigns_correctly() {
		Campaign fakeCampaign = createFakeCampaign();
		campaignDAO.insertCampaign(fakeCampaign);
		Assert.assertTrue(campaignDAO.getCampaignByKidId(kidId).getTitle().equals("Fake Campaign 123"));
	}
	
	@Test
	public void delete_campaign_by_campaign_id_deletes_off_table_correctly() {
		Campaign fakeCampaign = createFakeCampaign();
		campaignDAO.insertCampaign(fakeCampaign);
		int sizeBefore = campaignDAO.getAllCampaigns().size();
		campaignDAO.deleteCampaignByCampaignId(getNextCampaignId() - 1);
		int sizeAfter = campaignDAO.getAllCampaigns().size();
		Assert.assertTrue(sizeBefore > sizeAfter);
	}
	
	@Test
	public void get_campaign_by_campaign_id_returns_correct_campaign() {
		Campaign fakeCampaign = createFakeCampaign();
		campaignDAO.insertCampaign(fakeCampaign);
		Assert.assertTrue(campaignDAO.getCampaignByCampaignId(getNextCampaignId() - 1).getTitle().equals("Fake Campaign 123"));
	}
	
	@Test
	public void get_campaign_by_kid_id_returs_correct_campaign() {
		Campaign fakeCampaign = createFakeCampaign();
		campaignDAO.insertCampaign(fakeCampaign);
		Assert.assertTrue(campaignDAO.getCampaignByKidId(kidId).getTitle().equals("Fake Campaign 123"));
	}
	
	@Test
	public void get_all_campaigns_returns_a_list_of_all_campaigns() {
		int sizeBefore = campaignDAO.getAllCampaigns().size();
		Campaign fakeCampaign = createFakeCampaign();
		campaignDAO.insertCampaign(fakeCampaign);
		int sizeAfter = campaignDAO.getAllCampaigns().size();
		Assert.assertTrue(sizeBefore < sizeAfter);
	}
	
	@Test
	public void get_unapproved_campaign_by_kid_id_returns_unapproved_campaigns_only() {
		Campaign fakeCampaign = createFakeCampaign();
		campaignDAO.insertCampaign(fakeCampaign);
		Assert.assertFalse(campaignDAO.getUnapprovedCampaignByKidId(kidId).isApproved());
	}
	
	@Test
	public void approve_campaign_approves_correctly() {
		Campaign fakeCampaign = createFakeCampaign();
		campaignDAO.insertCampaign(fakeCampaign);
		Assert.assertFalse(campaignDAO.getCampaignByKidId(kidId).isApproved());
		campaignDAO.approveCampaign(getNextCampaignId() - 1);
		Assert.assertTrue(campaignDAO.getCampaignByKidId(kidId).isApproved());
	}
	
	@Test
	public void ensure_get_all_approved_campaigns_returns_results() {
		Campaign c = createFakeCampaign();
		c.setApproved(true);
		campaignDAO.insertCampaign(c);
		List<Campaign> result = campaignDAO.getAllApprovedCampaigns();
		Assert.assertTrue(result.size() > 0);
	}
	
	private Campaign createFakeCampaign() {
		Campaign c = new Campaign();
		c.setKidId(kidId);
		c.setTitle("Fake Campaign 123");
		c.setDescription("Fake Description");
		c.setCurrent(20.00);
		c.setGoal(50.00);
		c.setApproved(false);
		return c;
	}
	
	private int insertFakeUser(String userName, String parentOrKid) {
		String sql = "INSERT INTO app_user VALUES (?, ?, ?, ?, ?)";
		int fakeUserId = getNextUserId();
		jdbcTemplate.update(sql, fakeUserId, userName, "fakePassword123", "parentOrKid", "yes.");
		return fakeUserId;
	}
	
	private int insertFakeGuardian(int userId) {
		String sql = "INSERT INTO guardian_users VALUES (?, ?, 'Andrew', 'LeVan', to_date('01-01-1900','MM-DD-YYYY'), 'opera@example.com')";
		int fakeGuardianId = getNextGaurdianId();
		jdbcTemplate.update(sql, userId, fakeGuardianId);
		return fakeGuardianId;
	}
	
	private int insertFakeKid(int userId, int guardianId) {
		String sql = "INSERT INTO kid_users VALUES (?, ?, ?, 'kiddo', 'junior', to_date('10-20-2008','MM-DD-YYYY'), 'businessName', 'logo', 'I am a kid', 'email@example.com')";
		int kidId = getNextKidId();
		jdbcTemplate.update(sql, userId, kidId, guardianId);
		return kidId;
	}
	
	private int getNextUserId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_user_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next user_id value");
		}
	}
	
	private int getNextGaurdianId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_guardian_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next guardian_id value");
		}
	}
	
	private int getNextKidId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_kid_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next kid_id value");
		}
	}
	
	private int getNextCampaignId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_campaign_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next campaign_id value");
		}
	}

}
