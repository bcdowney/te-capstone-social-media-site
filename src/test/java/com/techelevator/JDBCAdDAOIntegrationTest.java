package com.techelevator;

import java.sql.Date;
import java.sql.Time;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.Ad;
import com.techelevator.model.JDBC.JDBCAdDAO;

public class JDBCAdDAOIntegrationTest extends DAOIntegrationTest {
	
	private JdbcTemplate jdbcTemplate;
	private JDBCAdDAO adDAO;
	private int userIdGuardian;
	private int userIdKid;
	private int guardianId;
	private int kidId;
	
	@Before
	public void setup() {
		adDAO = new JDBCAdDAO(super.getDataSource());
		jdbcTemplate = new JdbcTemplate(super.getDataSource());
		userIdGuardian = insertFakeUser("fakeUserName1","guardian");
		userIdKid = insertFakeUser("fakeUserName2","kid");
		guardianId = insertFakeGuardian(userIdGuardian);
		kidId = insertFakeKid(userIdKid, guardianId);
	}
	
	@Test
	public void when_ad_is_inserted_it_is_inserted_correctly() {
		Ad fakeAd = createAd("job");
		int sizeBefore = adDAO.getAllAds().size();
		fakeAd.setApproved(true);
		adDAO.insertAd(fakeAd);
		int sizeAfter = adDAO.getAllAds().size();
		Assert.assertTrue(sizeAfter > sizeBefore);
	}
	
	@Test
	public void get_all_ads_returns_a_list_of_all_ads() {
		Ad fakeAd = createAd("job");
		int sizeBefore = adDAO.getAllAds().size();
		fakeAd.setApproved(true);
		adDAO.insertAd(fakeAd);
		int sizeAfter = adDAO.getAllAds().size();
		Assert.assertTrue(sizeAfter > sizeBefore);
	}
	
	@Test
	public void get_ad_by_ad_id_returns_correct_ad() {
		Ad fakeAd = createAd("job");
		adDAO.insertAd(fakeAd);
		int fakeAdId = getNextAdId() - 1;
		Assert.assertEquals(fakeAdId, adDAO.getAdByAdId(fakeAdId).getAdId());
	}
	
	@Test 
	public void is_ad_approved_returns_back_correct_response() {
		Ad fakeAd = createAd("job");
		
		adDAO.insertAd(fakeAd);
		int fakeAdId = getNextAdId() - 1;
		Assert.assertFalse(adDAO.isAdApproved(fakeAdId));
	}
	
	@Test 
	public void get_all_jobs_returns_all_job_ads() {
		int sizeBefore = adDAO.getAllJobs().size();
		Ad fakeAd = createAd("job");
		adDAO.insertAd(fakeAd);
		int sizeAfter = adDAO.getAllJobs().size();
		Assert.assertTrue(sizeBefore < sizeAfter);
	}
	
	@Test 
	public void get_all_skills_returns_all_skill_ads() {
		int sizeBefore = adDAO.getAllSkills().size();
		Ad fakeAd = createAd("skill");
		adDAO.insertAd(fakeAd);
		int sizeAfter = adDAO.getAllSkills().size();
		Assert.assertTrue(sizeBefore < sizeAfter);
	}
	
	@Test
	public void approve_ad() {
		Ad fakeAd = createAd("skill");
		adDAO.insertAd(fakeAd);
		int fakeAdId = getNextAdId() - 1;
		adDAO.approveAd(fakeAdId);
		Assert.assertTrue(adDAO.isAdApproved(fakeAdId));
	}
	
	@Test
	public void get_approved_ads_by_kid_id_returns_approved_ads_only() {
		Ad fakeAd = createAd("skill");
		adDAO.insertAd(fakeAd);
		int fakeAdId = getNextAdId() - 1;
		Assert.assertTrue(adDAO.getApprovedAdsbyKidId(kidId).size() == 0);
		adDAO.approveAd(fakeAdId);
		Assert.assertTrue(adDAO.getApprovedAdsbyKidId(kidId).size() == 1);
	}
	
	@Test
	public void get_unapproved_ads_by_kid_id_returns_unapproved_ads_only() {
		Ad fakeAd = createAd("skill");
		adDAO.insertAd(fakeAd);
		int fakeAdId = getNextAdId() - 1;
		Assert.assertTrue(adDAO.getUnapprovedAdsbyKidId(kidId).size() == 1);
		adDAO.approveAd(fakeAdId);
		Assert.assertTrue(adDAO.getUnapprovedAdsbyKidId(kidId).size() == 0);
	}
	
	
	private Ad createAd(String type) {
		Date date = new Date(System.currentTimeMillis());
		Time time = new Time(System.currentTimeMillis());
		Ad fakeAd = new Ad();
		fakeAd.setKidId(kidId);
		fakeAd.setAdType(type);
		fakeAd.setTitle("fakeTitle");
		fakeAd.setDescription("fake");
		fakeAd.setStartDate(date);
		fakeAd.setEndDate(date);
		fakeAd.setStartTime(time);
		fakeAd.setEndTime(time);
		fakeAd.setPayDollars(5.00);
		fakeAd.setBarterDescription("fake");
		fakeAd.setApproved(false);
		fakeAd.setLogoFileName("logoFileName");
		fakeAd.setBusinessName("businessName");
		return fakeAd;
	}
	
	private int insertFakeUser(String userName, String parentOrKid) {
		String sql = "INSERT INTO app_user VALUES (?, ?, ?, ?, ?)";
		int fakeUserId = getNextUserId();
		jdbcTemplate.update(sql, fakeUserId, userName, "fakePassword123", "parentOrKid", "yes.");
		return fakeUserId;
	}
	
	private int insertFakeGuardian(int userId) {
		String sql = "INSERT INTO guardian_users VALUES (?, ?, 'Andrew', 'LeVan', to_date('01-01-1900','MM-DD-YYYY'), 'opera@example.com')";
		int fakeGuardianId = getNextGaurdianId();
		jdbcTemplate.update(sql, userId, fakeGuardianId);
		return fakeGuardianId;
	}
	
	private int insertFakeKid(int userId, int guardianId) {
		String sql = "INSERT INTO kid_users VALUES (?, ?, ?, 'kiddo', 'junior', to_date('10-20-2008','MM-DD-YYYY'), 'businessName', 'logo', 'I am a kid', 'email@example.com')";
		int kidId = getNextKidId();
		jdbcTemplate.update(sql, userId, kidId, guardianId);
		return kidId;
	}
	
	private int getNextUserId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_user_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next user_id value");
		}
	}
	
	private int getNextGaurdianId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_guardian_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next guardian_id value");
		}
	}
	
	private int getNextKidId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_kid_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next kid_id value");
		}
	}
	
	private int getNextAdId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_ad_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next ad_id value");
		}
	}

}
