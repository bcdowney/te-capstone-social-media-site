<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <c:import url="/WEB-INF/jsp/header.jsp" />
 
<div class="container-fluid">
	<div class="jumbotron" style="background-color: #FDFEFF">
		<h1 class="text-center display-3" style="border-bottom: 4px solid orange"> JOB BOARD</h1>
		<div class="text-center" id="change-details-buttons">
			<button class="btn btn-primary active see-jobs-btn">See All Jobs</button>
			<button class="btn btn-outline-primary post-job-btn">Post a Job</button>
			<button class="btn btn-outline-primary see-campaign-btn">See Campaigns</button>
		</div>
	</div>
</div>
	<div class="container see-jobs">
		<c:set var="count" value="1"/>
		<c:forEach var="ad" items="${currentAds}" >
			
			<div class="row ad " style="padding-top: 1em; padding-bottom: 1em; margin-bottom: 1em; border-bottom: 2px solid lightblue">
				<div class="col-sm-12">
					<div class="col-sm-3 align-top" style="display: inline-block; height: 100%">
							<c:url var="logo" value="${ad.logoFileName }" />
							<img src = "${logo}" class="img-thumbnail img-responsive img-fluid" alt="business logo"/>
					</div>
					<div class="col-sm-5" style="display: inline-block">
						<h2 style="display: inline-block; text-align: center;" class="center">${ad.title }</h2>
						<h4>${ad.businessName }</h4>
						<div style="margin-top: 2em;">
						
							<!-- Button trigger modal -->
							<button type="button" class="btn btn-outline-primary generate-comments" id="${ad.adId }" style="display: inline-block; width: 50%; margin: auto" data-toggle="modal" data-target="#exampleModal${count}">
						 		 View Details
							</button>
						</div>
					</div>
					<div class="col-sm-3" compensation" style="display: inline-block; vertical-align: top">
						<div style="margin:.5em">
							<strong>Compensation: </strong><em><fmt:formatNumber value="${ad.payDollars }" type="currency"/></em>
						</div>
						<div style="margin:.5em">
							<strong>Start:</strong><em><c:out value=" ${ad.startDate }" /></em>
						</div>
					</div>
				</div>
			</div>
				<!-- Modal -->
				<div class="modal fade bd-example-modal-lg" id="exampleModal${count}" tabindex="-1" role="dialog" aria-labelledby="exampleModal${count }Label" aria-hidden="true">
				  <div class="modal-dialog rounded" style="background-color: #FDFEFF; width: 80%; margin: auto; margin-top: 4em; padding: .3em;" role="document">
				    <div class="modal-content">
				      <div class="modal-header bg-info text-white">
				        <h5 class="modal-title" id="exampleModal${count}Label">${ad.title}</h5>
				        <p class="hidden" id="${ad.adId}"></p>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<strong>Description: </strong>
				        <p> ${ad.description } </p>
				        <br>
				        <strong>Compensation: </strong>
				        <c:choose>
				        	<c:when test="${ad.barterDescription.length() > 0 }" >
				        		<p>${ad.barterDescription}</p>
				        	</c:when>
				        	<c:otherwise>
				        		<p><fmt:formatNumber value="${ad.payDollars }" type="currency"/></p>
				        	</c:otherwise>
				        </c:choose>
				        <br>
				        <strong>Start Date: </strong>
				        <p><c:out value="${ad.startDate }" /></p>
				        <br>
				        <c:if test="${ad.endDate != null }" >
							<strong>End Date: </strong>
							<p><c:out value=" ${ad.endDate }"/></p>
						</c:if>
						<br>
						<c:if test="${ad.startTime != null }" >
							<strong>Start Time: </strong>
							<p><em><c:out value=" ${ad.startTime }"/></em></p>
						</c:if>
						<br>
						<c:if test="${ad.endTime != null }" >
							<strong>End Time: </strong>
							<p><c:out value=" ${ad.endTime }"/></p>
						</c:if>
						<br>
				        <strong>Comments</strong>
<!-- 				        <div class="comments-section">
 -->				  	<c:set var="adId" value="${ad.adId }"/>
				      	<c:set var="commentList" value="${comments[adId]}"/>
				      	<c:set var="commentCount" value="0" />
				      		<c:forEach var="comment" items="${commentList }">
								<c:choose>
									<c:when test="${commentCount % 2 == 0 }">
										<div class="comments-section bg-light">
											<p>${comment.comment}</p>
					      					<p style="margin-bottom: 0">POSTED BY: ${comment.contactEmail } </p>
					      				</div>
									</c:when>
									<c:otherwise>
										<div class="comments-section">
							      			<p>${comment.comment}</p>
							      			<p style="margin-bottom: 0">FROM: ${comment.contactEmail } </p>
							      		</div>
									</c:otherwise>
								</c:choose>
					      		<c:set var="commentCount" value="${commentCount + 1 }" />
				      		</c:forEach>
<!-- 				        </div>
 -->				   </div>
				      <div class="modal-footer bg-info" id="${ad.adId }-comments">
				      	<c:url var="addComment" value="/bulletinboard/ad"/>
						<form method="POST" action="${addComment}" style="width: 100%">
							<input type="hidden" name="adId" value="${ad.adId}"/>
						    <div class="form-group">
						        <input type="text" class="form-control" value="" name="comment" id="formGroupExampleInput" placeholder="Enter a Comment">
						    </div>
						    <div class="form-group">
						  	  <input type="submit" class="btn btn-white text-center" value="submit comment" />
						    </div>
						</form>
				      </div>
				    </div>
				  </div>
				</div>
			<c:set var="count" value="${count + 1}" />
		</c:forEach>
	</div>
	<div class="container hidden post-job"> 
		<c:url var="postAd" value="bulletinboard/newad" />
		<form action=${postAd } method="POST" id="jobSkillPost">
			<div class="form-group">
				<Strong>Are you posting a Job, Skill or Crowdfunding Campaign: </Strong>
				<label for="job-radio">Job</label>
				<input type="radio" value="job" name="adType" checked id="job-radio" />
				
				<label for="skill-radio">Skill</label>
				<input type="radio" value="skill" name="adType" id="skill-radio" />
				
				<label for="campaign-radio">Campaign</label>
				<input type="radio" value="campaign" name="adType" id="campaign-radio" />
				
			</div>
		    <div class="form-group">
		        <label for="formGroupExampleInput">Give your post a title</label>
		        <input type="text" class="form-control" name="title" value="" id="formGroupExampleInput" placeholder="Enter Some Text">
		    </div>
		    <div class="form-group">
		        <label for="formGroupExampleInput2">Give your post a description</label>
		        <input type="text" class="form-control" name="description" id="formGroupExampleInput2" placeholder="Enter Some Text">
		    </div>
		    <div class="form-group hidden" id="campaign">
		    	<strong>Enter a goal amount</strong>
		    	<input type="text" name="goal" value="0.0" placeholder="$20.00" />
		    </div>
		    <jsp:useBean id="now" class="java.util.Date" />
			<fmt:formatDate var="date" value="${now}" pattern="yyyy-MM-dd" />
		    <div class="form-group" id="not-campaign">
		    	<strong>Enter a start date(yyyy-mm-dd)</strong>
		    	<input type="date" name="startDate" value="${date}" placeholder="YYYY-MM-DD" />
		    </div>
		    <div class="form-group barter-yes-no" id="not-campaign">
		    	<strong>Are you Open to Bartering</strong>
		    	<label for="yes">Yes</label>
		    	<input type="radio" value="true" id="yes" name="barter" />
		    	
		    	<label for="no">No</label>
		    	<input type="radio" value="false" id="no" name="barter" />
		    </div>
		    <div class="form-group hidden " id="compensation-div">
		    	<label for="compensation">Please enter desired compensation</label>
		    	<input type="text" class="form-control" value=0 id="compensation" name="payDollars" placeholder="2.99">
		    </div>
		    <div class="form-group hidden " id="bartering-div">
		    	<label for="barter">What Would you like to Barter?</label>
		    	<input type="text" class="form-control" value="" name="barterDescription" />
		    </div>
			<div class="form-group hidden " id="submit">
				<input type="submit" class="btn btn-info" value="Submit Form" />
			</div>
		</form>
	</div>
	<div class="container hidden see-campaigns">
		<c:set var="modalCount" value="0"/>
		<c:forEach var="campaign" items="${allCampaigns}">
			<c:url var="linkToProfile" value="/dashboard/profile?businessId=${campaign.kidId }" />
			<c:set var="campaignKidId" value="${campaign.kidId }" />
			<c:set var="kid" value="${campaignKid[campaignKidId]}" />
			<div class="row" style=" padding-top: 1em; padding-bottom: 1em; margin-bottom: 1em; text-decoration: none; border-bottom: 2px solid lightblue">
				<div class="col-sm-8">
				<div class="">
					<div class="col-sm-4" style="padding-left: none; display: inline-block; vertical-align: top;">
						<c:url var="logo" value="${kid.logoFileName}" />
						<img src="${logo }" class="img-thumbnail" alt="business logo" />
					</div>
					<div class="col-sm-7" style="display: inline-block; padding-right: none">
						<div style="display: inline-block">
							<div>
								<strong>${kid.businessName }</strong>
							</div>
							<div>
								<strong>${campaign.title }</strong>
							</div>
							<div class="text-secondary">
								<strong>current donations: <fmt:formatNumber value="${campaign.current }" type="currency"/></strong>
							</div>
							<div class="text-secondary">
								<strong>goal: <fmt:formatNumber value="${campaign.goal }" type="currency"/></strong>
							</div>
						</div>
						<div class="col-sm-2 float-right" style="display: inline-block">
							<a href="${linkToProfile }"><button class="btn btn-info">View Profile</button></a>
							<button class="btn btn-info" data-toggle="modal" data-target="#donateModel${modalCount }" style="margin-top: 1em;">Donate</button>
						</div>
						<div class="progress" style="margin-top: .5em">
							<c:set var="math" value="${(campaign.current/campaign.goal) * 100 }" />
							<div class="progress-bar progress-bar-striped bg-warning progress-bar-animated" role="progressbar" style="width:${math}%" aria-valuenow="${campaign.current }" aria-valuemin="0" aria-valuemax="${campaign.goal }"></div>
						</div>
						<div class="modal" id="donateModel${modalCount }" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title text-info" id="exampleModalLabel">Make a Campaign Donation: ${campaign.title }</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <c:url var="donate" value="/dashboard/profile" />	
							      <form method="POST" action="${donate }" >
							      <div class="modal-body">
							      	<input type="hidden" name="toKid" value="${campaign.kidId}" />
							      		<div class="form-group">
										    <input type="text" class="form-control" id="amount" placeholder="Donation Amount" name="amount">
									  	</div>
							      		<h6>To make this donation, you must login with your business account.</h6>
							      		<div class="form-group">
										    <input type="text" class="form-control" id="username" placeholder="Username" name="username">
									  	</div>
									  	<div class="form-group">
										    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
									  	</div>
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							        <input type="submit" value="submit" class="btn btn-success" />
							      </div>
							      </form>
							    </div>
							  </div>
							</div>
					</div>
<%-- 					<div class="col-sm-2 float-right" style="display: inline-block">
						<a href="${linkToProfile }"><button class="btn btn-info">View Profile</button></a>
						<button class="btn btn-info">Donate</button>
					</div> --%>
				</div>
				</div>
			</div>
			<c:set var="modalCount" value="${modalCount + 1 }" />
		</c:forEach>
	</div>
	<div class="container hidden post-skill">
		<p>this is probably super pointless and can just be in the above one with the jobs!</p>
	</div>
	
 
 <c:import url="/WEB-INF/jsp/footer.jsp" />
