<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:url var="createAdmin" value="/dashboard/admin/create_admin"/>

<div class="container-fluid">
	<div class="row">
		<nav class="col-md-2 col-xs-1 d-none d-md-block bg-light sidebar" style="margin-top:2em">
			<div class="sidebar-sticky">
				<h5 class="text-center">Reports</h5>
				<hr>
				<!-- This section defines the pill tabs that toggle the content on the right side of the page
					The ID is referenced in the div that actually holds the content below. More can be added
					using the "a" elements as a template -->
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				  <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-pending-parent-approval" role="tab" aria-controls="v-pills-pending-parent-approval" aria-selected="true">Kids Pending Changes</a>
				  <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-historical-changes" role="tab" aria-controls="v-pills-historical-changes" aria-selected="false">Historical Changes</a>
				</div>
			</div>
		</nav>
		<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
			<div class="row">
				<div class="col-sm-4">
					<div class="card text-center">
		  				<div class="card-body">
		    				<h5 class="card-title">Users</h5>
		    				<div class="container-fluid tab-pane  show active" id="v-pills-all-users" role="tabpanel" aria-labelledby="v-pills-all-users-tab">
		    					<h1 data-toggle="tab" class="card-text"><c:out value="${totalUsers}"/></h1>
		    				</div>
		    				<div class="container-fluid tab-pane  hidden" id="v-pills-child" role="tabpanel" aria-labelledby="v-pills-child-tab">
		    					<h1 data-toggle="tab" class="card-text"><c:out value="${childUsers}"/></h1>
		    				</div>
		    				<div class="container-fluid tab-pane  hidden" id="v-pills-guardian" role="tabpanel" aria-labelledby="v-pills-guardian-tab">
		    					<h1 data-toggle="tab" class="card-text "><c:out value="${guardianUsers}"/></h1>
		    				</div>
		    				<div class="container-fluid tab-pane  hidden" id="v-pills-admin" role="tabpanel" aria-labelledby="v-pills-admin-tab">
		    					<h1 data-toggle="tab" class="card-text"><c:out value="${adminUsers}"/></h1>
		    				</div>
		    				<div class="nav flex-column nav-pills" id="v-pills-user_count tab" role="tablist" aria-orientation="vertical">
				  				<a class="nav-link active" id="v-pills-all_users-tab" data-toggle="pill" href="#v-pills-all-users" role="tab" aria-controls="v-pills-all-users" aria-selected="true">All</a>
				 				<a class="nav-link" id="v-pills-child-tab" data-toggle="pill" href="#v-pills-child" role="tab" aria-controls="v-pills-child" aria-selected="false">Child</a>
								<a class="nav-link" id="v-pills-guardian-tab" data-toggle="pill" href="#v-pills-guardian" role="tab" aria-controls="v-pills-guardian" aria-selected="false">Guardian</a>
								<a class="nav-link" id="v-pills-admin-tab" data-toggle="pill" href="#v-pills-admin" role="tab" aria-controls="v-pills-admin" aria-selected="false">Admin</a>
							</div>		  				
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card text-center">
		  				<div class="card-body">
		    				<h5 class="card-title">Pending Moderation</h5>
		    				<h1 class="card-text alert">0</h1>
		    				<a href="#" class="btn btn-primary bg-info">Feature Not Enabled</a>
		  				</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card text-center">
		  				<div class="card-body">
		    				<h5 class="card-title">Account Management</h5>
		    				<div class="btn-group-vertical">
			    				<a href="#" class="btn btn-primary bg-info" data-toggle="modal" data-target="#viewAccountModal">View</a>
			    				<a href="${createAdmin}" class="btn btn-primary bg-success">Create</a>
			    				<a href="#" class="btn btn-primary bg-confirm" data-toggle="modal" data-target="#reinstateModal">Reinstate</a>
			    				<a href="#" class="btn btn-primary bg-warning" data-toggle="modal" data-target="#suspendModal">Suspend</a>
			    				<a href="#" class="btn btn-primary bg-danger" data-toggle="modal" data-target="#disableModal">Disable</a>
			    			</div>
		  				</div>
					</div>
				</div>
			</div>
			<!-- MODAL -->
			<div class="modal fade" id="viewAccountModal" tabindex="-1" role="dialog" aria-labelledby="viewAccountModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <c:url var="results" value="/dashboard/admin/results"/>
			    <form method="POST" action="${results}">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Account Search</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      	
					<div class="form-group">
						<input type="text" class="form-control" id="searchQueryUsername" name="query" placeholder="Username">
						<input type="hidden" name="control" value="1">
					</div>
					<div class="form-group">
						<label for="searchQueryRole">User Type</label>
					    <select multiple class="form-control" name="role" id="searchQueryRole">
					    	<option value="child" selected="selected">Child</option>
					    </select>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <input type="hidden" name="view">
			        <input type="submit" class="btn btn-primary" value="Search">
			      </div>
				</div>
			    </form>
			 </div>
			</div>
			</div>
			<div class="modal fade" id="reinstateModal" tabindex="-1" role="dialog" aria-labelledby="reinstateAccountModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <c:url var="results" value="/dashboard/admin/results"/>
			    <form method="POST" action="${results}">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Account Search</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      	
					<div class="form-group">
						<input type="text" class="form-control" id="searchQueryUsername" name="query" placeholder="Username">
						<input type="hidden" name="control" value="3">
					</div>
					<div class="form-group">
						<label for="searchQueryRole">User Type</label>
					    <select multiple class="form-control" name="role" id="searchQueryRole">
					    	<option value="child">Child</option>
					    </select>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <input type="submit" class="btn btn-primary" value="Search">
			      </div>
				</div>
			    </form>
			 </div>
			</div>
			</div>
			<div class="modal fade" id="suspendModal" tabindex="-1" role="dialog" aria-labelledby="suspendModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <c:url var="results" value="/dashboard/admin/results"/>
			    <form method="POST" action="${results}">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Account Search</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      	
					<div class="form-group">
						<input type="text" class="form-control" id="searchQueryUsername" name="query" placeholder="Username">
						<input type="hidden" name="control" value="4">
					</div>
					<div class="form-group">
						<label for="searchQueryRole">User Type</label>
					    <select multiple class="form-control" name="role" id="searchQueryRole">
					    	<option value="child">Child</option>
					    </select>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <input type="submit" class="btn btn-primary" value="Search">
			      </div>
				</div>
			    </form>
			 </div>
			</div>
			<div class="modal fade" id="disableModal" tabindex="-1" role="dialog" aria-labelledby="disableModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <c:url var="results" value="/dashboard/admin/results"/>
			    <form method="POST" action="${results}">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Account Search</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      	
					<div class="form-group">
						<input type="text" class="form-control" id="searchQueryUsername" name="query" placeholder="Username">
						<input type="hidden" name="control" value="5">
					</div>
					<div class="form-group">
						<label for="searchQueryRole">User Type</label>
					    <select multiple class="form-control" name="role" id="searchQueryRole">
					    	<option value="child">Child</option>
					    </select>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <input type="submit" class="btn btn-primary" value="Search">
			      </div>
				</div>
			    </form>
			 </div>
			</div>
			<!-- END MODAL -->
			<hr>
			<div class="tab-content" id="v-pills-tabContent">
				<!-- Here the classes tab-pane, fade, show, and active as well as the ID and role define this as the
					div containing the section that will be toggled when the pill tab is selected. Aria is for screen 
					readers -->
				<div class="container-fluid tab-pane fade show active" id="v-pills-pending-parent-approval" role="tabpanel" aria-labelledby="v-pills-pending-parent-approval-tab">
					<h3>Requests Awaiting Parent Approval</h3>
					<table class="table table-striped table-bordered">
						<tr>
							<th scope="col">Pending Request ID</th>
							<th scope="col">Username</th>
							<th scope="col">Kid ID</th>
							<th scope="col">Guardian Username</th>
							<th scope="col">Column to Change</th>
							<th scope="col">Current Value</th>
							<th scope="col">Requested Value</th>
							<th scope="col">Created</th>
						</tr>
						<c:forEach var="pendingChange" items="${pendingChanges}">
							<tr>
								<th scope="row"><c:out
										value="${pendingChange.pendingRequestId}" /></th>
								<td><c:out value="${pendingChange.kidUserName}" /></td>
								<td><c:out value="${pendingChange.kidId}" /></td>
								<td><c:out value="${pendingChange.guardianUserName}" /></td>
								<td><c:out value="${pendingChange.columnName}" /></td>
								<td><c:out value="${pendingChange.currentValue}" /></td>
								<td><c:out value="${pendingChange.requestedValue}" /></td>
								<td><c:out value="${pendingChange.created}" /></td>
							</tr>
						</c:forEach>
					</table>
				</div>
				<div class="container-fluid tab-pane fade" id="v-pills-historical-changes" role="tabpanel" aria-labelledby="v-pills-historical-changes-tab">
					<h3>Historical Changes</h3>
					<table class="table table-striped table-bordered">
						<tr>
							<th>Change ID</th>
							<th>User Initiated By</th>
							<th>Initiator's Role</th>
							<th>Change to User</th>
							<th>Changed Column</th>
							<th>Old Value</th>
							<th>New Value</th>
							<th>Request Time</th>
							<th>Complete Time</th>
						</tr>
						<c:forEach var="historicalChange" items="${historicalChanges}">
							<tr>
								<th scope="row"><c:out
										value="${historicalChange.historicalChangeId}" /></th>
								<td><c:out value="${historicalChange.changeByUserName}" /></td>
								<td><c:out value="${historicalChange.changeInitiatedByRole}" /></td>
								<td><c:out value="${historicalChange.changeToUserName}" /></td>
								<td><c:out value="${historicalChange.changedColumn}" /></td>
								<td><c:out value="${historicalChange.oldValue}" /></td>
								<td><c:out value="${historicalChange.newValue}" /></td>
								<td><c:out value="${historicalChange.requestTime}" /></td>
								<td><c:out value="${historicalChange.completeTime}" /></td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</main>
	</div>
</div>

<c:import url="/WEB-INF/jsp/footer.jsp" />