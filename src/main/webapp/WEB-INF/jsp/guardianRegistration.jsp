<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


 <c:import url="/WEB-INF/jsp/header.jsp" />

 <div class="main-content">
 	<div class="container">
 		<div class="col-sm-9">
 			<c:url var="guardianRegistration" value="/guardianRegistration" />
 			<form:form action="${guardianRegistration }"  method="post" id="profileRegistration" modelAttribute="currentUser">
 			 	<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
 			 	<div>
 					<h3>Co-founder Profile</h3>
 				</div>
 				<div style="margin-top:.5em">
 					<p>First Name</p>
 				 	<form:input path="firstName" name="firstName" />
 				 	<form:errors path="firstName" cssClass="error"/>
 				</div>
 				<div style="margin-top:.5em">
 					<p>Last Name</p>
 					<form:input path="lastName" name="lastName" />
 					<form:errors path="lastName" cssClass="error"/>
 				</div>
 				<div style="margin-top:.5em">
 					<p>Date Of Birth yyyy-dd-mm</p>
 		 			<form:input type="date" path="dateOfBirth" name="dateOfBirth" />
 		 			<form:errors path="dateOfBirth" cssClass="error"/>
 		 			
 				</div> 
 				<div style="margin-top:.5em">
 					<p>Email Address</p>
 					<form:input path="email" name="emailAddress" />
 					<form:errors path="email" cssClass="error"/>
 				</div>
 				<div>
 					<input type="checkbox" name="above18" value=""> I certify I am 18 years or older<br>
 				</div>
 				<br>
 				<input type="submit" value="submit"/>
 			</form:form>
 		</div>
 	</div>
 	<br>
	<div class="container">
	 	<div class="col-sm-4">
			<div class="progress ">
				<div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
			</div>
		</div>
	</div>
 
 </div>
 
 
  <c:import url="/WEB-INF/jsp/footer.jsp" />
 