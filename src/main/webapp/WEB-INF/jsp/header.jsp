<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
	<head>
	  <title>UE kids</title>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  
	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	  
	  <c:url var="css" value="/css/style.css" />
	  <link rel="stylesheet" type="text/css" href="${css}"/>
	  
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<c:url var="validation1" value="/js/jquery.min.js" />
	<script src="${validation1}" defer></script>
	<c:url var="validation2" value="/js/jquery.validate.min.js" />
	<script src="${validation2}" defer></script>
	</head>
	<body>
<div class="row">
<div class="col-sm-12" >

<!--Navbar-->
<nav class="navbar navbar-dark indigo darken-2 bg-info shadow-lg">
	<c:url var="logo" value="/img/UpperHand_Logo.png" />
	<c:url var="login" value="/"/>
	<a class="nav-link" href="${login }"><img src="${logo}" /></a>
  <!-- Navbar brand -->
    <div class="col-sm-3">
	  	<c:url var="profile" value="/dashboard"/>
	    <a class="nav-link text-white navbar-link" style="display: inline-block" href="${profile}">My Profile</a>
	    
	    <c:url var="jobs" value="/bulletinboard" />
	    <a class="nav-link text-white navbar-link" style="display: inline-block;" href="${jobs }">Jobs</a>
	    
	    <a class="nav-link text-white navbar-link" style="display: inline-block" href="${login }">Log Out</a>
	</div>
<!--   </div>
 -->  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->
</div>
</div>
