<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

 <c:import url="/WEB-INF/jsp/header.jsp" />
 
 <div class="main-content">
 	<div class="container">
 	 		<div class="col-sm-12">
		 		<c:url var="image" value="/img/accountRequest.jpg" />
		 		<img src="${image}" alt="kids" style="width:100%" />
		 	</div>		 	
 	</div>
 	<div class="container">
 		<div class="col-sm-9">
			<div>
				<h3>Before starting your business, you need a co-founder!</h3>
				<h5><em>Enter your name and guardian's email below to register an account!</em></h5>
			</div>
			<div>
	 			<c:url var="accountRequest" value="/accountRequest1" />
	 			<form action="${accountRequest }"  method="GET" id="requestForm">
	 			 	<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
	 				<div class="form-group">
				        <label for="firstName"><strong>Your First Name</strong></label>
				        <input type="text" class="form-control" name="firstName" value="" id="firstName" placeholder="Enter Some Text">
			    	</div>
			    	<div class="form-group">
				        <label for="lastName"><strong>Your Last Name</strong></label>
				        <input type="text" class="form-control" name="lastName" value="" id="lastName" placeholder="Enter Some Text">
			    	</div>
			    	<div class="form-group">
				        <label for="email"><strong>Guardian Email Address</strong></label>
				        <input type="text" class="form-control" name="email" value="" id="email" placeholder="Enter Some Text">
			    	</div>
			    	<div>
	 					<input type="submit" value="submit"/>
	 				</div>
	 			</form>
	 		</div>
 		</div>
 	</div>
 
 </div>
 
 
 <c:import url="/WEB-INF/jsp/footer.jsp" />