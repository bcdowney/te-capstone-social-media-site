<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

 <c:import url="/WEB-INF/jsp/header.jsp" />

<c:url var="login" value="/login" />
<c:url var="homepage1" value="/img/homepage1.jpg" />
<c:url var="homepage2" value="/img/homepage2.jpg" />
<c:url var="homepage3" value="/img/homepage3.jpg" />
<c:url var="homepage4" value="/img/homepage4.jpg" />

<div style="display:flex">

	<div style="width:30%; margin:2em">
	<form method="post" action="${login}" id="mainLogin" >
		<div>
		 	<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
			<input type="text" value="" name="username" placeholder="Username">
		</div>
		<div>
			<input type="password" value="" name="password" placeholder="Password">
		</div>
		<div class="crowdfunding-profile">
			<input type="submit" value="submit" />
		</div>
	</form>
	<div class="crowdfunding-profile">
		 <c:url var="kidRegister" value="/accountRequest" />
		 Want to join the fun? <a href="${kidRegister}">Create an account</a>
	</div>
	
	<div>
		 <c:url var="userRegistration" value="/userRegistration" />
		 Are you a co-founder? <a href="${userRegistration}">Register your child</a>
	</div> 
	
	
	<c:url var="loginFAKE" value="/loginFAKE" />
	
	<div class="crowdfunding-profile">
	 <form method="post" action="${loginFAKE}">
	 	<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
	 	<input type="hidden" value="1" name="kidId"/>
		<input type="submit" value="kid"/> 	
	 </form>
	 </div>
	 
	 <div class="crowdfunding-profile">
	 <form method="post" action="${loginFAKE}">
	 	<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
	 	<input type="hidden" value="1" name="guardianId" />
	 	<input type="submit" value="guardian" />
	 </form>
	</div>
	
	<div class="crowdfunding-profile">
	 <form method="post" action="${loginFAKE}">
	 	<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
	 	<input type="hidden" value="3" name="userId" />
	 	<input type="submit" value="Admin" />
	 </form>
	 </div>
	 
	 <div class="crowdfunding-profile">
	 <form method="post" action="${loginFAKE}">
	 	<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
	 	<input type="hidden" value="2" name="kidId" />
	 	<input type="hidden" value="1" name="businessId"/>
	 	<input type="submit" value="public profile" />
	 </form>
	 </div>
</div>

<div style="width:70%">
	<div id="carousel" class="carousel slide" data-ride="carousel">
	  <div class="carousel-inner">
	    <div class="carousel-item active">
	      <img class="d-block w-100" src="${homepage1 }" alt="First slide">
	    </div>
	    <div class="carousel-item">
	      <img class="d-block w-100" src="${homepage2 }" alt="Second slide">
	    </div>
	    <div class="carousel-item">
	      <img class="d-block w-100" src="${homepage3 }" alt="Third slide">
	    </div>
	     <div class="carousel-item">
	      <img class="d-block w-100" src="${homepage4 }" alt="Third slide">
	    </div>
	  </div>
	</div>
</div>
 </div>
 
 <c:import url="/WEB-INF/jsp/footer.jsp" />
 