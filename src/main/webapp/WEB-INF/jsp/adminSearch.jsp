<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:if test="${control == 1}">
	<h3>Results</h3>
	<table class="table table-striped table-bordered">
		<tr>
			<th scope="col">User ID</th>
			<th scope="col">Username</th>
			<th scope="col">Date Created</th>
			<th scope="col">Last Modified</th>
			<th></th>
		</tr>
		<c:forEach var="result" items="${results}">
			<tr scope="row">
				<td><c:out value="${result.key.userId}" /></td>
				<c:url value="/dashboard/profile" var="profilePage">
					<c:param name="businessId" value="${result.value.kidId}"/>
				</c:url>
				<td><a href="${profilePage}" target="_blank"><c:out value="${result.key.userName}"/></a></td>
				<td><c:out value="${result.key.dateCreated}"/></td>
				<td><c:out value="${result.key.lastModified}"/></td>
				<td><form action="${profilePage}" method="GET">
					<input type="hidden" value="${result.value.kidId}" name="businessId">
					<input type="submit" value="View" class="btn btn-secondary bg-info">
				</form></td>
			</tr>
		</c:forEach>
	</table>
</c:if>
<c:if test="${control == 2}">	
	<h3>New Admin Creation</h3>
</c:if>
<c:if test="${control == 3}">	
	<h3>Results</h3>
	<table class="table table-striped table-bordered">
		<tr>
			<th scope="col">User ID</th>
			<th scope="col">Username</th>
			<th scope="col">Date Created</th>
			<th scope="col">Last Modified</th>
			<th></th>
		</tr>
		<c:forEach var="disabled" items="${disabledUsers}">
			<tr scope="row">
				<td><c:out value="${disabled.userId}" /></td>
				<c:url value="/dashboard/profile" var="profilePage">
					<c:param name="businessId" value="1"/>
				</c:url>
				<td><a href="${profilePage}" target="_blank"><c:out value="${disabled.userName}"/></a></td>
				<td><c:out value="${disabled.dateCreated}"/></td>
				<td><c:out value="${disabled.lastModified}"/></td>
				<td><form action="${profilePage}" method="GET">
					<input type="hidden" value="" name="businessId">
					<input type="submit" value="Reinstate" class="btn btn-secondary bg-primary">
				</form></td>
			</tr>
		</c:forEach>
	</table>
</c:if>
<c:if test="${control == 4}">	
	<h3>Results</h3>
	<table class="table table-striped table-bordered">
		<tr>
			<th scope="col">User ID</th>
			<th scope="col">Username</th>
			<th scope="col">Date Created</th>
			<th scope="col">Last Modified</th>
			<th></th>
		</tr>
		<c:forEach var="result" items="${results}">
			<tr scope="row">
				<td><c:out value="${result.key.userId}" /></td>
				<c:url value="/dashboard/profile" var="profilePage">
					<c:param name="businessId" value="${result.value.kidId}"/>
				</c:url>
				<td><a href="${profilePage}" target="_blank"><c:out value="${result.key.userName}"/></a></td>
				<td><c:out value="${result.key.dateCreated}"/></td>
				<td><c:out value="${result.key.lastModified}"/></td>
				<td><form action="${profilePage}" method="GET">
					<input type="hidden" value="${result.value.kidId}" name="businessId">
					<input type="submit" value="Suspend" class="btn btn-secondary bg-warning">
				</form></td>
			</tr>
		</c:forEach>
	</table>
</c:if>
<c:if test="${control == 5}">	
	<h3>Results</h3>
	<table class="table table-striped table-bordered">
		<tr>
			<th scope="col">User ID</th>
			<th scope="col">Username</th>
			<th scope="col">Date Created</th>
			<th scope="col">Last Modified</th>
			<th></th>
		</tr>
		<c:forEach var="result" items="${results}">
			<tr scope="row">
				<td><c:out value="${result.key.userId}" /></td>
				<c:url value="/dashboard/profile" var="profilePage">
					<c:param name="businessId" value="${result.value.kidId}"/>
				</c:url>
				<td><a href="${profilePage}" target="_blank"><c:out value="${result.key.userName}"/></a></td>
				<td><c:out value="${result.key.dateCreated}"/></td>
				<td><c:out value="${result.key.lastModified}"/></td>
				<td><form action="${profilePage}" method="GET">
					<input type="hidden" value="${result.value.kidId}" name="businessId">
					<input type="submit" value="Delete" class="btn btn-secondary bg-danger">
				</form></td>
			</tr>
		</c:forEach>
	</table>
</c:if>

<c:import url="/WEB-INF/jsp/footer.jsp" />