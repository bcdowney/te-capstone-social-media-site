<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


 <c:import url="/WEB-INF/jsp/header.jsp" />
  <div class="main-content">
  <div class="container">
 	 		<div class="col-sm-12">
		 		<c:url var="image" value="/img/accountRequest.jpg" />
		 		<img src="${image}" alt="kids" style="width:100%" />
		 	</div>		 	
 	</div>
 	<div class="container">
 		<div class="col-sm-12">
 			<c:url var="userInput" value="/userInput" />
 			<form:form action="${userInput }"  method="post" id="userForm" modelAttribute="user">
 			 	<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
 			 	<div>
 			 		<c:if test="${param.firstName != null }" >
 					<h3><c:out value="${param.firstName}" />&nbsp<c:out value="${param.lastName}" /> wants to start a business and needs you to be a co-founder!</h3>
 					</c:if>
 					<h5><em>Please enter the fields below to create a co-founder account.</em></h5>
 					<h5><em>Upon completion, your child will be able to register his/her business account</em></h5>
 				</div>
 				<br>
 				<div>
 					<p>Username</p>
 					<form:input path="userName" name="userName" />
 					<form:errors path="userName" cssClass="error"/>
 				</div>
 				<div>
 					<p>Password</p>
 					<form:input type="password"  path="password" name="password" />
 					<form:errors path="password" cssClass="error"/>
 				</div>
 				<div>
 					<p>Confirm Password</p>
 					<form:input type="password" path="confirmPassword" name="confirmPassword" />
 					<form:errors path="passwordMatching" cssClass="error"/>
 				</div>
 				<br>
 				<div>
	 				<input type="hidden" name="role" value="guardian">
	 				<input type="submit" value="submit"/>
	 			</div>
 			</form:form>
 		</div>
 	</div>
 	<br>
	<div class="container">
	 	<div class="col-sm-4">
			<div class="progress ">
				<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
			</div>
		</div>
	</div>
 </div>
 
 
 
 
 
 
   <c:import url="/WEB-INF/jsp/footer.jsp" />
 