<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

 <c:import url="/WEB-INF/jsp/header.jsp" />

<div class="container skills-container">
	<div class="card shadow">
		<div class="card-header bg-info text-white">
		    <h2>Add or Remove Skills from your Current Set</h2>
		</div>
		<div class="card-body">
			<ul class="list-group" >
				<c:forEach var="skill" items="${sessionScope.skillList }">
					<c:set var="count" value="0" />
					<c:forEach var="kidSkill" items="${sessionScope.skills}">
						<c:if test="${kidSkill.name == skill.name}">
							<c:set var="count" value="1" />
	 					</c:if>
					</c:forEach>
					<c:choose>
						<c:when test="${count > 0 }">
							<li class="list-group-item list-group-item-success skill" id="${skill.skillId}"> ${skill.name} </li>
						</c:when>
						<c:otherwise>
							<li class="skill list-group-item" id="${skill.skillId }"> ${skill.name} </li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</ul>
		</div>
		<c:url var="skillsPost" value="/skills"/>
		<form method="POST" action="">
			<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
			<div id="skillForm">
				<c:set var="currentSkills" value="" />
				<c:forEach var="kidSkill" items="${sessionScope.skills}"> 
					<c:set var="currentSkills" value="${currentSkills},${kidSkill.skillId }" />
				</c:forEach>
				<input type="hidden" name="skillsList" id="input-skills" value="${currentSkills}" />
 			</div>
 			<div class="col-sm-12">
				<input type="submit" value="submit" class="center-block btn btn-lg bg-primary skills-btn text-white text-center" >
			</div>
		</form>
	</div>
</div>


 <c:import url="/WEB-INF/jsp/footer.jsp" />
 