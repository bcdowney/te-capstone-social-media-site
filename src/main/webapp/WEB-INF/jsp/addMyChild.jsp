<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib  uri="http://www.springframework.org/tags" prefix="spring" %>


 <c:import url="/WEB-INF/jsp/header.jsp" />
 
 <div class="main-content">
 	<div class="container">
 		<div class="col-sm-9">
 			<c:url var="addChildUser" value="/addChildUser" />
 			<form:form action="${addChildUser }"  method="post" id="userForm" modelAttribute="kidUser">
 			 	<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
 			 	<div>
 					<h3>Add a child account</h3>
 				</div>
 				<div style="margin-top:.5em">
 					<p>Username</p>
 					<form:input path="userName" name="userName"/>
 					<form:errors path="userName" cssClass="error"/>
 				</div>
 				<div style="margin-top:.5em">
 					<p>Password</p>
 				 	<form:input type="password" path="password" name="password"/>
 				 	<form:errors path="password" cssClass="error"/>
 				</div>
 				<div style="margin-top:.5em">
 					<p>Confirm Password</p>
 					<form:input type="password" path="confirmPassword" name="confirmPassword" />
 					<form:errors path="passwordMatching" cssClass="error"/>
 				</div>
 				<div style="margin-top:.5em">
 					<input type="checkbox" name="guardian" value="guardian"> I certify I am the legal guardian of this account<br>
 				</div>
 				<br>
 				 <input type="hidden" name="role" value="kid">
 				<input type="submit" value="submit"/>
 			</form:form>
		</div>
	</div>
	<br>
	<div class="container">
	 	<div class="col-sm-4">
			<div class="progress ">
				<div class="progress-bar bg-info" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
			</div>
		</div>
	</div>
</div>

<c:import url="/WEB-INF/jsp/footer.jsp" />
