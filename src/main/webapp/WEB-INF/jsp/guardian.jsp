<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

 <c:import url="/WEB-INF/jsp/header.jsp" />
 	
 	<c:url var="submitChange" value="/dashboard/guardian"/>
 	
 <div class="container main-content">	
	<ul class="nav nav-pills">
	  <li class="nav-item">
	    <a class="nav-link active" id="profile-changes-tab" href="#!">Profile Changes</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" id="job-comments-tab"  href="#!">Job/Comments</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" id="financials-tab" href="#!">Financials</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" id="add-child-tab" href="#!">Add a Child Account</a>
	  </li>
	</ul>
	<div class="profile-picture shadow-sm">
				<c:url var="updatePic" value="/upload"/>
				<a href="${updatePic}">Click to update child's picture</a>
			</div>
	<div class="">
	
<!-- 	profile change requests  -->
 		<div id="profile-change-requests">
 		<c:set var="changeCount" value="0" />
 		<c:forEach var="change" items="${pendingChanges}">
		 	<div class="container">
		 		<div class="row" id="${change.columnName}" style="margin-top: 1em; border-bottom: 2px solid lightblue">
		 			<div class="col-sm-1"></div>
		 				<div class="col-sm-9 ">
		 					<c:forEach var="kid" items="${listOfKids}">
		 						<c:if test="${kid.kidId == change.kidId}">
		 							<h4 class="hidden" id="kidFirstName"><c:out value="${kid.firstName}"/></h4>
		 						</c:if>
		 					</c:forEach>
		 					<c:set var="columnLogo" value="logoFileName"/>
		 					<c:choose>
			 					<c:when test="${change.columnName == columnLogo}">
			 						<div class="${change.columnName}" id="messageToParents${changeCount}" style="margin: auto;"></div>
			 						<img src="${change.currentValue}" class="hidden" id="currentValue${changeCount}" alt="current logo"/>
			 						<img src="${change.requestedValue}" class="hidden" id="requestedValue${changeCount}" alt="requested logo"/>
			 						<p class="hidden" id="columnName${changeCount }">${change.columnName} </p>
			 					</c:when>
			 					<c:otherwise>
		 				
 				 					<div class="${change.columnName }" id="messageToParents${changeCount}" style="margin: auto;"></div>
				 					
				 					<p class="hidden" id="currentValue${changeCount}">${change.currentValue}</p>
				 					<p class="hidden" id="requestedValue${changeCount }">${change.requestedValue}</p>
				 					<p class="hidden" id="columnName${changeCount }">${change.columnName} </p> 
			 					</c:otherwise>
		 					
		 					</c:choose>

		 					<div class="approveDeny-profile" style="display: inline; padding: auto;">
								<form method="post" action="${submitChange}" class="approve-deny-form-profilePage">
									<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN"/>
									<input type="hidden" value="${change.pendingRequestId}" name="pendingChangeId"/>
 									<input type="hidden" value="false" name="approved" id="approveOrDeny-profile" />
 									<input type="submit" class="btn bg-success btn-sm skills-btn text-white approve-profile" value="APPROVE" style="display: inline-block; width: 30%; margin-right: 3em;"/>		
 									<input type="submit" class="text-white btn bg-danger btn-sm skills-btn deny-profile" value="DENY" style="display: inline-block; width: 30%; " />
								</form>
							</div>
							<div class="approveDeny" style="display: inline">

							</div>
		 				</div>
		 			</div>
		 		</div> 
		 		<c:set var="changeCount" value="${changeCount + 1 }" />
 			</c:forEach>
 			
 			<p class="hidden" id="changeCount"> ${changeCount } </p>
 			
 			</div>
 			
 			<!-- transaction/Campaign approvals -->
 			
 			<div class="hidden" id="financial-approvals">
 				<div class="btn-group" id="financialApprovals" role="group" aria-label="Basic example">
				    <button type="button" class="btn btn-outline-info transactionsToggleButton">Transactions</button>
				    <button type="button" class="btn btn-outline-info campaignsToggleButton">Campaigns</button>
				    <button type="button" class="btn btn-info setLimitsToggleButton">Set Transaction Limits</button>
				</div>
				<div class="container hidden" id="limits">
					<c:forEach var="kid" items="${listOfKids}">
						<p>Current daily transaction amount limit - <fmt:formatNumber value="${kid.transactionAmountLimit}" type="currency"/></p>
						<p>Current daily transaction limit - <c:out value="${kid.transactionsPerDay}"/></p>
						<form method="post" action="${submitChange}" class="approve-new-limits">
							<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN"/>
							<input type="hidden" value="${kid.kidId}" name="kidId"/>
							<input type="hidden" value="true" name="approved"/>
							<label for="amountLimit">New Amount Limit $</label>
							<input type="text" value="" name="amountLimit" id="amountLimit"/>
							<label for="transactionLimit">New Daily Transaction Limit</label>
							<input type="text" value="" name="transactionLimit" id="transactionLimit"/>
							<input type="submit" class="btn bg-success btn-sm skills-btn text-white approve-transaction" value="APPROVE" style="display: inline-block; width: 30%; margin-right: 3em;"/>
						</form>
					</c:forEach>
				</div> 
				<div class="container hidden" id="transaction">
					<c:forEach var="transactionsList" items="${pendingTransactions}" >
 						<c:forEach var="transaction" items="${transactionsList}" >
 							<div class="pending-ad card ${transaction.transactionType}-type">
	 							<p>${transaction.transactionType} </p>
	 							<p><fmt:formatNumber value="${transaction.amount}" type="currency"/></p>
	 							
								<form method="post" action="${submitChange}" class="approve-deny-form-ad">
									<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN"/>
									<input type="hidden" value="${transaction.transactionNumber}" name="transactionId"/>
 									<input type="hidden" value="false" name="approved" id="approveOrDeny-transaction" />
 									<input type="submit" class="btn bg-success btn-sm skills-btn text-white approve-transaction" value="APPROVE" style="display: inline-block; width: 30%; margin-right: 3em;"/>		
 									<input type="submit" class="text-white btn bg-danger btn-sm skills-btn deny-transaction" value="DENY" style="display: inline-block; width: 30%; " />
								</form>
 							</div>							
 						</c:forEach>
 					</c:forEach>
				</div>
				<div class="container hidden" id="campaign">
 						<c:forEach var="campaign" items="${pendingCampaigns}" >
 							<c:if test="${campaign.title != null}">
	 							<div class="pending-ad card ${campaign.campaignNumber}-type">
		 							<p>${campaign.title} </p>
		 							<p>${campaign.description} </p>
		 							<p><fmt:formatNumber value="${campaign.goal}" type="currency"/></p>
		 							
									<form method="post" action="${submitChange}" class="approve-deny-form-ad">
										<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN"/>
										<input type="hidden" value="${campaign.campaignNumber}" name="campaignId"/>
	 									<input type="hidden" value="false" name="approved" id="approveOrDeny-campaign" />
	 									<input type="submit" class="btn bg-success btn-sm skills-btn text-white approve-campaign" value="APPROVE" style="display: inline-block; width: 30%; margin-right: 3em;"/>		
	 									<input type="submit" class="text-white btn bg-danger btn-sm skills-btn deny-campaign" value="DENY" style="display: inline-block; width: 30%; " />
									</form>
	 							</div>
	 						</c:if>							
 						</c:forEach>
				</div>
 			</div>
 			
 			<!--  ad/comment aprovals -->
 			<div class="hidden" id="job-comment-requests">
				<div class="btn-group" id="jobCommentToggle" role="group" aria-label="Basic example">
				    <button type="button" class="btn btn-info jobToggleButton">Jobs</button>
				    <button type="button" class="btn btn-outline-info commentsToggleButton">Comments</button>
				</div>
				 				
 				 <!-- ad approval -->
 				 <c:set var="adCount" value="0" />
 				<div class="container" id="ads">
 					<c:forEach var="adList" items="${pendingAds }" >
 						<c:forEach var="ad" items="${adList }" >
 							<div class="pending-ad ${ad.adType}-type" style="margin-top: 1em; border-bottom: 2px solid lightblue">
	 							<p>${ad.adType } </p>
	 							<p>${ad.title } </p>
	 							<p>${ad.description}</p>
	 							
								<form method="post" action="${submitChange}" class="approve-deny-form-ad">
									<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN"/>
									<input type="hidden" value="${ad.adId }" name="adId"/>
 									<input type="hidden" value="false" name="approved" id="approveOrDeny-ad" />
 									<input type="submit" class="btn bg-success btn-sm skills-btn text-white approve-ad" value="APPROVE" style="display: inline-block; width: 30%; margin-right: 3em;"/>		
 									<input type="submit" class="text-white btn bg-danger btn-sm skills-btn deny-ad" value="DENY" style="display: inline-block; width: 30%; " />
								</form>
 							</div>
 							<c:set var="adCount" value="${adCount + 1}" />							
 						</c:forEach>
 					</c:forEach>
 					<p class="hidden" id="adCount"> ${adCount } </p>
 				</div>
 				
  				<!-- comment approval -->
 				<div class="container hidden" id="comments">
 					<c:set var="commentCount" value="0" />
 					<c:forEach var="commentList" items="${pendingComments }" >
 						<c:forEach var="comment" items="${commentList }">
 							<div style="margin-top: 1em; padding-bottom: 1em; border-bottom: 2px solid lightblue">
 								<strong>Comment: </strong>
 								<p>${comment.comment }</p>
 							
 							
 							<form method="post" action="${submitChange }" class="approve-deny-form-comment">
 									<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN"/>
									<input type="hidden" value="${comment.commentId }" name="commentId"/>
 									<input type="hidden" value="false" name="approved" id="approveOrDeny-comment" />
 									<input type="submit" class="btn bg-success btn-sm skills-btn text-white approve-comment" value="APPROVE" style="display: inline-block; width: 30%; margin-right: 3em;"/>		
 									<input type="submit" class="text-white btn bg-danger btn-sm skills-btn deny-comment" value="DENY" style="display: inline-block; width: 30%; " />
 							</form> 
  							</div>
 
 							<c:set var="commentCount" value="${commentCount + 1 }" />
  						</c:forEach>
 					</c:forEach>
 					<p class="hidden" id="commentCount">${commentCount }</p>
 				</div>
 				
 			</div>

 			
 			
<!--  			add a child account page 
 --> 			
 			<div id="add-child-page" class="hidden">
 				<h1>Need to create an account for your child?</h1>
 				<c:url var="createChildAccount" value="/addMyChild" />
 				<a href="${createChildAccount}" ><button type="button" class="btn-lg btn-info">Click Here To Get Started</button></a>
 				
 			</div>
 			
 		</div>
 	</div>
 	
 	<c:url var="guardianScript" value="/js/guardian.js" />
	<script src="${guardianScript}"></script>
 <c:import url="/WEB-INF/jsp/footer.jsp" />