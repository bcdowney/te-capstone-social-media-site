<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

 <c:import url="/WEB-INF/jsp/header.jsp" />

<div class="main-content">
	<div class="container-fluid">
		<div class="col-sm-3" style="display: inline-block;">		
			<div class="profile-picture shadow-sm">
				<%-- <c:url var="arnoldPic" value="/img/square-logo-no-tagline.png" /> --%>
				<c:url var="updatePic" value="/upload"/>
				<c:url var="profilePic" value="${business.logoFileName}"/>
				<img src="${profilePic}" alt="business logo" class="center-block img-thumbnail"/>
				<c:if test="${param.businessId == null || param.businessId == visitor}">
					<p><a href="${updatePic}">Click to update picture</a></p>
				</c:if>
			</div>
			<c:if test="${campaign != null }">
				<div class="crowd-funding card box-shadow border">
					<h4 class="card-header bg-info text-white">Crowd Funding</h4>
					<div class="card-body ">
						<h5 class="text-info ">Campaign Title: </h5>
						<c:out value="${campaign.title }"/>
						<br>
						<h5 class="text-info crowdfunding-profile">Description: </h5>
						<c:out value="${campaign.description }"/>
						<br>
						<div style="display:flex; justify-content:space-between">
						<div>
							<h5 class="text-info crowdfunding-profile">Current</h5>
							<c:out value="$ ${campaign.current }0"/>
						</div>
						<div>
							<h5 class="text-info crowdfunding-profile">Goal</h5>
							<fmt:formatNumber value="${campaign.goal }" type="currency"/>
						</div>
						</div>
						<div class="progress">
							<c:set var="math" value="${(campaign.current/campaign.goal) * 100 }" />
							<div class="progress-bar progress-bar-striped bg-warning progress-bar-animated" role="progressbar" style="width:${math}%" aria-valuenow="${campaign.current }" aria-valuemin="0" aria-valuemax="${campaign.goal }"></div>
						</div>
						<br>
						<button type="button" class="btn btn-success" data-toggle="modal" data-target="#donateModel">Donate</button>
					</div>
				</div>
			</c:if>
		</div>
		<div class="col-sm-8 rounded" style="display: inline-block; vertical-align: top;">
			<c:if test="${param.businessId == null || param.businessId == visitor}">
				<button type="button" id="edit-profile" class="btn-danger btn-sm skills-btn float-right text-white ">Edit Profile</button>
			</c:if>
			<button type="button" id="view-mode" class="btn bg-primary btn-sm skills-btn float-right text-white hidden">View Mode</button>
			<div class="business-view">
				<div class="business-name">
					<h1 style="border-bottom: solid 4px #F5B041">${business.businessName} </h1>
	 			</div>
				<div class="business-summary">
					<p>${business.summary }</p>
				</div>
			</div>
			<div class="" id="input-form">
				<c:url var="pendingChanges" value="" />
				<form method="POST" style="margin-bottom: 4em;" action="" id="input-form">
				 	<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
					<p class="hidden"> Business Name </p>
					<input type="text" id="businessName" name="businessName" value="${business.businessName}" style="width: 50%" class="form-control-lg hidden"/>
					
					<p class="hidden"> Business Summary </p>
					<textarea class="form-control hidden" id="summary" name="summary" rows="8">${business.summary }</textarea>
					
					<input type="submit" value="Submit Changes" id="submit-button" style="margin-left: 2em;" class="hidden btn bg-success btn-sm skills-btn float-right text-white"/>
				</form>
			</div>
			<div class="skills-panel" style="margin-bottom: 1em;">
				<h4 style="display: inline-block">Skills</h4>
								<c:url var="skillsPage" value="/skills"/>
				<c:if test="${param.businessId == null || param.businessId == visitor}">
					<a href="${skillsPage}"><button type="button" class="btn btn-info btn-sm skills-btn float-right" id="expand-skills">Add More Skills</button></a>	
				</c:if>		
				<ul class="list-group shadow-sm skills-list">
				
				<c:set var="skillsCount" value="0"/>
				<c:forEach var="skill" items="${skills}">
					<c:choose>
						<c:when test="${skillsCount % 2 == 0}"> 
							<li class="list-group-item">${skill.name}</li>
						</c:when>
						<c:otherwise>
							<li class="list-group-item bg-light">${skill.name}</li>
						</c:otherwise>
					</c:choose>
					<c:set var="skillsCount" value="${skillsCount + 1 }"/>
				</c:forEach>
				</ul>
<%-- 				<c:url var="skillsPage" value="/skills"/>
				<c:if test="${param.businessId == null || param.businessId == currentUser.kidId}">
					<a href="${skillsPage}"><button type="button" class="btn btn-info btn-sm skills-btn float-right" id="expand-skills">Add More Skills</button></a>	
				</c:if>		 --%>	
 			</div>
		</div>
	</div>
</div>
<div class="modal" id="donateModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-info" id="exampleModalLabel">Make a Campaign Donation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <c:url var="donate" value="/dashboard/profile" />	
      <form method="POST" action="${donate }" >
      <div class="modal-body">
      	<input type="hidden" name="toKid" value="${campaign.kidId}" />
      		<div class="form-group">
			    <input type="text" class="form-control" id="amount" placeholder="Donation Amount" name="amount">
		  	</div>
      		<h6>To make this donation, you must login with your business account.</h6>
      		<div class="form-group">
			    <input type="text" class="form-control" id="username" placeholder="Username" name="username">
		  	</div>
		  	<div class="form-group">
			    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
		  	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" value="submit" class="btn btn-success" />
      </div>
      </form>
    </div>
  </div>
</div>

<c:import url="/WEB-INF/jsp/footer.jsp" />

