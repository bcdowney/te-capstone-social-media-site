<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



	</body>
	<footer>
		<c:url var="profileScript" value="/js/profile.js" />
		<script src="${profileScript}"></script>
				
		<c:url var="skillsScript" value="/js/skills.js" />
		<script src="${skillsScript}"></script>
		
		<c:url var="formScript" value="/js/form_validation.js" />
		<script src="${formScript}"></script>
		
		<c:url var="adminScript" value="/js/admin.js"/>
		<script src="${adminScript }"></script>
		
		<c:url var="bulletinBoardScript" value="/js/bulletinboard.js" />
		<script src="${bulletinBoardScript }"></script>
 
 		<c:url var="formValidate" value="/js/form_validation.js"/>
 		<script src="${formValidate}" defer></script>
		
	</footer>
</html>