<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

 <c:import url="/WEB-INF/jsp/header.jsp" />
 
 <div class="main-content">
 	<div class="container">
 		<div class="col-sm-9">
			 <div>
				 <h3>Success! Once your co-founder is registered, you can begin creating your business account!</h3>
			</div>
			<div style="margin-top:2em">
				<h5>In the meantime...</h5>
				<ul>
					<li><em>Start thinking about your business name and logo</em></li>
					<li><em>Spend some time brainstorming about services your business might provide</em></li>
					<li><em>Begin working on a description for your business</em></li>
				</ul>
			</div>
		</div>
		<div>
			<c:url var="link" value="/userRegistration" >
				<c:param name="email" value="${param.email }" />
				<c:param name="firstName" value="${param.firstName }" />
				<c:param name="lastName" value="${param.lastName }" />
			</c:url>
			<h4><a href="${link }">Link</a></h4>
		</div>
	</div>
</div>
 
 <c:import url="/WEB-INF/jsp/footer.jsp" />