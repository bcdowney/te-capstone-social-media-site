<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib  uri="http://www.springframework.org/tags" prefix="spring" %>

<c:import url="/WEB-INF/jsp/header.jsp" />
 
 <div class="main-content">
 	<div class="container">
 		<div class="col-sm-9">
 			<c:url var="addChildProfile" value="/addChildProfile" />
 			<form:form action="${addChildProfile }"  method="post" id="profileRegistration" modelAttribute="kid">
 			 	<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
 			 	<div>
 					<h3>Add a child account</h3>
 				</div>
 				<div style="margin-top:.5em">
 					<p>Child First Name</p>
 				 	<form:input path="firstName" name="firstName"/>
 				 	<form:errors path="firstName" cssClass="error"/>
 				</div>
 				<div style="margin-top:.5em">
 					<p>Child Last Name</p>
 				 	<form:input path="lastName" name="lastName"/>
 				 	<form:errors path="lastName" cssClass="error"/>
 				</div>
 				<div style="margin-top:.5em">
 					<p>Child Date Of Birth yyyy-dd-mm</p>
 		 			<form:input type="date" path="dateOfBirth" name="dateOfBirth" />
 		 			<form:errors path="dateOfBirth" cssClass="error"/>
 				</div>
 				<br>
 				<input type="submit" value="submit"/>
 			</form:form>
		</div>
	</div>
	<br>
	<div class="container">
	 	<div class="col-sm-4">
			<div class="progress ">
				<div class="progress-bar bg-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
			</div>
		</div>
	</div>
</div>

<c:import url="/WEB-INF/jsp/footer.jsp" />
