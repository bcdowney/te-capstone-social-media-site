<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:url var="createAdmin" value="/dashboard/admin/create_admin"/>

	<div class="container-fluid">
		<form action="${createAdmin}" method="POST">
		<h3>Enter new admin's information</h3>
			<%-- <input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
			<input type="text" name="userName" />
			<input type="email" name="email"/>
			<input type="password" name="password" />
 			<input type="password" name="confirmPassword" />
 			<input type="hidden" name="role" value="admin"/> --%>

		<div class="input-group mb-3">
			<div class="input-group-prepend">
				<span class="input-group-text" id="basic-addon1">Username</span>
			</div>
			<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
			<input type="text" class="form-control" name="userName"
				aria-label="Username" aria-describedby="basic-addon1">
		</div>
		<div class="input-group mb-3">
			<input type="password" class="form-control" name="password"
				placeholder="Password" aria-label="password"
				aria-describedby="basic-addon2">
			<div class="input-group-append">
			</div>
		</div>
		<div class="input-group mb-3">
			<input type="password" class="form-control" name="confirmPassword"
				placeholder="Confirm Password" aria-label="confirm password"
				aria-describedby="basic-addon2">
			<div class="input-group-append">
			</div>
		</div>
		<div class="input-group mb-3">
			<div class="input-group-prepend">
				<span class="input-group-text" id="basic-addon3">Email</span>
			</div>
			<input type="email" class="form-control" name="email"
				aria-label="email" aria-describedby="basic-addon3">
			<input type="hidden" name="role" value="admin"/>
		</div>
		<input type="submit" class="btn btn-primary" value="Submit">
	</form>
	</div>
 
<c:import url="/WEB-INF/jsp/footer.jsp" />