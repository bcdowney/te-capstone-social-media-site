<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

 <c:import url="/WEB-INF/jsp/header.jsp" />
 	<c:url var="upload" value="/upload"/>
 	<div>
 		<form method="post" action="${upload}" enctype="multipart/form-data">
 			<input type="hidden" value="${CSRF_TOKEN}" name="CSRF_TOKEN" />
 			<input type="file" name="file" id="file"/>
 			<input type="submit" name="Submit"/>
 		</form>
 	</div>
 
 
 <c:import url="/WEB-INF/jsp/footer.jsp" />