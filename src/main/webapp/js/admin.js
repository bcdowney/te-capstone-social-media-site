$(document).ready(function() {
	
	$(".nav-link").on("click", function(event) {
		
		if ($(event.target).attr("id") === "v-pills-all_users-tab") {
			$(event.target).addClass("active");
			$("#v-pills-all-users").addClass("active");
			$("#v-pills-all-users").removeClass("hidden");
			$("#v-pills-all-users").addClass("show")
		} else {
			$("#v-pills-all-users").removeClass("active");
			$("#v-pills-all-users").addClass("hidden");
			$("#v-pills-all-users").removeClass("show");
			$("#v-pills-all_users-tab").removeClass("active");
		}
		
		if ($(event.target).attr("id") === "v-pills-child-tab") {
			$(event.target).addClass("active");
			$("#v-pills-child").addClass("active");
			$("#v-pills-child").addClass("show");
			$("#v-pills-child").removeClass("hidden");
		} else {
			$("#v-pills-child").removeClass("active");
			$("#v-pills-child").addClass("hidden");
			$("#v-pills-child-users").removeClass("show");
			$("#v-pills-child-tab").removeClass("active");
		}
		
		if ($(event.target).attr("id") === "v-pills-guardian-tab") {
			$(event.target).addClass("active");
			$("#v-pills-guardian").addClass("active");
			$("#v-pills-guardian").addClass("show");
			$("#v-pills-guardian").removeClass("hidden");
		} else {
			$("#v-pills-guardian").removeClass("active");
			$("#v-pills-guardian").addClass("hidden");
			$("#v-pills-guardian-users").removeClass("show");
			$("#v-pills-guardian-tab").removeClass("active");
		}
		
		if ($(event.target).attr("id") === "v-pills-admin-tab") {
			$(event.target).addClass("active");
			$("#v-pills-admin").addClass("active");
			$("#v-pills-admin").addClass("show");
			$("#v-pills-admin").removeClass("hidden");
		} else {
			$("#v-pills-admin").removeClass("active");
			$("#v-pills-admin").addClass("hidden");
			$("#v-pills-admin").removeClass("show");
			$("#v-pills-admin-tab").removeClass("active");
		}
		
		
	})
	
});