$(document).ready(function () {
	
	
	//approving and denying profile changes
	$(".approve-profile").on("mouseenter", function(event) {
			console.log($(event.target).siblings("#approveOrDeny-profile"));
			$(event.target).siblings("#approveOrDeny-profile").val("true");
			$(event.target).siblings("#approveOrDeny-profile").attr("value", "true");
//			$("#approveOrDeny-profile").val("true");
//			$("#approveOrDeny-profile").attr("value", "true");	
//		console.log($("#approveOrDeny").val());
	});
	
	
	$(".deny-profile").on("mouseenter", function(event) {
		console.log($(event.target).siblings("#approveOrDeny-profile"));
		$(event.target).siblings("#approveOrDeny-profile").val("false");
		$(event.target).siblings("#approveOrDeny-profile").attr("value", "false");
		
		
//		$(event.target).find("#approveOrDeny-profile").val("false");
//		$("#approveOrDeny-profile").val("false");
//		$("#approveOrDeny-profile").attr("value", "false");
//		
//		console.log($("#approveOrDeny").val());
	});
	//
	
	
	//approving and denying job posting
	$(".deny-ad").on("mouseenter", function(event) {	
		console.log($(event.target).siblings("#approveOrDeny-ad"));
		$(event.target).siblings("#approveOrDeny-ad").val("false");
		$(event.target).siblings("#approveOrDeny-ad").attr("value", "false");
		
		
//		
//		$("#approveOrDeny-ad").val("false");
//		$("#approveOrDeny-ad").attr("value", "false");	
//		console.log($("#approveOrDeny").val());
	});
	
	
	$(".approve-ad").on("mouseenter", function() {
		console.log($(event.target).siblings("#approveOrDeny-ad"));
		$(event.target).siblings("#approveOrDeny-ad").val("true");
		$(event.target).siblings("#approveOrDeny-ad").attr("value", "true");
		
	});
	//
	
	
	//approving and denying comments
	$(".deny-comment").on("mouseenter", function(event) {		
		console.log($(event.target).siblings("#approveOrDeny-comment"));
		$(event.target).siblings("#approveOrDeny-comment").val("false");
		$(event.target).siblings("#approveOrDeny-comment").attr("value", "false");
	});
	
	
	$(".approve-comment").on("mouseenter", function() {
		console.log($(event.target).siblings("#approveOrDeny-comment"));
		$(event.target).siblings("#approveOrDeny-comment").val("true");
		$(event.target).siblings("#approveOrDeny-comment").attr("value", "true");
		
	});
	
	$(".deny-transaction").on("mouseenter", function(event) {
		console.log($(event.target).siblings("#approveOrDeny-transaction"));
		$(event.target).siblings("#approveOrDeny-transaction").val("false");
		$(event.target).siblings("#approveOrDeny-transaction").attr("value", "false");
		
		
//		$(event.target).find("#approveOrDeny-profile").val("false");
//		$("#approveOrDeny-profile").val("false");
//		$("#approveOrDeny-profile").attr("value", "false");
//		
//		console.log($("#approveOrDeny").val());
	});
	
	$(".approve-transaction").on("mouseenter", function(event) {
		console.log($(event.target).siblings("#approveOrDeny-transaction"));
		$(event.target).siblings("#approveOrDeny-transaction").val("true");
		$(event.target).siblings("#approveOrDeny-transaction").attr("value", "true");
		
		
//		$(event.target).find("#approveOrDeny-profile").val("false");
//		$("#approveOrDeny-profile").val("false");
//		$("#approveOrDeny-profile").attr("value", "false");
//		
//		console.log($("#approveOrDeny").val());
	});
	
	$(".deny-campaign").on("mouseenter", function(event) {
		console.log($(event.target).siblings("#approveOrDeny-campaign"));
		$(event.target).siblings("#approveOrDeny-campaign").val("false");
		$(event.target).siblings("#approveOrDeny-campaign").attr("value", "false");
		
		
//		$(event.target).find("#approveOrDeny-profile").val("false");
//		$("#approveOrDeny-profile").val("false");
//		$("#approveOrDeny-profile").attr("value", "false");
//		
//		console.log($("#approveOrDeny").val());
	});
	
	$(".approve-campaign").on("mouseenter", function(event) {
		console.log($(event.target).siblings("#approveOrDeny-campaign"));
		$(event.target).siblings("#approveOrDeny-campaign").val("true");
		$(event.target).siblings("#approveOrDeny-campaign").attr("value", "true");
		
		
//		$(event.target).find("#approveOrDeny-profile").val("false");
//		$("#approveOrDeny-profile").val("false");
//		$("#approveOrDeny-profile").attr("value", "false");
//		
//		console.log($("#approveOrDeny").val());
	});
	//
	//
	$(".transactionsToggleButton").on("click", function(event) {
		$(".transactionsToggleButton").addClass("btn-info");
		$(".transactionsToggleButton").removeClass("btn-outline-info");
		
		$(".campaignsToggleButton").addClass("btn-outline-info");
		$(".campaignsToggleButton").removeClass("btn-info");
		
		$(".setLimitsToggleButton").addClass("btn-outline-info");
		$(".setLimitsToggleButton").removeClass("btn-info");
		
		$("#limits").addClass("hidden");
		$("#campaign").addClass("hidden");
		$("#transaction").removeClass("hidden");

	});
	
	$(".campaignsToggleButton").on("click", function(event) {
		$(".campaignsToggleButton").addClass("btn-info");
		$(".campaignsToggleButton").removeClass("btn-outline-info");
		
		$(".transactionsToggleButton").addClass("btn-outline-info");
		$(".transactionsToggleButton").removeClass("btn-info");
		
		$(".setLimitsToggleButton").addClass("btn-outline-info");
		$(".setLimitsToggleButton").removeClass("btn-info");
		
		$("#limits").addClass("hidden");
		$("#transaction").addClass("hidden");
		$("#campaign").removeClass("hidden");

	});
	
	$(".setLimitsToggleButton").on("click", function(event) {
		$(".setLimitsToggleButton").addClass("btn-info");
		$(".setLimitsToggleButton").removeClass("btn-outline-info");
		
		$(".transactionsToggleButton").addClass("btn-outline-info");
		$(".transactionsToggleButton").removeClass("btn-info");
		
		$(".campaignsToggleButton").addClass("btn-outline-info");
		$(".campaignsToggleButton").removeClass("btn-info");
		
		$("#limits").removeClass("hidden");
		$("#transaction").addClass("hidden");
		$("#campaign").addClass("hidden");

	});
	//button toggles for job/comment when approving/denying jobs/comments
	$(".jobToggleButton").on("click", function(event) {
			$(".jobToggleButton").addClass("btn-info");
			$(".jobToggleButton").removeClass("btn-outline-info");
			
			$(".commentsToggleButton").addClass("btn-outline-info");
			$(".commentsToggleButton").removeClass("btn-info");
			
			$("#comments").addClass("hidden");
			$("#ads").removeClass("hidden");
	
	});
	
	$(".commentsToggleButton").on("click", function(event) {
					
			$(".commentsToggleButton").addClass("btn-info");
			$(".commentsToggleButton").removeClass("btn-outline-info");
			
			$(".jobToggleButton").removeClass("btn-info");
			$(".jobToggleButton").addClass("btn-outline-info")
			
			$("#ads").addClass("hidden");
			$("#comments").removeClass("hidden");
	})
	//
	
	
	
	//navbar links at top to switch between the "pages" of the guardian dashboard
	$(".change-button").on("click", function() {
		$("#changes").toggleClass("hidden");
		$(".change-button").toggleClass("hidden");
	});
	
	$(".nav-link").on("click", function(event) {
		$(".active").removeClass("active");
		$(event.target).addClass("active");
		
		if (!$("#profile-changes-tab").hasClass("active")) {
			$("#profile-change-requests").addClass("hidden");
		} else {
			$("#profile-change-requests").removeClass("hidden");
		}
		
		if (!$("#add-child-tab").hasClass("active")) {
			$("#add-child-page").addClass("hidden");
		} else {
			$("#add-child-page").removeClass("hidden");
		}
		
		if (!$("#financials-tab").hasClass("active")) {
			$("#financial-approvals").addClass("hidden");
		} else {
			$("#financial-approvals").removeClass("hidden");
		}
		
		if (!$("#job-comments-tab").hasClass("active")) {
			$("#job-comment-requests").addClass("hidden");
		} else {
			$("#job-comment-requests").removeClass("hidden");
		}
		
	});
	//
	
	
	//writing message when there are not pending changes for profile/jobs/comments
	let profileChangeCount = $("#changeCount").html();
	if (profileChangeCount == 0) {
		console.log("test")
		$("#profile-change-requests").append("<div class='' style='margin-top: 1em'><h2 style='margin-left: 1em; margin-top: 1em; margin-bottom: 1em;'> There are no pending profile change requests </h2></div>");
	}
	
	let jobChangeCount = $("#adCount").html();
	if (jobChangeCount == 0) {
		$("#ads").append("<div class='' style='margin-top: 1em'><h2 style='margin-left: 1em; margin-top: 1em; margin-bottom: 1em;'> There are no pending job requests </h2></div>");
	}
	
	let commentChangeCount = $("#commentCount").html();
	if (commentChangeCount == 0) {
		$("#comments").append("<div class='' style='margin-top: 1em'><h2 style='margin-left: 1em; margin-top: 1em; margin-bottom: 1em;'> There are no pending comment requests </h2></div>");
	}
	//
	
	//determines what to write in profile changes depending on what information is passed in, IE summary vs skill
	for (let i = 0; i < profileChangeCount; i++) {

		let name = $("#kidFirstName").html();
		
		let currentValue = $("#currentValue"+i).html();
		
		let test = $("#requestedValue"+i).html().split(",");
		
		let columnName = $("#columnName" + i).html();
		let requestedValue = $("#requestedValue" + i).html();

		
		if ($("#messageToParents" + i).hasClass("skills")) {
			$("#messageToParents" + i).append("<h2>" + name + " Wants to " + currentValue + " the skill " + test[0] + "</h2>");
		} else {
			$("#messageToParents" + i).append("<h2 style='display: inline-block;'>" + name + " Wants to change his " + columnName + "</h2>");
			$("#messageToParents" + i).append("<button type='button' style='display: inline-block; margin-left: 3em;' class='btn btn-info' data-toggle='modal' data-target='#exampleModal3'>View More Info</button><div class='modal fade' id='exampleModal3' tabindex='-1' role='dialog' aria-labelledby='exampleModal3Label' aria-hidden='true'><div class='modal-dialog' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' id='exampleModal3Label'>New Requested Summary</h5></div><div class='modal-body'>" + requestedValue + "</div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button></div></div></div></div>");



		}		
	}
	//
	
	
	console.log("test");
	console.log("profileChanges: " +  profileChangeCount);
	console.log("jobChanges: " + jobChangeCount);
	console.log("commentChanges: " + commentChangeCount);
	
	console.log($("#columnName").html());
});