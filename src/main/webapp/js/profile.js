$(document).ready(function () {

	
	$("#edit-profile").on("click", function () {
		$(".business-view").toggleClass("hidden");
		$("#input-form > p").toggleClass("hidden");
		$("#input-form > input").toggleClass("hidden");
		$("#input-form > textarea").toggleClass("hidden");
		$("#view-mode").toggleClass("hidden");
		
		if ($("#edit-profile").html() == "cancel") {
			$("#edit-profile").html("edit profile");
		} else {
			$("#edit-profile").html("cancel");
		}
	})
	
	$("#view-mode").on("click", function() {
		let newName = $("#businessName").val();
		$(".business-name > h1").html(newName);

		$(".business-view").toggleClass("hidden");
		$("#input-form > p").toggleClass("hidden");
		$("#input-form > input").toggleClass("hidden");
		$("#input-form > textarea").toggleClass("hidden");

		
		$("#view-mode").toggleClass("hidden");
		$("#edit-profile").html("edit profile");
		
		let newSummary = $("#summary").val();
		$(".business-summary > p").html(newSummary);
		$("#submit-changes").toggleClass("hidden");



	})

});