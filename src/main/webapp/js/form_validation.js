/// <reference path="../js/jquery.min.js" />
/// <reference path="../js/jquery.validate.min.js" />

$(document).ready( function() {

	$("#userForm").validate({
//      debug: true,
      rules: {
          userName: {
        	  required: true
          },
          password: {
              required: true,
              minlength: 10,
              strongPassword: true
          },
          confirmPassword: {
              equalTo: "#password"
          },
      },
      messages: {
          userName: {
              required: "Please enter a user name"
          },
          password: {
              required: "Please enter a password",
              minlength: "Password must be at least 10 characters in length"
          },
          confirmPassword: {
              equalTo: "Passwords do not match"
          }
      },
      errorClass: "error",
      validClass: "valid"
  })
  
	$("#profileRegistration").validate({
//        debug: true,
        rules: {
            firstName: {
                required: true
            },
            lastName: {
                required: true,
                minlength: 2,
                lettersonly: true
            },
            email: {
                required: true,
                email: true,
            },
            dateOfBirth: {
            	required: true,
            	dateISO: true
            }
        },
        messages: {
            firstName: {
                required: "Please enter a first name"
            },
            lastName: {
                required: "Please enter a last name",
                minlength: "Last name must be at least two characters long",
                lettersonly: "Please only user letters for last name"
            },
            email: {
                required: "Please enter an email address",
                email: "Please enter a valid email address"
            },
        },
        errorClass: "error",
        validClass: "valid"
    })
    
    $("#jobSkillPost").validate({
//        debug: true,
        rules: {
            title: {
                required: true
            },
            description: {
                required: true
            },
            startDate: {
                required: true
            },
        },
        messages: {
            title: {
                required: "Title required - this will help draw attention to your post!"
            },
            description: {
                required: "Description required - this will help your audience understand your job or skill!"
            },
            startDate: {
                required: "Start date is required"
            },
        },
        errorClass: "error",
        validClass: "valid"
    })
    
    $("#mainLogin").validate({
//      debug: true,
      rules: {
    	  username: {
        	  required: true
          },
          password: {
              required: true,
          },
      },
      messages: {
    	  username: {
              required: "Required"
          },
          password: {
              required: "Required",
          },
      },
      errorClass: "error",
      validClass: "valid"
  })
  
  $("#requestForm").validate({
//        debug: true,
        rules: {
            firstName: {
                required: true
            },
            lastName: {
                required: true,
                minlength: 2,
                lettersonly: true
            },
            email: {
                required: true,
                email: true,
            },
        },
        messages: {
            firstName: {
                required: "Please enter a first name"
            },
            lastName: {
                required: "Please enter a last name",
            },
            email : {
            	required: "Email address is required",
            	email: "Please enter a valid email address"
            }
        },
        errorClass: "error",
        validClass: "valid"
    })

})

$.validator.addMethod("strongPassword", function(value, index) {
    return value.match(/[A-Z]/) && value.match(/[a-z]/) && value.match(/\d/) && value.match(/[\[\\\^\$\.\|\?\*\+\(\)\]@#%&:;",!_-~`]/)
}, "Please enter a strong password (one capital, one lower case, one number, and one special character");

