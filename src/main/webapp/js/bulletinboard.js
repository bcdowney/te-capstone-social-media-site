$(document).ready(function() {	
	
	
	
	
	$("#yes").on("click", function() {
		$("#compensation-div").addClass("hidden");
		$("#bartering-div").removeClass("hidden");
		$("#submit").removeClass("hidden");
	});
	
	$("#no").on("click", function() {
		$("#bartering-div").addClass("hidden");
		$("#compensation-div").removeClass("hidden");
		$("#submit").removeClass("hidden");
	})
	
	$("#campaign-radio").on("click", function() {
		$("#campaign").removeClass("hidden");
		$("#submit").removeClass("hidden");
		$("#not-campaign").addClass("hidden");
		$(".barter-yes-no").addClass("hidden");
	});
	
	$("#job-radio").on("click", function() {
		$("#campaign").addClass("hidden");
		$("#submit").addClass("hidden");
		$("#not-campaign").removeClass("hidden");
		$(".barter-yes-no").removeClass("hidden");
	});
	
	$("#skill-radio").on("click", function() {
		$("#campaign").addClass("hidden");
		$("#submit").addClass("hidden");
		$("#not-campaign").removeClass("hidden");
		$(".barter-yes-no").removeClass("hidden");
	});
	
	$("#change-details-buttons").on("click", function(event) {
		
		if ($(event.target).hasClass("btn")) {
			$(".active").removeClass("active");
			$(".btn-primary").addClass("btn-outline-primary");
			$(".btn-primary").removeClass("btn-primary");
			
			$(event.target).addClass("btn-primary");
			$(event.target).removeClass("btn-outline-primary");
			$(event.target).addClass("active");
			
			if ($(event.target).hasClass("see-jobs-btn")) {
				$(".see-jobs").removeClass("hidden");
			} else {
				$(".see-jobs").addClass("hidden");
			}
			
			if ($(event.target).hasClass("post-job-btn")) {
				$(".post-job").removeClass("hidden");
			} else {
				$(".post-job").addClass("hidden");
			}
			
			if ($(event.target).hasClass("see-campaign-btn")) {
				$(".see-campaigns").removeClass("hidden");
			} else {
				$(".see-campaigns").addClass("hidden");
			}
			
			if ($(event.target).hasClass("post-skill-btn")) {
				$(".post-skill").removeClass("hidden");
			} else {
				$(".post-skill").addClass("hidden");
			}
		}

	});



});