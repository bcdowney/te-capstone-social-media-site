$(document).ready(function () {
	
	$(".skill").on("click", function(event) {
		
		if ($(event.target).hasClass("list-group-item-success") || $(event.target).hasClass("bg-danger")) {
			$(event.target).toggleClass("list-group-item-success");
			$(event.target).toggleClass("bg-danger text-white");
		} else {
			$(event.target).toggleClass("bg-success");
			$(event.target).toggleClass("text-white");
		}
		
		let valueList = $("#input-skills").val().split(",");
		let skillValue = $(event.target).attr("id");
		
		let inList = false;
		
		
		for (let i = 0; i < valueList.length; i++) {
			if (skillValue === valueList[i]) {	
				valueList.splice(i, 1);
				inList = true;
			} else {
				continue;
			}
		}
		
		if (inList != true) {
			valueList += "," + skillValue;
		} 
		
		$("#input-skills").attr("value", valueList);
		console.log(valueList);
	})
		
});