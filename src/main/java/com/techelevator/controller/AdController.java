package com.techelevator.controller;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.Ad;
import com.techelevator.model.Admin;
import com.techelevator.model.Campaign;
import com.techelevator.model.Comment;
import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.DAO.AdDAO;
import com.techelevator.model.DAO.CampaignDAO;
import com.techelevator.model.DAO.CommentDAO;
import com.techelevator.model.DAO.GuardianDAO;
import com.techelevator.model.DAO.KidDAO;

@Controller
public class AdController {
	
	//ad DAO and such
	AdDAO adDAO;
	CommentDAO commentDAO;
	KidDAO kidDAO;
	GuardianDAO guardianDAO;
	CampaignDAO campaignDAO;
	
	@Autowired
	public AdController(AdDAO adDAO, CommentDAO commentDAO, KidDAO kidDAO, GuardianDAO guardianDAO, CampaignDAO campaignDAO) {
		this.adDAO = adDAO;
		this.commentDAO = commentDAO;
		this.kidDAO = kidDAO;
		this.guardianDAO = guardianDAO;
		this.campaignDAO = campaignDAO;
	}
	
	@RequestMapping(path="/bulletinboard", method=RequestMethod.GET)
	public String displayMessageBoard(ModelMap map, HttpSession session) {
		
		List<Campaign> allCampaigns = campaignDAO.getAllCampaigns();
		Map<Integer, Kid> campaignAndKid = createCampaignKidMap(allCampaigns);
		List<Ad> allAds = adDAO.getAllAds();
		Map<Integer, List<Comment>> allComments = new HashMap<Integer, List<Comment>>();
		
		if (session.getAttribute("currentUser") instanceof Kid || session.getAttribute("currentUser") instanceof Admin) {
			Kid kid = new Kid();
			if (session.getAttribute("currentUser") instanceof Kid) {
				kid = (Kid) session.getAttribute("currentUser");
			} 
			for (Ad ad : allAds) {
				if (session.getAttribute("currentUser") instanceof Admin || ad.getKidId() == kid.getKidId()) {
					allComments.put(ad.getAdId(), commentDAO.getCommentsByAdId(ad.getAdId()));
				} else {
					List<Comment> adComments = new ArrayList<Comment>();
					for (Comment comment : commentDAO.getApprovedCommentsByKidId(kid.getKidId())) {
						if (comment.getAdId() == ad.getAdId())
							adComments.add(comment);
					}
					allComments.put(ad.getAdId(), adComments);
				}
			}
			map.addAttribute("currentAds", allAds);
			map.addAttribute("comments", allComments);
			map.addAttribute("allCampaigns", allCampaigns);
			map.addAttribute("campaignKid", campaignAndKid);
			
			return "bulletinboard";
		}
		
		return "redirect:/dashboard";
	}
	
	@RequestMapping(path="bulletinboard/newad", method=RequestMethod.GET)
	public String createNewAd() {
		
		return "newAd";
	}
	
	@RequestMapping(path="bulletinboard/newad", method=RequestMethod.POST)
	public String submitNewAd(HttpSession session,
							@RequestParam String title,
							@RequestParam String adType,
							@RequestParam String description,
							@RequestParam(required=false) double goal,
							@RequestParam(required=false) Date startDate,
							@RequestParam(required=false) Time startTime,
							@RequestParam(required=false) Date endDate,
							@RequestParam(required=false) Time endTime,
							@RequestParam(required=false) double payDollars,
							@RequestParam(required=false) String barterDescription) {
		Kid kid = (Kid) session.getAttribute("currentUser");
		if (goal != 0.0 && campaignDAO.getCampaignByKidId(kid.getKidId()).getTitle() == null) {
			Campaign campaign = mapToNewCampaign(kid.getKidId(), title, description, goal);
			campaignDAO.insertCampaign(campaign);
		} else if (goal == 0.0){
			Ad ad = mapToNewAd(kid, title, adType, description, startDate, startTime, endDate, endTime, payDollars, barterDescription);
			adDAO.insertAd(ad);
		}
		return "redirect:/bulletinboard";
	}
	
	@RequestMapping(path="/bulletinboard/ad", method=RequestMethod.POST)
	public String submitComment(ModelMap map, 
								HttpSession session,
								@RequestParam int adId,
								@RequestParam String comment) { //@PathVariable(value="adId") int id) if map doesn't work
		if (session.getAttribute("currentUser") instanceof Admin || session.getAttribute("currentUser") instanceof Guardian) {
			return "redirect:/bulletinboard";
		}
		Kid kid = (Kid) session.getAttribute("currentUser");
		String contactEmail = kid.getEmail();
		if (contactEmail == null) {
			contactEmail = guardianDAO.getGuardianByGuardianId(kid.getGuardianId()).getEmail();
		}
		commentDAO.insertComment(mapToNewComment(adId, kid.getKidId(), comment, contactEmail));
		return "redirect:/bulletinboard";
	}


	private Ad mapToNewAd(Kid kid, String adTitle, String adType, String description, Date startDate, Time startTime, 
						Date endDate, Time endTime, double payDollars, String barterDescription) {
		Ad ad = new Ad();
		
		ad.setKidId(kid.getKidId());
		ad.setTitle(adTitle);
		ad.setAdType(adType);
		ad.setDescription(description);
		ad.setStartDate(startDate);
		ad.setStartTime(startTime);
		ad.setEndDate(endDate);
		ad.setEndTime(endTime);
		ad.setPayDollars(payDollars);
		ad.setBarterDescription(barterDescription);
		ad.setApproved(false);
		return ad;
	}
	
	private Comment mapToNewComment(int adId, int commenterId, String comment, String contactEmail) {
		Comment newComment = new Comment();
		newComment.setAdId(adId);
		newComment.setCommenterId(commenterId);
		newComment.setComment(comment);
		newComment.setContactEmail(contactEmail);
		newComment.setApproved(false);
		newComment.setCreateDate(new Timestamp(System.currentTimeMillis()));
		return newComment;
	}
	
	private Campaign mapToNewCampaign(int kidId, String title, String description, double goal) {
		Campaign campaign = new Campaign();
		campaign.setKidId(kidId);
		campaign.setGoal(goal);
		campaign.setTitle(title);
		campaign.setDescription(description);
		campaign.setApproved(false);
		campaign.setCreateDate(new Timestamp(System.currentTimeMillis()));
		campaign.setCurrent(0.0);
		return campaign;
	}
	
	private Map<Integer, Kid> createCampaignKidMap(List<Campaign> campaigns){
		Map<Integer, Kid> campaignAndKid = new LinkedHashMap<Integer, Kid>();
		for(Campaign c : campaigns) {
			Kid kid = kidDAO.getKidByKidId(c.getKidId());
			campaignAndKid.put(c.getCampaignNumber(), kid);
		}
		return campaignAndKid;
	}

}
