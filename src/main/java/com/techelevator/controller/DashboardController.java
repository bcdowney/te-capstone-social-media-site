package com.techelevator.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.Ad;
import com.techelevator.model.Admin;
import com.techelevator.model.Campaign;
import com.techelevator.model.Comment;
import com.techelevator.model.Kid;
import com.techelevator.model.Guardian;
import com.techelevator.model.HistoricalChange;
import com.techelevator.model.PendingProfileChange;
import com.techelevator.model.Skill;
import com.techelevator.model.Transaction;
import com.techelevator.model.User;
import com.techelevator.model.UsersAbstract;
import com.techelevator.model.DAO.AdDAO;
import com.techelevator.model.DAO.AdminDAO;
import com.techelevator.model.DAO.CampaignDAO;
import com.techelevator.model.DAO.CommentDAO;
import com.techelevator.model.DAO.GuardianDAO;
import com.techelevator.model.DAO.HistoricalChangeDAO;
import com.techelevator.model.DAO.KidDAO;
import com.techelevator.model.DAO.PendingProfileChangeDAO;
import com.techelevator.model.DAO.SkillDAO;
import com.techelevator.model.DAO.TransactionDAO;
import com.techelevator.model.DAO.UserDAO;

@Controller
public class DashboardController {
	
	private static Map<String, String> SQL_COMMANDS = new HashMap<String, String>() {
		{
			put("firstName", "first_name");
			put("lastName", "last_name");
			put("businessName", "business_name");
			put("dateOfBirth", "date_of_birth");
			put("logoFileName", "logo_file_name");
			put("summary", "summary");
			put("email", "email");
			put("skills", "skills");
		}
	};
	
	private KidDAO kidDAO;
	private SkillDAO skillDAO;
	private GuardianDAO guardianDAO;
	private AdminDAO adminDAO;
	private PendingProfileChangeDAO pendingProfileChangeDAO;
	private HistoricalChangeDAO historicalChangeDAO;
	private UserDAO userDAO;
	private AdDAO adDAO;
	private CommentDAO commentDAO;
	private TransactionDAO transactionDAO;
	private CampaignDAO campaignDAO;
	
	@Autowired
	public DashboardController(KidDAO kidDAO, GuardianDAO guardianDAO, AdminDAO adminDAO, 
			PendingProfileChangeDAO pendingProfileChangeDAO, SkillDAO skillDAO, 
			HistoricalChangeDAO historicalChangeDAO, UserDAO userDAO,
			AdDAO adDAO, CommentDAO commentDAO, CampaignDAO campaignDAO, TransactionDAO transactionDAO) {
		this.kidDAO = kidDAO;
		this.guardianDAO = guardianDAO;
		this.adminDAO = adminDAO;
		this.skillDAO = skillDAO;
		this.pendingProfileChangeDAO = pendingProfileChangeDAO;
		this.historicalChangeDAO = historicalChangeDAO;
		this.userDAO = userDAO;
		this.adDAO = adDAO;
		this.commentDAO = commentDAO;
		this.campaignDAO = campaignDAO;
		this.transactionDAO = transactionDAO;
	}
	
	@RequestMapping(path="/dashboard", method=RequestMethod.GET)
	public String checkDashboardOwner(HttpSession session) {
		if (session.getAttribute("currentUser") == null)
			return "redirect:/login";
		if (session.getAttribute("currentUser") instanceof Kid)
			return "redirect:/dashboard/profile";
		if (session.getAttribute("currentUser") instanceof Guardian)
			return "redirect:/dashboard/guardian";
		return "redirect:/dashboard/admin";
	}
	
	@RequestMapping(path="/dashboard/profile", method=RequestMethod.GET)
	public String displayKidDashboard(ModelMap map, HttpSession session, @RequestParam(required=false) Integer businessId) {
		Kid kid = new Kid();
		if (session.getAttribute("currentUser") instanceof Kid) {
			kid = (Kid) session.getAttribute("currentUser");
			session.setAttribute("visitor", kid.getKidId());
		}
		if (session.getAttribute("currentUser") instanceof Admin) {
			kid = kidDAO.getKidByKidId(businessId);
		}
		List<Skill> skillList = new ArrayList<Skill>();
		Campaign campaign = new Campaign();
		if (businessId == null || businessId == kid.getKidId()) {
			List<PendingProfileChange> pendingProfileChanges = pendingProfileChangeDAO.getPendingChangesByKid(kid);
			if (!pendingProfileChanges.isEmpty()) {
				session.setAttribute("pendingChanges", pendingProfileChanges);
			}
		}
		if (businessId == null) {
			map.addAttribute("business", kidDAO.getKidByKidId(kid.getKidId()));
			skillList = skillDAO.getSkillsByKidId(kid.getKidId());
			campaign = campaignDAO.getCampaignByKidId(kid.getKidId());
		} else {
			map.addAttribute("business", kidDAO.getKidByKidId(businessId));
			skillList = skillDAO.getSkillsByKidId(businessId);
			campaign = campaignDAO.getCampaignByKidId(businessId);
		}
		if (campaign.getTitle() != null) {
			map.addAttribute("campaign", campaign);
		}
		session.setAttribute("skills", skillList);
		return "profile";
	}
	
	@RequestMapping(path="/dashboard/profile", method=RequestMethod.POST) //add crowdfunding requests?
	public String updateProfile(@RequestParam(required=false) String userName,
											@RequestParam(required=false) String firstName,
											@RequestParam(required=false) String lastName,
											@RequestParam(required=false) String dateOfBirth,
											@RequestParam(required=false) String fileName,
											@RequestParam(required=false) String summary,
											@RequestParam(required=false) String email,
											@RequestParam(required=false) Integer toKid,
											@RequestParam(required=false) String username,
											@RequestParam(required=false) String password,
											@RequestParam(required=false) Double amount,
											@RequestParam(required=false) String businessName,
											HttpSession session) {
		if (toKid != null) {
			if(!userDAO.searchForUsernameAndPassword(username, password)) {
				return "dashboard/profile";
			}
			if (session.getAttribute("currentUser") instanceof Kid) {
				Kid kid = (Kid) session.getAttribute("currentUser");
				Timestamp yesterday = new Timestamp(System.currentTimeMillis() - 1000L * 60L * 60L * 24L);
				Timestamp today = new Timestamp(System.currentTimeMillis());
//				if (kidDAO.getTransactionAmountLimitByKidId(kid.getKidId()) == -1) {
//					if (transactionDAO.getTotalOfTransactionsBetweenTimesByFromKidId(yesterday, today, kid.getKidId()) < kidDAO.getTransactionAmountLimitByKidId(kid.getKidId()) + amount){
//						Transaction transaction = mapToNewTransaction(toKid, kidDAO.getKidIdByKidUserId(userDAO.getUserIdByUsername(username)), amount);
//						transactionDAO.insertTransaction(transaction);
//					}
//				} else if (kidDAO.getTransactionsPerDayLimitByKidId(kid.getKidId()) == -1) {
//					if (transactionDAO.getNumberOfTransactionsFromBeginningByFromKidId(yesterday, today, kid.getKidId()) < kidDAO.getTransactionsPerDayLimitByKidId(kid.getKidId())){
//						Transaction transaction = mapToNewTransaction(toKid, kidDAO.getKidIdByKidUserId(userDAO.getUserIdByUsername(username)), amount);
//						transactionDAO.insertTransaction(transaction);
//					}
//				} else if (transactionDAO.getTotalOfTransactionsBetweenTimesByFromKidId(yesterday, today, kid.getKidId()) < kidDAO.getTransactionAmountLimitByKidId(kid.getKidId()) + amount &&
//				transactionDAO.getNumberOfTransactionsFromBeginningByFromKidId(yesterday, today, kid.getKidId()) < kidDAO.getTransactionsPerDayLimitByKidId(kid.getKidId())) {
//					Transaction transaction = mapToNewTransaction(toKid, kidDAO.getKidIdByKidUserId(userDAO.getUserIdByUsername(username)), amount);
//					transactionDAO.insertTransaction(transaction);
//				}
				Transaction transaction = mapToNewTransaction(toKid, kidDAO.getKidIdByKidUserId(userDAO.getUserIdByUsername(username)), amount);
				transactionDAO.insertTransaction(transaction);
			}
		} else if (firstName != null || lastName != null || dateOfBirth != null || fileName!= null || summary != null || email != null){
			List<PendingProfileChange> updates = checkForUpdates(session.getAttribute("currentUser"), firstName, lastName, dateOfBirth, fileName, summary, email, businessName);
			pendingProfileChangeDAO.saveToPendingChanges(updates);
		}
		return "redirect:/dashboard/profile";
	}

	@RequestMapping(path="/dashboard/guardian", method=RequestMethod.GET)
	public String displayGuardianDashboard(HttpSession session, ModelMap map) {
		Guardian guardian = (Guardian) session.getAttribute("currentUser");
		List<Kid> kids = guardianDAO.getAllKids(guardian.getGuardianID());
		List<List<Comment>> pendingComments = new ArrayList<List<Comment>>();
		List<List<Ad>> pendingAds = new ArrayList<List<Ad>>();
		List<PendingProfileChange> pendingProfileChanges = pendingProfileChangeDAO.getPendingChangesByGuardian(guardian);
		List<List<Transaction>> pendingTransactions = new ArrayList<List<Transaction>>();
		List<Campaign> pendingCampaigns = new ArrayList<Campaign>();
		
		for (Kid kid : kids) {
			pendingComments.add(commentDAO.getUnapprovedCommentsByKidId(kid.getKidId()));
			pendingAds.add(adDAO.getUnapprovedAdsbyKidId(kid.getKidId()));
			pendingTransactions.add(transactionDAO.getAllUnapprovedTransactionsByFromKidId(kid.getKidId()));
			pendingCampaigns.add(campaignDAO.getUnapprovedCampaignByKidId(kid.getKidId()));
		}
		

		map.addAttribute("pendingComments", pendingComments);
		map.addAttribute("pendingAds", pendingAds);
		session.setAttribute("pendingChanges", pendingProfileChanges);
		map.addAttribute("pendingTransactions", pendingTransactions);
		map.addAttribute("pendingCampaigns", pendingCampaigns);
		map.addAttribute("listOfKids", kids);
		return "guardian";
	}
	
	@RequestMapping(path="/dashboard/guardian", method=RequestMethod.POST)
	public String approveOrDenyChanges(HttpSession session,
										@RequestParam(required=false) Integer pendingChangeId,
										@RequestParam(required=false) Integer adId,
										@RequestParam(required=false) Integer commentId,
										@RequestParam(required=false) Integer campaignId,
										@RequestParam(required=false) Integer transactionId,
										@RequestParam(required=false) Double amountLimit,
										@RequestParam(required=false) Integer transactionLimit,
										@RequestParam(required=false) Integer kidId,
										@RequestParam boolean approved) {
		Guardian guardian = (Guardian) session.getAttribute("currentUser");
		List<PendingProfileChange> pendingProfileChanges = pendingProfileChangeDAO.getPendingChangesByGuardian(guardian);
		List<Kid> listOfKids = guardianDAO.getAllKids(guardian.getGuardianID());
		List<List<Comment>> pendingComments = new ArrayList<List<Comment>>();
		List<List<Ad>> pendingAds = new ArrayList<List<Ad>>();
		List<List<Transaction>> pendingTransactions = new ArrayList<List<Transaction>>();
		List<Campaign> pendingCampaigns = new ArrayList<Campaign>();
		for (Kid kid : listOfKids) {
			pendingComments.add(commentDAO.getUnapprovedCommentsByKidId(kid.getKidId()));
			pendingAds.add(adDAO.getUnapprovedAdsbyKidId(kid.getKidId()));
			pendingTransactions.add(transactionDAO.getAllUnapprovedTransactionsByFromKidId(kid.getKidId()));
			pendingCampaigns.add(campaignDAO.getUnapprovedCampaignByKidId(kid.getKidId()));
		}
		if (kidId != null) {
			if (transactionLimit != null) {
				kidDAO.updateTransactionsPerDayLimitByKidId(kidId, transactionLimit);
			}
			if (amountLimit != null) {
				kidDAO.updateTransactionAmountLimitByKidId(kidId, amountLimit);
			}
		}
		if (campaignId != null) {
			for (int i = 0; i < pendingCampaigns.size(); i++) {
				if (pendingCampaigns.get(i).getCampaignNumber() == campaignId && approved) {
					campaignDAO.approveCampaign(campaignId);
					pendingCampaigns.remove(i);
					i = -1;
					continue;
				}
				if (pendingCampaigns.get(i).getCampaignNumber() == campaignId && !approved) {
					campaignDAO.deleteCampaignByCampaignId(campaignId);
					pendingCampaigns.remove(i);
					i = -1;
					continue;
				}
			}
		}
		if (transactionId != null) {
			for (List<Transaction> list : pendingTransactions) {
				for (int i = 0; i < list.size(); i++) {
					if (list.get(i).getTransactionNumber() == transactionId && approved) {
						campaignDAO.contributeToCampaign(campaignDAO.getCampaignByKidId(list.get(i).getToKid()).getCampaignNumber(), list.get(i).getAmount());
						transactionDAO.approveTransaction(transactionId);
						list.remove(i);
						i = -1;
						continue;
					}
					if (list.get(i).getTransactionNumber() == transactionId && !approved) {
						transactionDAO.deleteTransactionByTransactionNumber(transactionId);;
						list.remove(i);
						i = -1;
						continue;
					}
				}
			}
		}
		if (adId != null) {
			for (List<Ad> list : pendingAds) {
				for (int i = 0; i < list.size(); i++) {
					if (list.get(i).getAdId() == adId && approved) {
						adDAO.approveAd(adId);;
						list.remove(i);
						i = -1;
						continue;
					}
					if (list.get(i).getAdId() == adId && !approved) {
						adDAO.deleteAdByAdId(adId);
						list.remove(i);
						i = -1;
						continue;
					}
				}
			}
		}
		if (commentId != null) {
			for (List<Comment> list : pendingComments) {
				for (int i = 0; i < list.size(); i++) {
					if (list.get(i).getCommentId() == commentId && approved) {
						commentDAO.approveComment(commentId);
						list.remove(i);
						i = -1;
						continue;
					}
					if (list.get(i).getCommentId() == commentId && !approved) {
						commentDAO.deleteCommentById(commentId);
						list.remove(i);
						i = -1;
						continue;
					}
				}
			}
		}
		if (pendingChangeId != null) {
			for (int i = 0; i < pendingProfileChanges.size(); i++) {
				if (pendingProfileChanges.get(i).getPendingRequestId() == pendingChangeId && approved) {
					
					if (pendingProfileChanges.get(i).getColumnName().equals("skills")) {
						
						if (pendingProfileChanges.get(i).getCurrentValue().equals("add")) {
							skillDAO.updateSkillsWithApprovedSkillByParent(pendingProfileChanges.get(i));
							pendingProfileChangeDAO.deletePendingChange(pendingChangeId);
							pendingProfileChanges.remove(i);
							i = -1;
							continue;
						} else {
							skillDAO.removeSkillsWithApprovalByParen(pendingProfileChanges.get(i));
							pendingProfileChangeDAO.deletePendingChange(pendingChangeId);
							pendingProfileChanges.remove(i);
							i = -1;
							continue;
						}
						
					} else {
						
						kidDAO.updateKidWithApprovedChange(pendingProfileChanges.get(i));
						pendingProfileChangeDAO.deletePendingChange(pendingChangeId);
						pendingProfileChanges.remove(i);
						i = -1;
						continue;
					}
	//				
	//				kidDAO.updateKidWithApprovedChange(pendingChanges.get(i));
	//				pendingChangeDAO.deletePendingChange(pendingChangeId);
	//				pendingChanges.remove(i);
	//				i = -1;
	//				continue;
				}
				
				if (pendingProfileChanges.get(i).getPendingRequestId() == pendingChangeId && !approved) {
					pendingProfileChangeDAO.deletePendingChange(pendingChangeId);
					pendingProfileChanges.remove(i);
					i = -1;
					continue;
				}
			}
		}
		return "redirect:/dashboard/guardian";
	}
	
	@RequestMapping(path="/dashboard/admin", method=RequestMethod.GET)
	public String displayAdminDashboard(HttpSession session) {
		Admin admin = (Admin) session.getAttribute("currentUser");
		List<PendingProfileChange> pendingProfileChanges = pendingProfileChangeDAO.getAllPendingChanges();
		List<HistoricalChange> historicalChanges = historicalChangeDAO.getAllHistoricalChanges();
		List<Comment> allComments = commentDAO.getAllComments();
		List<Ad> allAds = adDAO.getAllAds();
		//List<Campaign> allCampaigns = campaignDAO.getAllCampaigns();
		//This needs to be refactored into a method elsewhere. These numbers are not useful on their own.
		//Method should return all of them in a map.
		int totalUsers = 0; 
		totalUsers = userDAO.getTotalUsers(" ");
		int childUsers = 0;
		childUsers = userDAO.getTotalUsers("child");
		int guardianUsers = 0;
		guardianUsers = userDAO.getTotalUsers("guardian");
		int adminUsers = 0;
		adminUsers = userDAO.getTotalUsers("admin");
		session.setAttribute("totalUsers", totalUsers);
		session.setAttribute("childUsers", childUsers);
		session.setAttribute("guardianUsers", guardianUsers);
		session.setAttribute("adminUsers", adminUsers);
		session.setAttribute("allAds", allAds);
		session.setAttribute("allComments", allComments);
		//END OF CODE CRIME
		if (!pendingProfileChanges.isEmpty())
			session.setAttribute("pendingChanges", pendingProfileChanges);
		if (!historicalChanges.isEmpty())
			session.setAttribute("historicalChanges", historicalChanges);
		return "admin";
	}
	
	@RequestMapping(path="/dashboard/admin/results", method=RequestMethod.POST)
	public String displayAccountSearchResults(HttpSession session,
												@RequestParam(required=false) String query,
												@RequestParam String role,
												@RequestParam int control) {
		if (session.getAttribute("currentUser") instanceof Admin) {
			List<User> userResults = userDAO.searchUserByUserName(query, role);
			Map<User, Kid> results = new HashMap<User, Kid>();
			for (User user : userResults) {
				results.put(user, kidDAO.getKidByUserId(user.getUserId()));
			}
			List<User> disabledUsers = adminDAO.getAllSuspendedUsersByUsername(query);
			session.setAttribute("results", results);
			session.setAttribute("control", control);
			session.setAttribute("disabledUsers", disabledUsers);
			return "adminSearch";
		}
		session.invalidate();
		return "login";
	}
	
	@RequestMapping(path="/dashboard/admin/create_admin", method=RequestMethod.GET)
	public String displayAdminCreationPage(HttpSession session, Model model) {
		if (session.getAttribute("currentUser") instanceof Admin) {
			if(!model.containsAttribute("user")) {
				model.addAttribute("user", new User());
			}
			return "createAdmin";
		}
		session.invalidate();
		return "login";
	}
	
	@RequestMapping(path="/dashboard/admin/create_admin", method=RequestMethod.POST)
	public String createNewAdmin(@Valid @ModelAttribute("user") User user,
								@RequestParam String email,
			 					BindingResult result, ModelMap model, HttpSession session) {
		if (result.hasErrors()) {
			return "dashboard/admin/create_admin";
		}
		else {
			userDAO.insertUser(user);
			user = (User) userDAO.getUserByUserName(user.getUserName());
			adminDAO.saveAdmin(mapUserToAdmin(user, email));
		}
		return "redirect:/dashboard/admin";
	}
	
	@RequestMapping(path="/skills", method=RequestMethod.GET)
	public String displaySkillsPage(HttpSession session) {
		List<Skill> skillList = skillDAO.getAllSkills();
		session.setAttribute("skillList", skillList);
		
		return "skills";
	}
	
	@RequestMapping(path="/skills", method=RequestMethod.POST)

	public String displaySkillsPage(HttpSession session, @RequestParam String skillsList) {
		Kid kid = (Kid) session.getAttribute("currentUser");
		List<Skill> skillList = skillDAO.getSkillsByKidId(kid.getKidId());
		List<PendingProfileChange> pendingProfileChanges = new ArrayList<PendingProfileChange>();
		
		String[] skillId = skillsList.split(",");
		
	//	pendingChangeDAO.saveToPendingChanges();
		
		//removing
		for (int i=0; i < skillList.size(); i++) {
		//	skillDAO.removeSkillBySkillNameKidId(skillList.get(i).getSkillId(), kid.getKidId());	
			int count = 0;
			for (int j = 1; j < skillId.length; j++) {	
				if (skillList.get(i).getSkillId() == Integer.valueOf(skillId[j])) {
					count++;
				}
			}
			
			if (count > 0) {
				continue;
			} else {
				PendingProfileChange change = mapToPendingChange("skills", kid, skillList.get(i).getName() + "," + skillList.get(i).getSkillId(), "remove");
				pendingProfileChanges.add(change);	
			}
		}
		
		//adding
		for(String s : skillId) {
//			if (!s.isEmpty()) {
//				Skill skill = skillDAO.getSkillBySkillId(Integer.valueOf(s));
//		//		skillDAO.addSkillBySkillNameKidId(skill.getName(), kid.getKidId());
//				PendingChange change = mapToPendingChange("skills", kid, skill.getName() + "," + s, "add" );
//				pendingChanges.add(change);
//			}
			
			if (!s.isEmpty()) {
				Skill skill = skillDAO.getSkillBySkillId(Integer.valueOf(s));
				int count=0;
				
				for (int i = 0; i < skillList.size(); i++) {
					if (s.equals(String.valueOf(skillList.get(i).getSkillId()))) {
						count++;
					}
				}
				
				if (count > 0) {
					continue;
				} else {
					PendingProfileChange change = mapToPendingChange("skills", kid, skill.getName() + "," + s, "add" );
					pendingProfileChanges.add(change);
				}
			}
		}
		
		
		pendingProfileChangeDAO.saveToPendingChanges(pendingProfileChanges);

		
		return "redirect:/dashboard/profile";
	}
	
	private List<PendingProfileChange> checkForUpdates(Object kid, String firstName, String lastName, String dateOfBirth, 
										String logoFileName, String summary, String email, String businessName) {
		
		List<PendingProfileChange> pendingProfileChanges = new ArrayList<>();
		if (firstName != null && !((Kid) kid).getFirstName().equalsIgnoreCase(firstName))
			pendingProfileChanges.add(mapToPendingChange("firstName", (Kid) kid, firstName, ((Kid) kid).getFirstName()));
		
		if (lastName != null && !((Kid) kid).getLastName().equalsIgnoreCase(lastName))
			pendingProfileChanges.add(mapToPendingChange("lastName", (Kid) kid, lastName, ((Kid) kid).getLastName()));
		
		if (dateOfBirth != null && !createBirthDateString(((Kid) kid)).equalsIgnoreCase(dateOfBirth))
			pendingProfileChanges.add(mapToPendingChange("dateOfBirth", (Kid) kid, dateOfBirth, createBirthDateString((Kid) kid)));
		
		if (logoFileName != null && !((Kid) kid).getLogoFileName().equalsIgnoreCase(logoFileName))
			pendingProfileChanges.add(mapToPendingChange("logoFileName", (Kid) kid, logoFileName, ((Kid) kid).getLogoFileName()));
		
		if (summary != null && !((Kid) kid).getSummary().equalsIgnoreCase(summary))
			pendingProfileChanges.add(mapToPendingChange("summary", (Kid) kid, summary, ((Kid) kid).getSummary()));
		
		if (email != null && !((Kid) kid).getEmail().equalsIgnoreCase(email))
			pendingProfileChanges.add(mapToPendingChange("email", (Kid) kid, email, ((Kid) kid).getEmail()));
		
		if (businessName != null && !((Kid) kid).getBusinessName().equalsIgnoreCase(businessName)) 
			pendingProfileChanges.add(mapToPendingChange("businessName", (Kid) kid, businessName, ((Kid) kid).getBusinessName()));;
				
		return pendingProfileChanges;
	}
	
	private String createBirthDateString (Kid kid) {
		String result = "";
		if (kid.getDateOfBirth().getMonthValue() < 10)
			result += "0" + kid.getDateOfBirth().getMonthValue() + "-";
		else
			result += kid.getDateOfBirth().getMonthValue() + "-";
		if (kid.getDateOfBirth().getDayOfMonth() < 10)
			result += "0" + kid.getDateOfBirth().getDayOfMonth() + "-";
		else
			result += kid.getDateOfBirth().getDayOfMonth() + "-";
		result += kid.getDateOfBirth().getYear();
		return result;
	}
	
	private PendingProfileChange mapToPendingChange(String column, Kid kid, String change, String current) {
		PendingProfileChange pendingProfileChange = new PendingProfileChange();
		pendingProfileChange.setApproved(false);
		pendingProfileChange.setUserId(kid.getUserId());
		pendingProfileChange.setGuardianId(kid.getGuardianId());
		pendingProfileChange.setKidId(kid.getKidId());
		pendingProfileChange.setColumnName(column);
		pendingProfileChange.setCurrentValue(current);
		pendingProfileChange.setRequestedValue(change);
		pendingProfileChange.setCreated(new Timestamp(System.currentTimeMillis()));
		return pendingProfileChange;
	}
	
	private Transaction mapToNewTransaction(Integer toKid, Integer fromKid, double amount) {
		Transaction transaction = new Transaction();
		transaction.setFromKid(fromKid);
		transaction.setToKid(toKid);
		transaction.setAmount(amount);
		transaction.setTransactionType("Funding");
		transaction.setTransactionTime(new Timestamp(System.currentTimeMillis()));
		transaction.setTransactionApproved(false);
		return transaction;
	}
	
	private Admin mapUserToAdmin(User user, String email) {
		Admin admin = new Admin();
		admin.setUserId(user.getUserId());
		admin.setAccessLevel();
		admin.setEmail(email);
		admin.setUserName(user.getUserName());
		return admin;
	}

}
