package com.techelevator.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.techelevator.FileServlet;
import com.techelevator.RandomString;
import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.PendingProfileChange;
import com.techelevator.model.DAO.GuardianDAO;
import com.techelevator.model.DAO.HistoricalChangeDAO;
import com.techelevator.model.DAO.KidDAO;
import com.techelevator.model.DAO.PendingProfileChangeDAO;

@Controller
public class UploadController {
	
	private PendingProfileChangeDAO pendingProfileChangeDAO;
	private GuardianDAO guardianDAO;
	private KidDAO kidDAO;
	
	
	@Autowired
	ServletContext servletContext;
	public UploadController(PendingProfileChangeDAO pendingProfileChangeDAO, GuardianDAO guardianDAO, KidDAO kidDAO) {
		this.pendingProfileChangeDAO = pendingProfileChangeDAO;
		this.guardianDAO = guardianDAO;
		this.kidDAO = kidDAO;
	}
	
	@RequestMapping(path="/upload", method=RequestMethod.GET)
	public String uploadAFile() {
		return "upload";
	}
	
	@RequestMapping(path="/upload", method=RequestMethod.POST)
	public String selectedFileIsUploading(ModelMap map, HttpSession session, @RequestParam MultipartFile file) throws IOException { //will need param for kid id
		File imagePath = getImageFilePath();
		String imageName = imagePath + File.separator + "testImage";
		
		if (file.isEmpty()) {
			map.addAttribute("message", "File Object empty");
		} else {
			createImage(file, imageName);
		}
		String fileName = moveFileAround(file);
		if (fileName.isEmpty()) {
			return "redirect:/upload";
		}
		session.setAttribute("logoId", fileName);
		if (session.getAttribute("currentUser") instanceof Kid) {
			Kid kid = (Kid) session.getAttribute("currentUser");
			List<PendingProfileChange> logoUpdate = new ArrayList<PendingProfileChange>();
			logoUpdate.add(mapToPendingChange("logoFileName", kid, FileServlet.logoFolder + "/" + fileName, kid.getLogoFileName()));
			pendingProfileChangeDAO.saveToPendingChanges(logoUpdate);
			pendingProfileChangeDAO.saveToHistoricalChanges(logoUpdate.get(0));
		} else if (session.getAttribute("currentUser") instanceof Guardian){
			int kidIdForLogo = 1; //This will be sent in the post request, ultimately, as a parameter in the submit
			Kid kid = kidDAO.getKidByKidId(kidIdForLogo);
			PendingProfileChange pendingProfileChange = mapToPendingChange("logoFileName", kid, FileServlet.logoFolder + "/" + fileName, kid.getLogoFileName());
			pendingProfileChangeDAO.saveToHistoricalChanges(pendingProfileChange);
			kidDAO.updateKidWithApprovedChange(pendingProfileChange);
			return "redirect:/dashboard/guardian";
		}
    return "redirect:/dashboard/profile";
	}

	@RequestMapping("/success")
	public String returnSuccess() {
		return "success";
	}
	
	private File getImageFilePath() {
		String serverPath = getServerContextPath();
		File filePath = new File(serverPath);
		if (!filePath.exists()) {
			filePath.mkdirs();
		}
		return filePath;
	}
	
	private String getServerContextPath() {
		return servletContext.getRealPath("/") + "img/uploaded_logos";
	}
	
	private void createImage(MultipartFile file, String name) {
		File image = new File(name);
		try (BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(image))) {
	
			stream.write(file.getBytes());
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String moveFileAround(MultipartFile file) throws IllegalStateException, IOException {
		RandomString random = new RandomString(10, ThreadLocalRandom.current());
		String extension = FilenameUtils.getExtension(file.getOriginalFilename());
		String hash = random.nextString();
		if (!extensionChecker(extension)) {
			return "";
		}
		file.transferTo(new File(FileServlet.logoFolder + "/" + hash + "." + extension));
		return hash + "." + extension;
	}
	
	private boolean extensionChecker(String extension) {
		return extension.contains("jpg") || extension.contains("png") || extension.contains("jpeg");
	}
	
	private PendingProfileChange mapToPendingChange(String column, Kid kid, String change, String current) {
		PendingProfileChange pendingProfileChange = new PendingProfileChange();
		pendingProfileChange.setApproved(false);
		pendingProfileChange.setUserId(kid.getUserId());
		pendingProfileChange.setGuardianId(kid.getGuardianId());
		pendingProfileChange.setKidId(kid.getKidId());
		pendingProfileChange.setColumnName(column);
		pendingProfileChange.setCurrentValue(current);
		pendingProfileChange.setRequestedValue(change);
		pendingProfileChange.setCreated(new Timestamp(System.currentTimeMillis()));
		return pendingProfileChange;
	}
}
