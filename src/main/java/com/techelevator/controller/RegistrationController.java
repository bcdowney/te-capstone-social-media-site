package com.techelevator.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import com.techelevator.FileServlet;
import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.User;
import com.techelevator.model.DAO.AdminDAO;
import com.techelevator.model.DAO.GuardianDAO;
import com.techelevator.model.DAO.KidDAO;
import com.techelevator.model.DAO.UserDAO;

@SessionAttributes({"currentUser", "user", "kidUser", "kid"})
@Controller
public class RegistrationController {
	
	KidDAO kidDAO;
	GuardianDAO guardianDAO;
	AdminDAO adminDAO;
	UserDAO userDAO;
	
	@Autowired
	public RegistrationController(KidDAO kidDAO, GuardianDAO guardianDAO, AdminDAO adminDAO, UserDAO userDAO) {
		this.kidDAO = kidDAO;
		this.guardianDAO = guardianDAO;
		this.adminDAO = adminDAO;
		this.userDAO = userDAO;
	}
	
	@RequestMapping(path="/accountRequest", method=RequestMethod.GET)
	public String createRequestForParentSignUp() {
		return "accountRequest";
	}
	
	@RequestMapping(path="/accountRequest1", method=RequestMethod.GET)
	public String sendRequestForParentSignUp(@RequestParam String firstName, @RequestParam String lastName, @RequestParam String email) { 
		return "accountRequestConfirmation";
	}
	
	@RequestMapping(path="/userRegistration", method=RequestMethod.GET)
	public String newUserPage (Model model) {
		if(!model.containsAttribute("user")) {
			model.addAttribute("user", new User());
		}
		return "userRegistration";
	}
	
	@RequestMapping(path="/userInput", method=RequestMethod.POST)
	public String createNewUser (@Valid @ModelAttribute("user") User user, BindingResult result, ModelMap model, HttpSession session) {
		if(result.hasErrors()) {
			return "/userRegistration";
		}
		else {
			User user1 = (User) model.get("user");
			userDAO.insertUser(user1);
			session.setAttribute("user", user1);
		}
		return "redirect:/guardianRegistration";
	}
	
	@RequestMapping(path="/guardianRegistration", method=RequestMethod.GET)
	public String newGuardianRegistration(Model model) {
		if(!model.containsAttribute("currentUser")) {
			model.addAttribute("currentUser", new Guardian());
		}
		return "guardianRegistration";
	}
	
	@RequestMapping(path="/guardianRegistration", method=RequestMethod.POST)
	public String sendGuardianRegistration(@Valid @ModelAttribute("currentUser") Guardian guardian, 
											BindingResult result, ModelMap model,
											HttpSession session){
		if(result.hasErrors()) {
			return "/guardianRegistration";
		}
		else {
			User user = (User) model.get("user");
			int userId = userDAO.getUserIdByUsername(user.getUserName());
			Guardian newGuardian = (Guardian) model.get("currentUser");
			newGuardian.setUserId(userId);
			guardianDAO.insertNewGuardian(newGuardian);
			session.setAttribute("currentUser", newGuardian);
			return "redirect:/addChildUser";
		}
	}		
	
	@RequestMapping(path="/addChildUser", method=RequestMethod.GET)
	public String getChildUserPage(ModelMap model) {
		if(!model.containsAttribute("kidUser")){
			model.addAttribute("kidUser", new User());
		}
		return "addMyChild";
	}
	
	@RequestMapping(path="/addChildUser", method=RequestMethod.POST)
	public String sendChildUserPage(@Valid @ModelAttribute("kidUser") User user, 
									BindingResult result, ModelMap model, HttpSession session) {
		if (result.hasErrors()) {
			return "/addChildUser";
		}
		else {
			User user2 = (User) model.get("kidUser");
			userDAO.insertUser(user2);
			session.setAttribute("kidUser", user2);
		}
		return "redirect:/addChildProfile";
	}
	
	@RequestMapping(path="/addChildProfile", method=RequestMethod.GET)
	public String getChildProfilePage(ModelMap model) {
		if(!model.containsAttribute("kid")) {
			model.addAttribute("kid", new Kid());
		}
		return "createChildProfile";
	}
	
	@RequestMapping(path="/addChildProfile", method=RequestMethod.POST)
	public String childAccountRegistered(@Valid @ModelAttribute("kid") Kid kid,  
										 BindingResult result, ModelMap model, HttpSession session) {
		if (result.hasErrors()) {
			return "createChildProfile";
		}
		else {
			User newUser = (User) model.get("kidUser");
			Kid newKid = (Kid) model.get("kid");
			Guardian newGuardian = (Guardian) model.get("currentUser");
			newKid.setUserId(userDAO.getUserIdByUsername(newUser.getUserName()));
			newKid.setGuardianId(guardianDAO.getGuardianIdByUserId(newGuardian.getUserId()));
			kidDAO.insertNewTemporaryKid(setKidPlaceholderValues(newKid));
			guardianDAO.insertGuardianAndKid(kidDAO.getKidIdByKidUserId(newKid.getUserId()), guardianDAO.getGuardianIdByUserId(newGuardian.getUserId()));
			session.setAttribute("kid", newKid);
		}
		return "redirect:/dashboard/guardian";
	}
		
	private Kid setKidPlaceholderValues(Kid kid) {
		kid.setBusinessName("Enter Business Name Here");
		kid.setLogoFileName(FileServlet.logoFolder + "/logo-default.png");
		kid.setSummary("Enter Summary Here");
		kid.setEmail("placeholder@example.com");
		return kid;
	}

}
