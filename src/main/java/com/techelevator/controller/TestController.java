package com.techelevator.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.Admin;
import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.User;
import com.techelevator.model.DAO.AdminDAO;
import com.techelevator.model.DAO.GuardianDAO;
import com.techelevator.model.DAO.KidDAO;
import com.techelevator.model.DAO.UserDAO;

@Controller
public class TestController {
	
	private KidDAO kidDAO;
	private GuardianDAO guardianDAO;
	private AdminDAO adminDAO;
	private UserDAO userDAO;

	@Autowired
	public TestController(KidDAO kidDAO, GuardianDAO guardianDAO, AdminDAO adminDAO, UserDAO userDAO) {
		this.kidDAO = kidDAO;
		this.guardianDAO = guardianDAO;
		this.adminDAO = adminDAO;
		this.userDAO = userDAO; 
	}

	@RequestMapping(path= {"/login", "/"}, method=RequestMethod.GET)
	public String displayLoginForm(HttpSession session) {
		if (session.getAttribute("currentUser") != null) {
			session.removeAttribute("currentUser");
			session.invalidate();
		}
		
		return "login";
	}
	
	@RequestMapping(path="/login", method=RequestMethod.POST)
	public String login(HttpSession session, @RequestParam(required=true) String username,
											@RequestParam(required=true) String password){
		if(userDAO.searchForUsernameAndPassword(username, password)) {
			int userId = userDAO.getUserIdByUsername(username);
			User user = (User) userDAO.getUserByUserName(username);
			if (user.getRole().equals("admin")) {
				Admin admin = adminDAO.getAdminByUserId(userId);
				session.setAttribute("currentUser", admin);
				return "redirect:/dashboard";
			}
			else if (user.getRole().equals("kid")) {
				Kid kid = kidDAO.getKidByUserId(userId);
				session.setAttribute("currentUser", kid);
				return "redirect:/dashboard";
			}
			else if(user.getRole().equals("guardian")) {
				Guardian guardian = guardianDAO.getGuardianByUserId(userId);
				session.setAttribute("currentUser", guardian);
				return "redirect:/dashboard";
			}
		}
		return "redirect:/login";
	}
	
	@RequestMapping(path="/loginFAKE", method=RequestMethod.POST)
	public String loginFAKE(HttpSession session, @RequestParam(required=false) Integer kidId, 
											@RequestParam(required=false) Integer guardianId, 
											@RequestParam(required=false) Integer userId,
											@RequestParam(required=false) Integer businessId){
		if (kidId != null && businessId == null) {
			Kid kid = kidDAO.getKidByKidId(kidId);
			session.setAttribute("currentUser", kid);
			return "redirect:/dashboard";
		} else if (guardianId != null) {
			Guardian guardian = guardianDAO.getGuardianByGuardianId(guardianId);
			session.setAttribute("currentUser", guardian);
			return "redirect:/dashboard";
		} else if (userId != null){
			Admin admin = adminDAO.getAdminByUserId(userId);
			session.setAttribute("currentUser", admin);
			return "redirect:/dashboard";
		} else if (businessId != null && kidId != null) {
			Kid kid = kidDAO.getKidByKidId(2);
			session.setAttribute("currentUser", kid);
			return "redirect:/dashboard/profile?businessId=" + businessId;
		} else {
			return "redirect:/login";
		}
	}
	
}
