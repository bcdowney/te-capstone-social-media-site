package com.techelevator.model;

import java.sql.Timestamp;

public class HistoricalChange {
	
	private int historicalChangeId;
	private int changeInitiatedById;
	private int changeToUserId;
	private String  changedColumn;
	private String oldValue;
	private String newValue;
	private Timestamp requestTime;
	private Timestamp completeTime;
	private String changeByUserName;
	private String changeToUserName;
	private String changeInitiatedByRole;
	
	public int getHistoricalChangeId() {
		return historicalChangeId;
	}
	public void setHistoricalChangeId(int historicalChangeId) {
		this.historicalChangeId = historicalChangeId;
	}
	public int getChangeInitiatedById() {
		return changeInitiatedById;
	}
	public void setChangeInitiatedById(int changeInitiatedById) {
		this.changeInitiatedById = changeInitiatedById;
	}
	public int getChangeToUserId() {
		return changeToUserId;
	}
	public void setChangeToUserId(int changeToUserId) {
		this.changeToUserId = changeToUserId;
	}
	public String getChangedColumn() {
		return changedColumn;
	}
	public void setChangedColumn(String changedColumn) {
		this.changedColumn = changedColumn;
	}
	public String getOldValue() {
		return oldValue;
	}
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}
	public String getNewValue() {
		return newValue;
	}
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	public Timestamp getRequestTime() {
		return requestTime;
	}
	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}
	public Timestamp getCompleteTime() {
		return completeTime;
	}
	public void setCompleteTime(Timestamp completeTime) {
		this.completeTime = completeTime;
	}
	public String getChangeByUserName() {
		return changeByUserName;
	}
	public void setChangeByUserName(String changeByUserName) {
		this.changeByUserName = changeByUserName;
	}
	public String getChangeToUserName() {
		return changeToUserName;
	}
	public void setChangeToUserName(String changeToUserName) {
		this.changeToUserName = changeToUserName;
	}
	public String getChangeInitiatedByRole() {
		return changeInitiatedByRole;
	}
	public void setChangeInitiatedByRole(String changeInitiatedByRole) {
		this.changeInitiatedByRole = changeInitiatedByRole;
	}
	
	

}
