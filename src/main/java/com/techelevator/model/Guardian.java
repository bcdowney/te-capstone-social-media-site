package com.techelevator.model;

import java.time.LocalDate;
import java.sql.Date;
import java.util.List;

public class Guardian extends UsersAbstract {
	
	private LocalDate dateOfBirth;
	private int guardianID;
	private List<Kid> children;
	
	public void setAccessLevel() {
		super.accessLevel = "guardian";
	}

	public List<Kid> getChildren() {
		return children;
	}

	public void setChildren(List<Kid> children) {
		this.children = children;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		LocalDate birthDate = dateOfBirth.toLocalDate();
		this.dateOfBirth = birthDate;
	}
	
	public int getGuardianID() {
		return guardianID;
	}

	public void setGuardianID(int guardianID) {
		this.guardianID = guardianID;
	}


}
