package com.techelevator.model;

import java.time.LocalDate;
import java.sql.Date;

public class Kid extends UsersAbstract {
	
	private int kidId;
	private int guardianId;
	private LocalDate dateOfBirth;
	private String businessName;
	private String logoFileName;
	private String summary;
	private String[] skillName;
	private int transactionsPerDay;
	private double transactionAmountLimit;
	
	
	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public void setAccessLevel() {
		super.accessLevel = "child";
	}
	
	public int getKidId() {
		return kidId;
	}
	public void setKidId(int kidId) {
		this.kidId = kidId;
	}
	public int getGuardianId() {
		return guardianId;
	}

	public void setGuardianId(int guardianId) {
		this.guardianId = guardianId;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		LocalDate birthDate = dateOfBirth.toLocalDate();
		this.dateOfBirth = birthDate;
	}

	public String getLogoFileName() {
		return logoFileName;
	}
	public void setLogoFileName(String logoFileName) {
		this.logoFileName = logoFileName;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getTransactionsPerDay() {
		return transactionsPerDay;
	}

	public void setTransactionsPerDay(int transactionsPerDay) {
		this.transactionsPerDay = transactionsPerDay;
	}

	public double getTransactionAmountLimit() {
		return transactionAmountLimit;
	}

	public void setTransactionAmountLimit(double transactionAmountLimit) {
		this.transactionAmountLimit = transactionAmountLimit;
	}
	
	
	

}
