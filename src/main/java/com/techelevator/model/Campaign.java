package com.techelevator.model;

import java.sql.Timestamp;

public class Campaign {
	
	private int campaignNumber;
	private int kidId;
	private double goal;
	private double current;
	private Timestamp createDate;
	private String title;
	private String description;
	private boolean approved;
	
	public int getCampaignNumber() {
		return campaignNumber;
	}
	public void setCampaignNumber(int campaignNumber) {
		this.campaignNumber = campaignNumber;
	}
	public int getKidId() {
		return kidId;
	}
	public void setKidId(int kidId) {
		this.kidId = kidId;
	}
	public double getGoal() {
		return goal;
	}
	public void setGoal(double goal) {
		this.goal = goal;
	}
	public double getCurrent() {
		return current;
	}
	public void setCurrent(double current) {
		this.current = current;
	}
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isApproved() {
		return approved;
	}
	public void setApproved(boolean approved) {
		this.approved = approved;
	}
	
	

}
