package com.techelevator.model;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class PendingProfileChange {
	
	private static Map<String, String> KID_OBJECT_ATTRIBUTES = new HashMap<String, String>() {
			{
				put("first_name", "firstName");
				put("last_name", "lastName");
				put("date_of_birth", "dateOfBirth");
				put("business_name", "businessName");
				put("logo_file_name", "logoFileName");
				put("summary", "summary");
				put("email", "email");
				put("skills", "skills");
			}
	};
	
	private static Map<String, String> SQL_COMMANDS = new HashMap<String, String>() {
		{
			put("firstName", "first_name");
			put("lastName", "last_name");
			put("businessName", "business_name");
			put("dateOfBirth", "date_of_birth");
			put("logoFileName", "logo_file_name");
			put("summary", "summary");
			put("email", "email");
			put("skills", "skills");
		}
	};
	
	private int pendingRequestId;
	private int userId;
	private int kidId;
	private int guardianId;
	private int skillId;
	private boolean approved;

	private String columnName;
	private String currentValue;
	private String requestedValue;
	private String businessName;
	private Timestamp created;
	
	private String kidUserName;
	private String guardianUserName;


	public int getPendingRequestId() {
		return pendingRequestId;
	}
	public void setPendingRequestId(int pendingRequestId) {
		this.pendingRequestId = pendingRequestId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getKidId() {
		return kidId;
	}
	public void setKidId(int kidId) {
		this.kidId = kidId;
	}
	public int getGuardianId() {
		return guardianId;
	}
	public void setGuardianId(int guardianId) {
		this.guardianId = guardianId;
	}
	public String getColumnName() {
		String temp = KID_OBJECT_ATTRIBUTES.get(this.columnName);
		return temp;
	}
	public void setColumnName(String columnName) {
		String temp = SQL_COMMANDS.get(columnName);
		this.columnName = temp;
	}
	public String getCurrentValue() {
		return currentValue;
	}
	public void setCurrentValue(String currentValue) {
		this.currentValue = currentValue;
	}
	public String getRequestedValue() {
		return requestedValue;
	}
	public void setRequestedValue(String requestedValue) {
		this.requestedValue = requestedValue;
	}
	public boolean isApproved() {
		return approved;
	}
	public void setApproved(boolean approved) {
		this.approved = approved;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public int getSkillId() {
		return skillId;
	}
	public void setSkillId(int skillId) {
		this.skillId = skillId;
	}
	public String getKidUserName() {
		return kidUserName;
	}
	public void setKidUserName(String kidUserName) {
		this.kidUserName = kidUserName;
	}
	public String getGuardianUserName() {
		return guardianUserName;
	}
	public void setGuardianUserName(String guardianUserName) {
		this.guardianUserName = guardianUserName;
	}

	
	
	
}
