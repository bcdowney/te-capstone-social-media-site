package com.techelevator.model;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

public class Ad {
	
	private int adId;
	private int kidId;
	private String adType;
	private String title;
	private String description;
	private String businessName;
	private Time startTime;
	private Time endTime;
	private Date startDate;
	private Date endDate;
	private Timestamp created;
	private double payDollars;
	private String barterDescription;
	private boolean approved;
	private String logoFileName;
	
	public int getAdId() {
		return adId;
	}
	public void setAdId(int adId) {
		this.adId = adId;
	}
	public int getKidId() {
		return kidId;
	}
	public void setKidId(int kidId) {
		this.kidId = kidId;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getAdType() {
		return adType;
	}
	public void setAdType(String adType) {
		this.adType = adType;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Time getStartTime() {
		return startTime;
	}
	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}
	public Time getEndTime() {
		return endTime;
	}
	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public double getPayDollars() {
		return payDollars;
	}
	public void setPayDollars(double payDollars) {
		this.payDollars = payDollars;
	}
	public String getBarterDescription() {
		return barterDescription;
	}
	public void setBarterDescription(String barterDescription) {
		this.barterDescription = barterDescription;
	}
	public boolean isApproved() {
		return approved;
	}
	public void setApproved(boolean approved) {
		this.approved = approved;
	}
	public String getLogoFileName() {
		return logoFileName;
	}
	public void setLogoFileName(String logoFileName) {
		this.logoFileName = logoFileName;
	}

}
