package com.techelevator.model.DAO;

import com.techelevator.model.User;
import java.util.List;

public interface UserDAO {

	public void saveUser(String userName, String password);

	public boolean searchForUsernameAndPassword(String userName, String password);

	public void updatePassword(String userName, String password);

	public Object getUserByUserName(String userName);
	
	public void insertUser(User user);
	
	public int getUserIdByUsername(String userName);
	
	//For Admin dashboard/info
	public int getTotalUsers(String userRole);
	
	public String getUserNameById(int userId);

	public List<User> searchUserByUserName(String searchString, String role);

}
