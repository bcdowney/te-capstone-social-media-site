package com.techelevator.model.DAO;

import java.sql.Timestamp;
import java.util.List;

import com.techelevator.model.Transaction;

public interface TransactionDAO {
	
	public void insertTransaction(Transaction transaction);
	
	public void deleteTransactionByTransactionNumber(int transactionNumber);
	
	public Transaction getTransactionByTransactionNumber(int transactionNumber);
	
	public List<Transaction> getAllTransactionsByFromKidId(int fromKidId);
	
	public List<Transaction> getAllTransactionsByToKidId(int toKidId);
	
	public int getNumberOfTransactionsFromBeginningByFromKidId(Timestamp startTime, Timestamp endTime, int fromKidId);
	
	public double getTotalOfTransactionsFromBeginningByFromKidId(Timestamp startTime, int fromKidId);
	
	public double getTotalOfTransactionsFromBeginningByToKidId(Timestamp startTime, int toKidId);
	
	public double getTotalOfTransactionsBetweenTimesByFromKidId(Timestamp startTime, Timestamp endTime, int fromKidId);
	
	public double getTotalOfTransactionsBetweenTimesByToKidId(Timestamp startTime, Timestamp endTime, int toKidId);

	public List<Transaction> getAllUnapprovedTransactionsByFromKidId(int fromKidId);

	public void approveTransaction(int transactionNumber);
	
}
