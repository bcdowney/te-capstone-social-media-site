package com.techelevator.model.DAO;

import java.util.List;

import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;

public interface GuardianDAO {
	
	public List<Kid> getAllKids(int guardianId);
	
	public Guardian getGuardianByUserId(int userId);
	
	public int getGuardianIdByUserId(int userId);
	
	public Guardian getGuardianByGuardianId(int guardianId);
	
	public void insertNewGuardian(Guardian guardian);
	
	public void insertGuardianAndKid(int kidId, int guardianId);
	
	public Guardian getGuardianByEmail(String email);
	
	public void updateGuardianByGuardianId(int guardianId, String column, String updateValue);

}
