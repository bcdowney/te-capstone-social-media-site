package com.techelevator.model.DAO;

import java.util.List;

import com.techelevator.model.Campaign;

public interface CampaignDAO {
	
	public void insertCampaign(Campaign campaign);
	
	public void deleteCampaignByCampaignId(int campaignId);
	
	public Campaign getCampaignByCampaignId(int campaignId);
	
	public Campaign getCampaignByKidId(int kidId);
	
	public List<Campaign> getAllCampaigns();

	public Campaign getUnapprovedCampaignByKidId(int kidId);

	public void approveCampaign(int campaignId);
	
	public void contributeToCampaign(int campaignId, double money);

	public List<Campaign> getAllApprovedCampaigns();

}
