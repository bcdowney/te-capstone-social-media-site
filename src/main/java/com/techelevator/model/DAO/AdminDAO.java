package com.techelevator.model.DAO;

import java.util.List;

import com.techelevator.model.Admin;
import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.User;

public interface AdminDAO {
	
	public Admin getAdminByUserId(int userId);
	
	public List<Admin> getAllAdmins();	
	
	public List<Kid> getKidsByGuardianId(int guardianId);
		
	public Guardian getGuardianByUserId(int userId);
	
	public Guardian getGuardianByGuardianId(int guardianId);
	
	public Guardian getGuardianByKidId(int kidId);
	
	public List<Guardian> getAllGuardians();

	public void saveAdmin(Admin newAdmin);

	public List<User> getAllSuspendedUsersByUsername(String username);

	public void disableUser(User user);
	
}
