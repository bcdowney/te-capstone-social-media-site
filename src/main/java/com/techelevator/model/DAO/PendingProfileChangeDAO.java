package com.techelevator.model.DAO;

import java.util.List;

import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.PendingProfileChange;

public interface PendingProfileChangeDAO {
	
	public List<PendingProfileChange> getPendingChangesByGuardian(Guardian guardian);
	
	public List<PendingProfileChange> getPendingChangesByKid(Kid kid);
	
	public List<PendingProfileChange> getAllPendingChanges();
	
	public void deletePendingChange(int pendingChangeId);
	
	public void saveToHistoricalChanges(PendingProfileChange change);
	
	public void saveToPendingChanges(List<PendingProfileChange> pendingProfileChanges);

}
