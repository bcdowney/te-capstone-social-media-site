package com.techelevator.model.DAO;

import java.util.List;

import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.PendingProfileChange;

public interface KidDAO {
	
	public Kid getKidByUserId(long userId);
	
	public Kid getKidByKidId(long kidId);
	
	public int getKidIdByKidUserId(int kidUserId);
	
	public void updateKidWithApprovedChange(PendingProfileChange pendingProfileChange);
	
	public void registerNewKid(Kid kid, Guardian guardian);
	
	public void insertNewTemporaryKid(Kid kid);
	
	public int getTransactionsPerDayLimitByKidId(int kidId);
	
	public void updateTransactionsPerDayLimitByKidId(int kidId, int transactionsLimit);
	
	public double getTransactionAmountLimitByKidId(int kidId);
	
	public void updateTransactionAmountLimitByKidId(int kidId, double amountLimit);
	
}
