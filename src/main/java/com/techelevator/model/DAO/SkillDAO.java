package com.techelevator.model.DAO;

import java.util.List;

import com.techelevator.model.PendingProfileChange;
import com.techelevator.model.Skill;

public interface SkillDAO {

	public void addSkillBySkillNameKidId(String skillName, int kidId);
	
	public void removeSkillBySkillNameKidId(int skillId, int kidId);
	
	public List<Skill> getSkillsByKidId(int kidId);
	
	public List<Skill> getAllSkills();
	
	public void saveNewSkill(String skillName);

	public Skill getSkillBySkillId(int skillId);

	void updateSkillsWithApprovedSkillByParent(PendingProfileChange pendingProfileChange);

	void removeSkillsWithApprovalByParen(PendingProfileChange pendingProfileChange);

}
