package com.techelevator.model.DAO;

import java.util.List;

import com.techelevator.model.HistoricalChange;

public interface HistoricalChangeDAO {
	
	public List<HistoricalChange> getAllHistoricalChanges();
	
	public HistoricalChange getHistoricalChangeById(int id);
	
	/* HEADS UP - 
	 * Currently we have a method that saves to Historical Changes in the PendingChangeDAO.
	 * We will also need a method here that deals with saving changes that are not from the 
	 * kid_users_pending_changes table as well. This is not included in the current sprint, so this can be 
	 * disregarded for now - saving this in here as a reminder.
	 */

}
