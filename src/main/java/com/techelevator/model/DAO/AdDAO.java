package com.techelevator.model.DAO;

import java.util.List;

import com.techelevator.model.Ad;

public interface AdDAO {
	
	public List<Ad> getAllAds();
	
	public Ad getAdByAdId(int adId);
	
	public boolean isAdApproved(int adId);
	
	public List<Ad> getAllJobs();
	
	public List<Ad> getAllSkills();
	
	public void insertAd(Ad ad);
	
	public void approveAd(int adId);
	
	public void deleteAdByAdId(int adId);
	
	public List<Ad> getApprovedAdsbyKidId(int kidId);
	
	public List<Ad> getUnapprovedAdsbyKidId(int kidId);

}
