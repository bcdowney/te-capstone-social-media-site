package com.techelevator.model.DAO;

import java.util.List;

import com.techelevator.model.Comment;

public interface CommentDAO {
	
	public List<Comment> getAllComments();
	
	public List<Comment> getCommentsByAdId(int adId);
	
	public List<Comment> getCommentsByKidId(int kidId);
	
	public List<Comment> getApprovedCommentsByKidId(int kidId);
	
	public List<Comment> getUnapprovedCommentsByKidId(int kidId);
	
	public void deleteCommentById(int commentId);
	
	public void insertComment(Comment comment);
	
	public void approveComment(int commentId);

}
