package com.techelevator.model;

import java.sql.Timestamp;

public class Transaction {
	
	private int transactionNumber;
	private int fromKid;
	private int toKid;
	private double amount;
	private String transactionType;
	private Timestamp transactionTime;
	private boolean transactionApproved;
	
	public int getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber(int transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	public int getFromKid() {
		return fromKid;
	}
	public void setFromKid(int fromKid) {
		this.fromKid = fromKid;
	}
	public int getToKid() {
		return toKid;
	}
	public void setToKid(int toKid) {
		this.toKid = toKid;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public Timestamp getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Timestamp transactionTime) {
		this.transactionTime = transactionTime;
	}
	public boolean isTransactionApproved() {
		return transactionApproved;
	}
	public void setTransactionApproved(boolean transactionApproved) {
		this.transactionApproved = transactionApproved;
	}
	
	

}
