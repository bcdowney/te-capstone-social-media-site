package com.techelevator.model.JDBC;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.Transaction;
import com.techelevator.model.DAO.TransactionDAO;

@Component
public class JDBCTransactionDAO extends JDBCUsersAbstractDAO implements TransactionDAO{
	
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JDBCTransactionDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Transaction getTransactionByTransactionNumber(int transactionNumber) {
		Transaction transaction = new Transaction();
		String sql = "SELECT * FROM transactions WHERE transaction_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, transactionNumber);
		if (result.next()) {
			transaction = mapToTransaction(result);
		}
		return transaction;
	}

	@Override
	public List<Transaction> getAllTransactionsByFromKidId(int fromKidId) {
		List<Transaction> transactions = new ArrayList<Transaction>();
		String sql = "SELECT * FROM transactions WHERE from_kid_id = ? AND approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, fromKidId);
		while (result.next()) {
			Transaction transaction = mapToTransaction(result);
			transactions.add(transaction);
		}
		return transactions;
	}

	@Override
	public List<Transaction> getAllTransactionsByToKidId(int toKidId) {
		List<Transaction> transactions = new ArrayList<Transaction>();
		String sql = "SELECT * FROM transactions WHERE to_kid_id = ? AND approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, toKidId);
		while (result.next()) {
			Transaction transaction = mapToTransaction(result);
			transactions.add(transaction);
		}
		return transactions;
	}

	@Override
	public double getTotalOfTransactionsFromBeginningByFromKidId(Timestamp startTime, int fromKidId) { //start time is the user superclass attribute "createDate"
		double total = 0.0;
		String sql = "SELECT SUM(amount) as total FROM transactions WHERE from_kid_id = ? AND transaction_timestamp >= ? "
				+ "AND approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, fromKidId, startTime);
		if (result.next()) {
			total = result.getDouble("total");
		}
		return total;
	}

	@Override
	public double getTotalOfTransactionsFromBeginningByToKidId(Timestamp startTime, int toKidId) {
		double total = 0.0;
		String sql = "SELECT SUM(amount) as total FROM transactions WHERE to_kid_id = ? AND transaction_timestamp >= ? "
				+ "AND approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, toKidId, startTime);
		if (result.next()) {
			total = result.getDouble("total");
		}
		return total;
	}

	@Override
	public double getTotalOfTransactionsBetweenTimesByFromKidId(Timestamp startTime, Timestamp endTime, int fromKidId) {
		double total = 0.0;
		String sql = "SELECT SUM(amount) as total FROM transactions WHERE from_kid_id = ? AND transaction_timestamp >= ? "
				+ "AND transaction_timestamp <= ? AND approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, fromKidId, startTime, endTime);
		if (result.next()) {
			total = result.getDouble("total");
		}
		return total;
	}

	@Override
	public double getTotalOfTransactionsBetweenTimesByToKidId(Timestamp startTime, Timestamp endTime, int toKidId) {
		double total = 0.0;
		String sql = "SELECT SUM(amount) as total FROM transactions WHERE to_kid_id = ? AND transaction_timestamp >= ? "
				+ "AND transaction_timestamp <= ? AND approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, toKidId, startTime, endTime);
		if (result.next()) {
			total = result.getDouble("total");
		}
		return total;
	}

	@Override
	public void insertTransaction(Transaction transaction) {
		if (transaction.getTransactionNumber() == 0) {
			String sql = "INSERT INTO transactions (transaction_id, from_kid_id, to_kid_id, amount, transaction_type, approved, "
					+ "transaction_timestamp) VALUES (default, ?, ?, ?, ?, ?, ?)";
			jdbcTemplate.update(sql, transaction.getFromKid(), transaction.getToKid(), transaction.getAmount(), transaction.getTransactionType(), 
					transaction.isTransactionApproved(), transaction.getTransactionTime());
		} else {
			String sql = "INSERT INTO transactions (transaction_id, from_kid_id, to_kid_id, amount, transaction_type, approved, "
					+ "transaction_timestamp) VALUES (?, ?, ?, ?, ?, ?, ?)";
			jdbcTemplate.update(sql, transaction.getTransactionNumber(), transaction.getFromKid(), transaction.getToKid(), transaction.getAmount(), transaction.getTransactionType(), 
					transaction.isTransactionApproved(), transaction.getTransactionTime());
		}
	}

	@Override
	public void deleteTransactionByTransactionNumber(int transactionNumber) {
		String sql = "DELETE FROM transactions WHERE transaction_id = ?";
		jdbcTemplate.update(sql, transactionNumber);
	}

	@Override
	public List<Transaction> getAllUnapprovedTransactionsByFromKidId(int fromKidId) {
		List<Transaction> transactions = new ArrayList<Transaction>();
		String sql = "SELECT * FROM transactions WHERE from_kid_id = ? AND approved = false";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sql, fromKidId);
		while (results.next()) {
			Transaction transaction = mapToTransaction(results);
			transactions.add(transaction);
		}
		return transactions;
	}

	@Override
	public void approveTransaction(int transactionNumber) {
		String sql = "UPDATE transactions SET approved = true WHERE transaction_id = ?";
		jdbcTemplate.update(sql, transactionNumber);
	}
	
	@Override
	public int getNumberOfTransactionsFromBeginningByFromKidId(Timestamp startTime, Timestamp endTime, int fromKidId) {
		String sql = "SELECT count(*) as total_transactions FROM transactions WHERE from_kid_id = ? AND transaction_timestamp >= ? AND transaction_timestamp <= ?;";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, fromKidId, startTime, endTime);
		if (result.next()) {
			return result.getInt("total_transactions");
		}
		return -1;
	}
	
	private Transaction mapToTransaction(SqlRowSet result) {
		Transaction transaction = new Transaction();
		transaction.setTransactionNumber(result.getInt("transaction_id"));
		transaction.setFromKid(result.getInt("from_kid_id"));
		transaction.setToKid(result.getInt("to_kid_id"));
		transaction.setAmount(result.getDouble("amount"));
		transaction.setTransactionType(result.getString("transaction_type"));
		transaction.setTransactionApproved(result.getBoolean("approved"));
		transaction.setTransactionTime(result.getTimestamp("transaction_timestamp"));
		return transaction;
	}
}
