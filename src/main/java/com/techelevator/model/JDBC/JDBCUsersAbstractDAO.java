package com.techelevator.model.JDBC;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.DAO.UsersAbstractDAO;

public abstract class JDBCUsersAbstractDAO implements UsersAbstractDAO {
	
	protected JdbcTemplate  jdbcTemplate;

	@Override
	public int getNextUserID() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_user_id')");
		if (nextIdResult.next() ) {
			return nextIdResult.getInt(1);
		} else {
			throw new RuntimeException("Error returning next user_id value");
		}
	}

}
