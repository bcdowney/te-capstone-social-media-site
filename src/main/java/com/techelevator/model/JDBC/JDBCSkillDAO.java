package com.techelevator.model.JDBC;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.PendingProfileChange;
import com.techelevator.model.Skill;
import com.techelevator.model.DAO.SkillDAO;

@Component
public class JDBCSkillDAO implements SkillDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JDBCSkillDAO(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void addSkillBySkillNameKidId(String skillName, int kidId) {
		String sqlInsertSkillIntoKidsSkills = "INSERT INTO kids_skills VALUES (?, (SELECT skill_id FROM skills WHERE name = ?))";
		jdbcTemplate.update(sqlInsertSkillIntoKidsSkills, kidId, skillName);
	}
	
	@Override
	public void removeSkillBySkillNameKidId(int skillId, int kidId) {
		String sql = "DELETE FROM kids_skills WHERE kid_id = ? AND skill_id = ?";
		jdbcTemplate.update(sql, kidId, skillId);
	}

	@Override
	public List<Skill> getSkillsByKidId(int kidId) {
		List<Skill> skillsByKid = new ArrayList<Skill>();
		String sqlGetSkillsByKid = "SELECT s.* from skills s " + 
				"JOIN kids_skills ks ON s.skill_id = ks.skill_id " + 
				"WHERE ks.kid_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetSkillsByKid, kidId);
		while (result.next()) {
			skillsByKid.add(mapRowToSkill(result));
		}
		return skillsByKid;
	}
	
	@Override
	public List<Skill> getAllSkills() {
		List<Skill> skillsList = new ArrayList<Skill>();
		String sqlQueryAllSkills = "SELECT * from skills";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlQueryAllSkills);
		while (result.next()) {
			skillsList.add(mapRowToSkill(result));
		}
		return skillsList;
	}

	@Override
	public void saveNewSkill(String skillName) {
		String sqlSaveNewSkill = "INSERT INTO skills VALUES ((SELECT nextval('seq_skill_id')), ?, default)";
		jdbcTemplate.update(sqlSaveNewSkill, skillName);
	}
	
	@Override 
	public Skill getSkillBySkillId(int skillId) {
		Skill skill = new Skill();
		String sql = "SELECT * from skills WHERE skill_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, skillId);
		
		if (result.next()) {
			skill = mapRowToSkill(result);
		}
		
		return skill;
	}
	
	@Override
	public void updateSkillsWithApprovedSkillByParent(PendingProfileChange pendingProfileChange) {
	//	String columnName = pendingChange.getColumnName();
//		String updatedValue = pendingChange.getRequestedValue();
//		int kidId = pendingChange.getKidId();
		String sql = "INSERT INTO kids_skills VALUES (?, ?, ?);";
		String[] nameAndId = pendingProfileChange.getRequestedValue().split(",");
		jdbcTemplate.update(sql, pendingProfileChange.getKidId(), Integer.valueOf(nameAndId[1]), pendingProfileChange.getCreated());
	}
	
	@Override 
	public void removeSkillsWithApprovalByParen(PendingProfileChange pendingProfileChange) {
		String sql = "DELETE FROM kids_skills WHERE kid_id = ? AND skill_id = ?";
		String[] nameAndId = pendingProfileChange.getRequestedValue().split(",");
		jdbcTemplate.update(sql, pendingProfileChange.getKidId(), Integer.valueOf(nameAndId[1]));
	}
	
	private Skill mapRowToSkill(SqlRowSet result) {
		Skill skill = new Skill();
		skill.setSkillId(result.getInt("skill_id"));
		skill.setName(result.getString("name"));
		return skill;
	}

}
