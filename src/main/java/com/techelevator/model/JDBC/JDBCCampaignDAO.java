package com.techelevator.model.JDBC;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;
import com.techelevator.model.Campaign;
import com.techelevator.model.DAO.CampaignDAO;

@Component
public class JDBCCampaignDAO extends JDBCUsersAbstractDAO implements CampaignDAO {
	
	@Autowired
	public JDBCCampaignDAO(DataSource dataSource) {
		super.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Campaign getCampaignByCampaignId(int campaignId) {
		String sql = "SELECT * FROM campaigns " + 
					 "WHERE campaign_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, campaignId);
		result.next();
		return mapToRow(result);
	}
	
	@Override
	public List<Campaign> getAllApprovedCampaigns() {
		List<Campaign> searchResults = new ArrayList<Campaign>();
		String sql = "SELECT * FROM campaigns " + 
					 "WHERE approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		while (result.next()) {
			searchResults.add(mapToRow(result));
		}
		return searchResults;
	}

	@Override
	public Campaign getCampaignByKidId(int kidId) {
		Campaign campaign = new Campaign();
		String sql = "SELECT * FROM campaigns " + 
				 	 "WHERE kid_id = ? AND approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidId);
		if (result.next()) {
			campaign = mapToRow(result);
		}
		return campaign;
	}

	@Override
	public void insertCampaign(Campaign campaign) {
		String sql = "INSERT INTO campaigns (campaign_id, kid_id, title, description, current_amount, goal, approved, create_date) "+
					 "VALUES (default, ?, ?, ?, ?, ?, ?, default)";
		jdbcTemplate.update(sql, campaign.getKidId(), campaign.getTitle(), campaign.getDescription(), campaign.getCurrent(), campaign.getGoal(), campaign.isApproved());
	}

	@Override
	public void deleteCampaignByCampaignId(int campaignId) {
		String sql = "DELETE FROM campaigns WHERE campaign_id = ?";
		jdbcTemplate.update(sql, campaignId);
	}

	@Override
	public List<Campaign> getAllCampaigns() {
		List<Campaign> allCampaigns = new ArrayList<Campaign>();
		String sql = "SELECT * FROM campaigns";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		while(result.next()) {
			Campaign campaign = mapToRow(result);
			allCampaigns.add(campaign);
		}
		return allCampaigns;
	}

	@Override
	public Campaign getUnapprovedCampaignByKidId(int kidId) {
		Campaign campaign = new Campaign();
		String sql = "SELECT * FROM campaigns "+
					 "WHERE kid_id = ? AND approved = false";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidId);
		if(result.next()) {
			campaign = mapToRow(result);
		}
		return campaign;
	}

	@Override
	public void approveCampaign(int campaignId) {
		String sql = "UPDATE campaigns SET approved = true " +
					 "WHERE campaign_id = ?";
		jdbcTemplate.update(sql, campaignId);
	}
	
	private Campaign mapToRow(SqlRowSet result) {
		Campaign c = new Campaign();
		c.setCampaignNumber(result.getInt("campaign_id"));
		c.setKidId(result.getInt("kid_id"));
		c.setTitle(result.getString("title"));
		c.setDescription(result.getString("description"));
		c.setCurrent(result.getDouble("current_amount"));
		c.setGoal(result.getDouble("goal"));
		c.setApproved(result.getBoolean("approved"));
		c.setCreateDate(result.getTimestamp("create_date"));
		return c;
	}

	@Override
	public void contributeToCampaign(int campaignId, double money) {
		Campaign c = new Campaign();
		String sql = "SELECT * FROM campaigns "+
					 "WHERE kid_id = ? AND approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, campaignId);
		while(result.next()) {
			c = mapToRow(result);
		}
		double newAmt = (c.getCurrent()) + money;
		String sql2 = "UPDATE campaigns SET current_amount = ? "+
					  "WHERE campaign_id = ?";
		jdbcTemplate.update(sql2, newAmt, campaignId);
	}

}
