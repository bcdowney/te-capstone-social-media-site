package com.techelevator.model.JDBC;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.DAO.GuardianDAO;
import com.techelevator.model.DAO.UsersAbstractDAO;

@Component
public class JDBCGuardianDAO extends JDBCUsersAbstractDAO implements GuardianDAO, UsersAbstractDAO {

	@Autowired
	public JDBCGuardianDAO (DataSource dataSource) {
		super.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Kid> getAllKids(int guardianId) {
		List<Kid> children = new ArrayList<Kid>();
		String sql = "SELECT k.* FROM kid_users k " + 
					 "JOIN guardians_kids pk ON k.kid_id = pk.kid_id " + 
					 "WHERE pk.guardian_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, guardianId);
		while(result.next()) {
			Kid kid = mapToRow(result);
			children.add(kid);
		}
		return children;
	}
	
	@Override
	public Guardian getGuardianByUserId(int userId) {
		Guardian guardian = new Guardian();
		String sql = "SELECT user_id, guardian_id, first_name, "
				+ "last_name, date_of_birth, email FROM guardian_users WHERE user_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, userId);
		if (result.next()) {
			guardian = mapToGuardian(result);
		}
		return guardian;
	}
	
	@Override
	public Guardian getGuardianByGuardianId(int guardianId ) {
		Guardian guardian = new Guardian();
		String sql = "SELECT user_id, guardian_id, first_name, "
				+ "last_name, date_of_birth, email FROM guardian_users WHERE guardian_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, guardianId);
		if (result.next()) {
			guardian = mapToGuardian(result);
		}
		
		return guardian;
	}
	
	@Override
	public void insertNewGuardian(Guardian guardian) {
		String sql = "INSERT INTO guardian_users (user_id, first_name, last_name, date_of_birth, email) "+
					 "VALUES (?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, guardian.getUserId(), guardian.getFirstName(), guardian.getLastName(), guardian.getDateOfBirth(), guardian.getEmail());
	}
	
	@Override
	public void insertGuardianAndKid(int kidId, int guardianId) {
		String sql = "INSERT INTO guardians_kids VALUES (?, ?)";
		jdbcTemplate.update(sql, kidId, guardianId);
	}
	
	@Override
	public int getGuardianIdByUserId(int userId) {
		String sql = "SELECT guardian_id FROM guardian_users "+
					 "WHERE user_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, userId);
		result.next();
		return result.getInt("guardian_id");
	}
	
	@Override
	public Guardian getGuardianByEmail(String email) {
		String sql = "SELECT * FROM guardian_users "+
					 "WHERE email = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, email);
		result.next();
		Guardian guardian = mapToGuardian(result);
		return guardian;
	}
	
	@Override
	public void updateGuardianByGuardianId(int guardianId, String column, String updateValue) {
		String sql = "UPDATE guardian_users SET " + column + " = ? WHERE guardian_id = ?;";
		jdbcTemplate.update(sql);
		
	}
	
	private Kid mapToRow(SqlRowSet result) {
		Kid kid = new Kid();
		kid.setUserId(result.getInt("user_id"));
		kid.setKidId(result.getInt("kid_id"));
		kid.setGuardianId(result.getInt("guardian_id"));
		kid.setFirstName(result.getString("first_name"));
		kid.setLastName(result.getString("last_name"));
		kid.setTransactionAmountLimit(result.getDouble("transaction_amount_limit"));
		kid.setTransactionsPerDay(result.getInt("transactions_per_day_limit"));
		kid.setDateOfBirth(result.getDate("date_of_birth"));
		kid.setSummary(result.getString("summary"));
		kid.setEmail(result.getString("email"));
		return kid;
	}
	
	private Guardian mapToGuardian(SqlRowSet result) {
		Guardian guardian = new Guardian();
		guardian.setUserId(result.getInt("user_id"));
		guardian.setGuardianID(result.getInt("guardian_id"));
		guardian.setFirstName(result.getString("first_name"));
		guardian.setLastName(result.getString("last_name"));
		guardian.setDateOfBirth(result.getDate("date_of_birth"));
		guardian.setEmail(result.getString("email"));
		return guardian;
	}
	
}
