package com.techelevator.model.JDBC;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.Admin;
import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.User;
import com.techelevator.model.DAO.AdminDAO;
import com.techelevator.model.DAO.UsersAbstractDAO;
import com.techelevator.security.PasswordHasher;

@Component
public class JDBCAdminDAO extends JDBCUsersAbstractDAO implements AdminDAO, UsersAbstractDAO{
	
	public JDBCAdminDAO(DataSource dataSource) {
		super.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Admin getAdminByUserId(int userId) {
		Admin admin = new Admin();
		String sql = "SELECT user_id, email FROM admin_users WHERE user_id = ?";
		
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, userId);
		if (result.next()) {
			admin = mapToAdmin(result);
		}
		
		return admin;
	}


	@Override
	public List<Admin> getAllAdmins() {
		List<Admin> adminList = new ArrayList<Admin>();
		String sql = "SELECT user_id, email FROM admin_users";
		
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		
		while(result.next()) {
			adminList.add(mapToAdmin(result));
		}
		return adminList;
	}

	@Override
	public List<Kid> getKidsByGuardianId(int guardianId) {
		List<Kid> listOfKids = new ArrayList<>();
		String sql = "SELECT user_id, kid_id, guardian_id, first_name, "
				+ "last_name, date_of_birth, summary, email FROM kid_users";
		
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		
		while (result.next()) {
			listOfKids.add(mapToKid(result));
		}
		
		return listOfKids;
	}

	@Override
	public List<User> getAllSuspendedUsersByUsername(String username){
		List<User> results = new ArrayList<User>();
		String sql = "SELECT au.* FROM app_user au " + 
					"JOIN disabled_users du ON au.id = du.user_id " +
					"WHERE UPPER(user_name) LIKE CONCAT('%', ?,'%')";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, username.toUpperCase());
		while (result.next()) {
			results.add(mapRowToUser(result));
		}
		return results;
		}
	
	@Override
	public void disableUser(User user) {
		PasswordHasher hash = new PasswordHasher();
		String sql = "INSERT INTO disabled_users VALUES (?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, user.getUserId(), user.getUserName(), user.getRole(), hash.generateRandomSalt(), user.getDateCreated(), user.getLastModified());
		String sqlRemove = "DELETE FROM app_user WHERE id = ?";
		jdbcTemplate.update(sqlRemove, user.getUserId());
	}

	@Override
	public Guardian getGuardianByGuardianId(int guardianId) {
		Guardian guardian = new Guardian();
		String sql = "SELECT user_id, guardian_id, first_name, last_name, date_of_birth, email FROM guardian_users WHERE guardian_id = ?";
		
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, guardianId);
		
		if (result.next()) {
			guardian = mapToGuardian(result);
		}
		
		return guardian;
	}

	@Override
	public Guardian getGuardianByKidId(int kidId) {
		Guardian guardian = new Guardian();
		String sql = "SELECT g.user_id, g.guardian_id, g.first_name, g.last_name, g.date_of_birth, g.email "
				+ "FROM guardian_users g JOIN guardians_kids gk ON (g.guardian_id = gk.guardian_id) JOIN "
				+ "kid_users k ON (gk.kid_id = k.kid_id) WHERE k.kid_id = ? ";
		
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidId);
		
		if (result.next()) {
			guardian = mapToGuardian(result);
		}
		
		return guardian;
	}

	@Override
	public List<Guardian> getAllGuardians() {
		List<Guardian> listOfGuardians = new ArrayList<>();
		String sql = "SELECT user_id, guardian_id, first_name, last_name, date_of_birth, email FROM guardian_users;";
		
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		
		while (result.next()) { 
			listOfGuardians.add(mapToGuardian(result));
		}
		
		return listOfGuardians;
	}
	
	@Override
	public void saveAdmin(Admin newAdmin) {
		String sql = "INSERT INTO admin_users VALUES (?, ?, default, default)";
		jdbcTemplate.update(sql, newAdmin.getUserId(), newAdmin.getEmail());
	}
	
	private Kid mapToKid(SqlRowSet result) {
		Kid kid = new Kid();
		kid.setUserId(result.getInt("user_id"));
		kid.setKidId(result.getInt("kid_id"));
		kid.setGuardianId(result.getInt("guardian_id"));
		kid.setFirstName(result.getString("first_name"));
		kid.setLastName(result.getString("last_name"));
		kid.setDateOfBirth(result.getDate("date_of_birth"));
		kid.setSummary(result.getString("summary"));
		kid.setEmail(result.getString("email"));
		return kid;
	}
	
	private Guardian mapToGuardian(SqlRowSet result) {
		Guardian guardian = new Guardian();
		guardian.setUserId(result.getInt("user_id"));
		guardian.setGuardianID(result.getInt("guardian_id"));
		guardian.setFirstName(result.getString("first_name"));
		guardian.setLastName(result.getString("last_name"));
		guardian.setDateOfBirth(result.getDate("date_of_birth"));
		guardian.setEmail(result.getString("email"));
		return guardian;
	}
	
	private Admin mapToAdmin(SqlRowSet result) {
		Admin admin = new Admin();
		admin.setUserId(result.getInt("user_id"));
		admin.setEmail(result.getString("email"));
		
		return admin;
	}
	
	private User mapRowToUser(SqlRowSet result) {
		User user = new User();
		user.setUserId(result.getInt("id"));
		user.setUserName(result.getString("user_name"));
		user.setPassword(result.getString("password"));
		user.setConfirmPassword(result.getString("password"));
		user.setRole(result.getString("role"));
		user.setDateCreated(result.getTimestamp("create_date"));
		user.setLastModified(result.getTimestamp("last_modified"));
		return user;
	}

	@Override
	public Guardian getGuardianByUserId(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

}
