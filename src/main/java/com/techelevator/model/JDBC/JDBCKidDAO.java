package com.techelevator.model.JDBC;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.PendingProfileChange;
import com.techelevator.model.DAO.KidDAO;
import com.techelevator.model.DAO.UsersAbstractDAO;

@Component
public class JDBCKidDAO extends JDBCUsersAbstractDAO implements KidDAO, UsersAbstractDAO{
	
	private static Map<String, String> SQL_COMMANDS = new HashMap<String, String>() {
		{
			put("firstName", "first_name");
			put("businessName", "business_name");
			put("lastName", "last_name");
			put("dateOfBirth", "date_of_birth");
			put("transactionsPerDay", "transactions_per_day_limit");
			put("transactionAmountLimit", "transaction_amount_limit");
			put("logoFileName", "logo_file_name");
			put("summary", "summary");
			put("email", "email");
			put("skills", "skills");
		}
	};
	
	public JDBCKidDAO(DataSource dataSource) {
		super.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Kid getKidByUserId(long userId) {
		Kid kid = new Kid();
		String sql = "SELECT user_id, kid_id, guardian_id, first_name, logo_file_name, "
				+ "last_name, business_name, date_of_birth, summary, email FROM kid_users WHERE user_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, userId);
		if (result.next()) {
			kid = mapToKid(result);
		}
		return kid;
	}

	@Override
	public Kid getKidByKidId(long kidId) {
		Kid kid = new Kid();
		String sql = "SELECT user_id, kid_id, guardian_id, first_name, logo_file_name, "
				+ "last_name, business_name, date_of_birth, summary, email FROM kid_users WHERE kid_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidId);
		if (result.next()) {
			kid = mapToKid(result);
		}
		return kid;
	}
	
	@Override
	public void updateKidWithApprovedChange(PendingProfileChange pendingProfileChange) {
		String columnName = SQL_COMMANDS.get(pendingProfileChange.getColumnName());
		String updatedValue = pendingProfileChange.getRequestedValue();
		int kidId = pendingProfileChange.getKidId();
		String sql = "UPDATE kid_users SET " + columnName + " = ? WHERE kid_id = ?";
		jdbcTemplate.update(sql, updatedValue, kidId);
	}
	
	// can the below method be deleted?? -mc 12/13/18
	@Override
	public void registerNewKid(Kid kid, Guardian guardian) {
		String sql = "INSERT INTO kid_users (user_id, kid_id, guardian_id, first_name, last_name, business_name, "
				+ "date_of_birth, email) VALUES (?, default, ?, ?, ?, ?, ?, '123@example.com')";
		
		//eventually change the one to correspond with correct app user id, it is only one right now for proving we can 
		//	register a new kid
		jdbcTemplate.update(sql, 1, guardian.getGuardianID(), kid.getFirstName(),
				kid.getLastName(), kid.getBusinessName(), kid.getDateOfBirth());
	}
	
	@Override
	public void insertNewTemporaryKid(Kid kid) {
		String sql = "INSERT INTO kid_users (user_id, guardian_id, first_name, last_name, date_of_birth, business_name, logo_file_name, summary, email) " + 
					 "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";	
		jdbcTemplate.update(sql, kid.getUserId(), kid.getGuardianId(), kid.getFirstName(), kid.getLastName(), kid.getDateOfBirth(), kid.getBusinessName(), kid.getLogoFileName(), kid.getSummary(), kid.getEmail());	
	}
	
	@Override
	public int getKidIdByKidUserId(int kidUserId) {
		String sql = "SELECT kid_id FROM kid_users "+
					 "WHERE user_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidUserId);
		result.next();
		return result.getInt("kid_id");
	}
	
	@Override
	public int getTransactionsPerDayLimitByKidId(int kidId) {
		String sql = "SELECT transactions_per_day_limit FROM kid_users WHERE kid_id = ? AND NOT NULL";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidId);
		if (result.next()) {
			return result.getInt("transactions_per_day_limit");
		}
		return -1;
	}

	@Override
	public double getTransactionAmountLimitByKidId(int kidId) {
		String sql = "SELECT transaction_amount_limit FROM kid_users WHERE kid_id = ? AND NOT NULL";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidId);
		if (result.next()) {
			return result.getInt("transaction_amount_limit");
		}
		return -1;
	}
	
	@Override
	public void updateTransactionsPerDayLimitByKidId(int kidId, int transactionsLimit) {
		String sql = "UPDATE kid_users SET transactions_per_day_limit = ? WHERE kid_id = ?";
		jdbcTemplate.update(sql, transactionsLimit, kidId);
	}

	@Override
	public void updateTransactionAmountLimitByKidId(int kidId, double amountLimit) {
		String sql = "UPDATE kid_users SET transaction_amount_limit = ? WHERE kid_id = ?";
		jdbcTemplate.update(sql, amountLimit, kidId);
	}
	
	private Kid mapToKid(SqlRowSet result) {
		Kid kid = new Kid();
		kid.setUserId(result.getInt("user_id"));
		kid.setKidId(result.getInt("kid_id"));
		kid.setGuardianId(result.getInt("guardian_id"));
		kid.setFirstName(result.getString("first_name"));
		kid.setLastName(result.getString("last_name"));
		kid.setLogoFileName(result.getString("logo_file_name"));
		kid.setBusinessName(result.getString("business_name"));
		kid.setDateOfBirth(result.getDate("date_of_birth"));
		kid.setSummary(result.getString("summary"));
		kid.setEmail(result.getString("email"));
		return kid;
	}


}
