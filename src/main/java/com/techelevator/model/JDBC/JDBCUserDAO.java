package com.techelevator.model.JDBC;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.User;
import com.techelevator.model.DAO.UserDAO;
import com.techelevator.security.PasswordHasher;

@Component
public class JDBCUserDAO implements UserDAO {

	private JdbcTemplate jdbcTemplate;
	private PasswordHasher hashMaster;

	@Autowired
	public JDBCUserDAO(DataSource dataSource, PasswordHasher hashMaster) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.hashMaster = hashMaster;
	}
	
	@Override
	public void saveUser(String userName, String password) {
		byte[] salt = hashMaster.generateRandomSalt();
		String hashedPassword = hashMaster.computeHash(password, salt);
		String saltString = new String(Base64.encode(salt));
		
		jdbcTemplate.update("INSERT INTO app_user(user_name, password, salt) VALUES (?, ?, ?)",
				userName, hashedPassword, saltString);
	}

	@Override
	public boolean searchForUsernameAndPassword(String userName, String password) {
		String sqlSearchForUser = "SELECT * "+
							      "FROM app_user "+
							      "WHERE UPPER(user_name) = ? ";
		
		SqlRowSet user = jdbcTemplate.queryForRowSet(sqlSearchForUser, userName.toUpperCase());
		if(user.next()) {
			String dbSalt = user.getString("salt");
			String dbHashedPassword = user.getString("password");
			String givenPassword = hashMaster.computeHash(password, Base64.decode(dbSalt));
			return dbHashedPassword.equals(givenPassword);
		} else {
			return false;
		}
	}

	@Override
	public void updatePassword(String userName, String password) {
		jdbcTemplate.update("UPDATE app_user SET password = ? WHERE user_name = ?", password, userName);
	}

	@Override
	public Object getUserByUserName(String userName) {
		String sqlSearchForUsername ="SELECT * "+
										"FROM app_user "+
										"WHERE UPPER(user_name) = ? ";
		SqlRowSet user = jdbcTemplate.queryForRowSet(sqlSearchForUsername, userName.toUpperCase()); 
		User thisUser = null;
		if(user.next()) {
			thisUser = mapRowToUser(user);
		}

		return thisUser;
	}

	@Override
	public void insertUser(User user) {
		byte[] salt = hashMaster.generateRandomSalt();
		String hashedPassword = hashMaster.computeHash(user.getPassword(), salt);
		String saltString = new String(Base64.encode(salt));
		String sql = "INSERT INTO app_user(user_name, password, role, salt) VALUES (?, ?, ?, ?)";
		jdbcTemplate.update(sql, user.getUserName(), hashedPassword, user.getRole(), saltString);
	}

	@Override
	public int getUserIdByUsername(String userName) {
		String sql = "SELECT id FROM app_user " + 
					 	"WHERE user_name = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, userName);
		result.next();
		return result.getInt("id");
	}

	/*
	 * For admin dashboard. Checks to see if string is valid user. If it's not, it returns the total number
	 * of users. If it is, it returns the total number of that particular user role.
	 */
	@Override
	public int getTotalUsers(String userRole) {
		SqlRowSet totalUsers = null;
		int result = 0;
		if(userRole.equals("admin") 
				|| userRole.equals("guardian") 
				|| userRole.equals("child") 
				|| userRole.equals("mentor")) {
			String sql = "SELECT COUNT(*) FROM app_user "
						+ "WHERE role = ?";
			totalUsers = jdbcTemplate.queryForRowSet(sql, userRole);
			totalUsers.next();
			result = totalUsers.getInt("count");	
		} else {
			String sql = "SELECT COUNT(*) FROM app_user";
			totalUsers = jdbcTemplate.queryForRowSet(sql);
			totalUsers.next();
			result = totalUsers.getInt("count");
		}
		return result;
	}
	
	@Override
	public String getUserNameById(int userId) {
		String sql = "SELECT user_name FROM app_user " + 
				 		"WHERE id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, userId);
		result.next();
		return result.getString("user_name");
	}
	
	@Override
	public List<User> searchUserByUserName(String searchString, String role){
		List<User> searchResults = new ArrayList<User>();
		String sql = "SELECT * FROM app_user " + 
						"WHERE UPPER(user_name) LIKE CONCAT('%', ?,'%') " +
						"AND role = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, searchString.toUpperCase(), role);
		while (result.next()) {
			searchResults.add(mapRowToUser(result));
		}
		return searchResults;
	}
	
	private User mapRowToUser(SqlRowSet result) {
		User user = new User();
		user.setUserId(result.getInt("id"));
		user.setUserName(result.getString("user_name"));
		user.setPassword(result.getString("password"));
		user.setConfirmPassword(result.getString("password"));
		user.setRole(result.getString("role"));
		user.setDateCreated(result.getTimestamp("create_date"));
		user.setLastModified(result.getTimestamp("last_modified"));
		return user;
	}

}
