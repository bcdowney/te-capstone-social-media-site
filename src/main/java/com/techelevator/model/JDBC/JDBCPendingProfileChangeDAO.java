package com.techelevator.model.JDBC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.Guardian;
import com.techelevator.model.Kid;
import com.techelevator.model.PendingProfileChange;
import com.techelevator.model.DAO.PendingProfileChangeDAO;
import com.techelevator.model.DAO.UsersAbstractDAO;

@Component
public class JDBCPendingProfileChangeDAO extends JDBCUsersAbstractDAO implements PendingProfileChangeDAO, UsersAbstractDAO {
	
	private static Map<String, String> KID_OBJECT_ATTRIBUTES = new HashMap<String, String>() {
		{
			put("first_name", "firstName");
			put("last_name", "lastName");
			put("date_of_birth", "dateOfBirth");
			put("business_name", "businessName");
			put("logo_file_name", "logoFileName");
			put("summary", "summary");
			put("email", "email");
			put("skills", "skills");
		}
	};
	
	private static Map<String, String> SQL_COMMANDS = new HashMap<String, String>() {
		{
			put("firstName", "first_name");
			put("lastName", "last_name");
			put("dateOfBirth", "date_of_birth");
			put("businessName", "business_name");
			put("logoFileName", "logo_file_name");
			put("summary", "summary");
			put("email", "email");
			put("skills", "skills");
		}
	};

	@Autowired
	public JDBCPendingProfileChangeDAO (DataSource dataSource) {
		super.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<PendingProfileChange> getPendingChangesByGuardian(Guardian guardian) {
		List<PendingProfileChange> pendingProfileChanges = new ArrayList<PendingProfileChange>();
		
		String sql = "SELECT * FROM kid_users_pending_changes " + 
					 "WHERE guardian_id = ?";
		
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, guardian.getGuardianID());
		
		while(result.next()) {
			PendingProfileChange pendingProfileChange = mapToRowSet(result);
			pendingProfileChanges.add(pendingProfileChange);
		}
		
		return pendingProfileChanges;
	}

	@Override
	public List<PendingProfileChange> getPendingChangesByKid(Kid kid) {
		List<PendingProfileChange> pendingProfileChanges = new ArrayList<PendingProfileChange>();
		String sql = "SELECT * FROM kid_users_pending_changes " + 
					 "WHERE kid_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kid.getKidId());
		while(result.next()) {
			PendingProfileChange pendingProfileChange = mapToRowSet(result);
			pendingProfileChanges.add(pendingProfileChange);
		}
		return pendingProfileChanges;
	}

	@Override
	public void deletePendingChange(int pendingChangeId) {
		String sql = "DELETE FROM kid_users_pending_changes "+
					 "WHERE pending_request_id = ?";
		jdbcTemplate.update(sql, pendingChangeId);
	}

	@Override
	public void saveToHistoricalChanges(PendingProfileChange change) {
		String sql = "INSERT INTO historical_changes VALUES(default, ?, ?, ?, ?, ?, ?, default)";
		jdbcTemplate.update(sql, change.getUserId(), change.getUserId(), SQL_COMMANDS.get(change.getColumnName()), change.getCurrentValue(), change.getRequestedValue(), change.getCreated());
		deletePendingChange(change.getPendingRequestId());
	}
	
	@Override
	public void saveToPendingChanges(List<PendingProfileChange> pendingProfileChanges) {
		for(PendingProfileChange change : pendingProfileChanges){
			@SuppressWarnings("unused")
			String column = change.getColumnName();
			String sql = "INSERT INTO kid_users_pending_changes "+
						 "VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, DEFAULT)";
			jdbcTemplate.update(sql, change.getUserId(), change.getKidId(), change.getGuardianId(), SQL_COMMANDS.get(change.getColumnName()), change.getCurrentValue(), change.getRequestedValue(), change.isApproved());
		}
	}
	
	@Override
	public List<PendingProfileChange> getAllPendingChanges() {
		List<PendingProfileChange> pendingProfileChanges = new ArrayList<PendingProfileChange>();
//		String sql = "SELECT * FROM kid_users_pending_changes";
		String sql = "SELECT pc.*, au.user_name AS guardian_user_name, au2.user_name AS kid_user_name FROM kid_users_pending_changes pc " + 
					"JOIN guardian_users gu ON pc.guardian_id = gu.guardian_id " + 
					"JOIN app_user au ON gu.user_id = au.id " + 
					"JOIN app_user au2 ON pc.user_id = au2.id;";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		while(result.next()) {
			PendingProfileChange pendingProfileChange = mapToRowSetWithUserNames(result);
			pendingProfileChanges.add(pendingProfileChange);
		}
		return pendingProfileChanges;
	}

	private PendingProfileChange mapToRowSet(SqlRowSet result) {
		PendingProfileChange pendingProfileChange = new PendingProfileChange();
		pendingProfileChange.setPendingRequestId(result.getInt("pending_request_id"));
		pendingProfileChange.setUserId(result.getInt("user_id"));
		pendingProfileChange.setKidId(result.getInt("kid_id"));
		pendingProfileChange.setGuardianId(result.getInt("guardian_id"));
		pendingProfileChange.setColumnName(KID_OBJECT_ATTRIBUTES.get(result.getString("changed_column")));
		pendingProfileChange.setCurrentValue(result.getString("current_value"));
		pendingProfileChange.setRequestedValue(result.getString("requested_value"));
		pendingProfileChange.setApproved(result.getBoolean("approved"));
		pendingProfileChange.setCreated(result.getTimestamp("created"));
		return pendingProfileChange;
	}
	
	private PendingProfileChange mapToRowSetWithUserNames(SqlRowSet result) {
		PendingProfileChange pendingChange = new PendingProfileChange();
		pendingChange.setPendingRequestId(result.getInt("pending_request_id"));
		pendingChange.setUserId(result.getInt("user_id"));
		pendingChange.setKidId(result.getInt("kid_id"));
		pendingChange.setGuardianId(result.getInt("guardian_id"));
		pendingChange.setColumnName(KID_OBJECT_ATTRIBUTES.get(result.getString("changed_column")));
		pendingChange.setCurrentValue(result.getString("current_value"));
		pendingChange.setRequestedValue(result.getString("requested_value"));
		pendingChange.setApproved(result.getBoolean("approved"));
		pendingChange.setCreated(result.getTimestamp("created"));
		pendingChange.setKidUserName(result.getString("kid_user_name"));
		pendingChange.setGuardianUserName(result.getString("guardian_user_name"));
		return pendingChange;
	}

}
