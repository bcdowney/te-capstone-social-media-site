package com.techelevator.model.JDBC;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.Ad;
import com.techelevator.model.DAO.AdDAO;

@Component
public class JDBCAdDAO extends JDBCUsersAbstractDAO implements AdDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	public JDBCAdDAO (DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Ad> getAllAds() {
		List<Ad> adsList = new ArrayList<Ad>();
		String sql = "SELECT a.ad_id, a.kid_id, a.ad_type, a.title, a.description, a.start_date, a.start_time, a.end_date, a.end_time, a.created, a.pay_dollars, a.barter_desc, a.approved, k.business_name, k.logo_file_name FROM ads a " +
					 "JOIN kid_users k ON a.kid_id = k.kid_id " +
					 "WHERE a.approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		while(result.next()) {
			Ad ad = mapToRow(result);
			adsList.add(ad);
		}
		return adsList;
	}

	@Override
	public Ad getAdByAdId(int adId) {
		String sql = "SELECT a.ad_id, a.kid_id, a.ad_type, a.title, a.description, a.start_date, a.start_time, a.end_date, a.end_time, a.created, a.pay_dollars, a.barter_desc, a.approved, k.business_name, k.logo_file_name FROM ads a " +
					 "JOIN kid_users k ON a.kid_id = k.kid_id " +
					 "WHERE a.ad_id = ?";
		SqlRowSet result  = jdbcTemplate.queryForRowSet(sql, adId);
		result.next();
		Ad ad = mapToRow(result);
		return ad;
	}

	@Override
	public boolean isAdApproved(int adId) {
		String sql = "SELECT approved FROM ads " +
					 "WHERE ad_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, adId);
		result.next();
		if (result.getBoolean("approved")) {
			return true;
		}
		return false;
	}
	
	@Override
	public List<Ad> getAllJobs() {
		List<Ad> jobList = new ArrayList<Ad>();
		String sql = "SELECT a.ad_id, a.kid_id, a.ad_type, a.title, a.description, a.start_date, a.start_time, a.end_date, a.end_time, a.created, a.pay_dollars, a.barter_desc, a.approved, k.business_name, k.logo_file_name FROM ads a " +
					 "JOIN kid_users k ON a.kid_id = k.kid_id " +
					 "WHERE a.ad_type = 'job'";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		while(result.next()) {
			Ad ad = mapToRow(result);
			jobList.add(ad);
		}
		return jobList;
	}

	@Override
	public List<Ad> getAllSkills() {
		List<Ad> skillList = new ArrayList<Ad>();
		String sql = "SELECT a.ad_id, a.kid_id, a.ad_type, a.title, a.description, a.start_date, a.start_time, a.end_date, a.end_time, a.created, a.pay_dollars, a.barter_desc, a.approved, k.business_name, k.logo_file_name FROM ads a " +
					 "JOIN kid_users k ON a.kid_id = k.kid_id " +
					 "WHERE a.ad_type = 'skill'";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		while(result.next()) {
			Ad ad = mapToRow(result);
			skillList.add(ad);
		}
		return skillList;
	}
	
	@Override
	public void insertAd(Ad ad) {
		String sql = "INSERT INTO ads (ad_id, kid_id, ad_type, title, description, start_date, start_time, end_date, end_time, created, pay_dollars, barter_desc, approved) " +
				 	 "VALUES (default, ?, ?, ?, ?, ?, ?, ?, ?, default, ?, ?, ?)";
		jdbcTemplate.update(sql, ad.getKidId(), ad.getAdType(), ad.getTitle(), ad.getDescription(), ad.getStartDate(), ad.getStartTime(), ad.getEndDate(), ad.getEndTime(), ad.getPayDollars(), ad.getBarterDescription(), ad.isApproved());
	}
	
	@Override
	public void approveAd(int adId) {
		String sql = "UPDATE ads SET approved = ? " +
					 "WHERE ad_id = ?";
		jdbcTemplate.update(sql, true, adId);
	}
	
	@Override
	public void deleteAdByAdId(int adId) {
		String sql = "DELETE FROM ads WHERE ad_id = ?";
		jdbcTemplate.update(sql);
	}
	
	@Override
	public List<Ad> getApprovedAdsbyKidId(int kidId) {
		List<Ad> approvedAds = new ArrayList<Ad>();
		String sql = "SELECT a.ad_id, a.kid_id, a.ad_type, a.title, a.description, a.start_date, a.start_time, a.end_date, a.end_time, a.created, a.pay_dollars, a.barter_desc, a.approved, k.business_name, k.logo_file_name FROM ads a " +
				 	 "JOIN kid_users k ON a.kid_id = k.kid_id " +
					 "WHERE a.kid_id = ? AND a.approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidId);
		while(result.next()) {
			Ad ad = mapToRow(result);
			approvedAds.add(ad);
		}
		return approvedAds;
	}
	
	@Override
	public List<Ad> getUnapprovedAdsbyKidId(int kidId) {
		List<Ad> unapprovedAds = new ArrayList<Ad>();
		String sql = "SELECT a.ad_id, a.kid_id, a.ad_type, a.title, a.description, a.start_date, a.start_time, a.end_date, a.end_time, a.created, a.pay_dollars, a.barter_desc, a.approved, k.business_name, k.logo_file_name FROM ads a " +
					 "JOIN kid_users k ON a.kid_id = k.kid_id " +
					 "WHERE a.kid_id = ? AND a.approved = false";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidId);
		while(result.next()) {
			Ad ad = mapToRow(result);
			unapprovedAds.add(ad);
		}
		return unapprovedAds;
	}
	
	private Ad mapToRow(SqlRowSet result) {
		Ad ad = new Ad();
		ad.setAdId(result.getInt("ad_id"));
		ad.setKidId(result.getInt("kid_id"));
		ad.setAdType(result.getString("ad_type"));
		ad.setTitle(result.getString("title"));
		ad.setDescription(result.getString("description"));
		ad.setStartDate(result.getDate("start_date"));
		ad.setEndDate(result.getDate("end_date"));
		ad.setStartTime(result.getTime("start_time"));
		ad.setEndTime(result.getTime("end_time"));
		ad.setCreated(result.getTimestamp("created"));
		ad.setPayDollars(result.getDouble("pay_dollars"));
		ad.setBarterDescription(result.getString("barter_desc"));
		ad.setApproved(result.getBoolean("approved"));
		ad.setBusinessName(result.getString("business_name"));
		ad.setLogoFileName(result.getString("logo_file_name"));
		return ad;
	}

}
