package com.techelevator.model.JDBC;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.HistoricalChange;
import com.techelevator.model.DAO.HistoricalChangeDAO;

@Component
public class JDBCHistoricalChangeDAO extends JDBCUsersAbstractDAO implements HistoricalChangeDAO {
	
	@Autowired
	public JDBCHistoricalChangeDAO (DataSource dataSource) {
		super.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<HistoricalChange> getAllHistoricalChanges() {
		List<HistoricalChange> allHistoricalChanges = new ArrayList<HistoricalChange>();
//		String sql = "SELECT * FROM historical_changes";
		String sql = "SELECT hc.*, au.user_name AS change_initiated_by_user_name, au2.user_name AS change_to_user_name, au.role AS change_initiated_by_role FROM historical_changes hc " + 
				"JOIN app_user au ON hc.change_initiated_by_id = au.id " + 
				"JOIN app_user au2 ON hc.change_to_user_id = au2.id;";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		while(result.next()) {
			HistoricalChange hc = mapToRowWithUserNames(result);
			allHistoricalChanges.add(hc);
		}
		return allHistoricalChanges;
	}

	@Override
	public HistoricalChange getHistoricalChangeById(int id) {
		String sql = "SELECT * FROM historical_changes "+
					 "WHERE id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, id);
		result.next();
		HistoricalChange hc = mapToRow(result);
		return hc;
	}
	
	private HistoricalChange mapToRow(SqlRowSet result) {
		HistoricalChange hc = new HistoricalChange();
		hc.setHistoricalChangeId(result.getInt("id"));
		hc.setChangeInitiatedById(result.getInt("change_initiated_by_id"));
		hc.setChangeToUserId(result.getInt("change_to_user_id"));
		hc.setChangedColumn(result.getString("changed_column"));
		hc.setOldValue(result.getString("old_val"));
		hc.setNewValue(result.getString("new_val"));
		hc.setRequestTime(result.getTimestamp("request_time"));
		hc.setCompleteTime(result.getTimestamp("complete_time"));
		return hc;
	}
	
	private HistoricalChange mapToRowWithUserNames(SqlRowSet result) {
		HistoricalChange hc = new HistoricalChange();
		hc.setHistoricalChangeId(result.getInt("id"));
		hc.setChangeInitiatedById(result.getInt("change_initiated_by_id"));
		hc.setChangeToUserId(result.getInt("change_to_user_id"));
		hc.setChangedColumn(result.getString("changed_column"));
		hc.setOldValue(result.getString("old_val"));
		hc.setNewValue(result.getString("new_val"));
		hc.setRequestTime(result.getTimestamp("request_time"));
		hc.setCompleteTime(result.getTimestamp("complete_time"));
		hc.setChangeByUserName(result.getString("change_initiated_by_user_name"));
		hc.setChangeToUserName(result.getString("change_to_user_name"));
		hc.setChangeInitiatedByRole(result.getString("change_initiated_by_role"));
		return hc;
	}

}
