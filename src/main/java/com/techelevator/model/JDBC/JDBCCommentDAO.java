package com.techelevator.model.JDBC;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.Comment;
import com.techelevator.model.DAO.CommentDAO;

@Component
public class JDBCCommentDAO extends JDBCUsersAbstractDAO implements CommentDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	public JDBCCommentDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Comment> getAllComments(){
		List<Comment> commentList = new ArrayList<Comment>();
		String sql = "SELECT * FROM ad_comments";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		while(result.next()) {
			Comment comment = mapToRow(result);
			commentList.add(comment);
		}
		return commentList;
	}
	
	@Override
	public List<Comment> getCommentsByAdId(int adId) {
		List<Comment> comments = new ArrayList<Comment>();
		String sql = "SELECT * FROM ad_comments "+
					 "WHERE ad_id = ? AND approved = 'true'";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, adId);
		while(result.next()) {
			Comment comment = mapToRow(result);
			comments.add(comment);
		}
		return comments;
	}

	@Override
	public List<Comment> getCommentsByKidId(int kidId) {
		List<Comment> comments = new ArrayList<Comment>();
		String sql = "SELECT * FROM ad_comments "+
					 "WHERE commenter_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidId);
		while(result.next()) {
			Comment comment = mapToRow(result);
			comments.add(comment);
		}
		return comments;
	}
	
	@Override
	public List<Comment> getApprovedCommentsByKidId(int kidId) {
		List<Comment> approvedComments = new ArrayList<Comment>();
		String sql = "SELECT * FROM ad_comments " +
					 "WHERE commenter_id = ? AND approved = true";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidId);
		while(result.next()) {
			Comment comment = mapToRow(result);
			approvedComments.add(comment);
		}
		return approvedComments;
	}

	@Override
	public List<Comment> getUnapprovedCommentsByKidId(int kidId) {
		List<Comment> unapprovedComments = new ArrayList<Comment>();
		String sql = "SELECT * FROM ad_comments " +
					 "WHERE commenter_id = ? AND approved = false";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql, kidId);
		while(result.next()) {
			Comment comment = mapToRow(result);
			unapprovedComments.add(comment);
		}
		return unapprovedComments;
	}

	@Override
	public void deleteCommentById(int commentId) {
		String sql = "DELETE FROM ad_comments WHERE comment_id = ?";
		jdbcTemplate.update(sql, commentId);
	}

	@Override
	public void insertComment(Comment comment) {
		String sql = "INSERT INTO ad_comments (ad_id, commenter_id, comment, contact_email, approved) " +
					 "VALUES (?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, comment.getAdId(), comment.getCommenterId(), comment.getComment(), comment.getContactEmail(), comment.isApproved());
	}
	
	@Override
	public void approveComment(int commentId) {
		String sql = "UPDATE ad_comments SET approved = true " +
					 "WHERE comment_id = ?";
		jdbcTemplate.update(sql, commentId);
	}
	
	private Comment mapToRow(SqlRowSet result) {
		Comment comment = new Comment();
		comment.setCommentId(result.getInt("comment_id"));
		comment.setAdId(result.getInt("ad_id"));
		comment.setCommenterId(result.getInt("commenter_id"));
		comment.setComment(result.getString("comment"));
		comment.setContactEmail(result.getString("contact_email"));
		comment.setApproved(result.getBoolean("approved"));
		comment.setCreateDate(result.getTimestamp("create_date"));
		return comment;
	}

}
