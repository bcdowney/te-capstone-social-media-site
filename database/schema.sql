-- *************************************************************************************************
-- This script creates all of the database objects (tables, sequences, etc) for the database
-- *************************************************************************************************

BEGIN TRANSACTION;

SET timezone = 'America/New_York';

DROP TABLE IF EXISTS campaigns;
DROP TABLE IF EXISTS transactions;
DROP TABLE IF EXISTS historical_changes;
DROP TABLE IF EXISTS kid_users_pending_changes;
DROP TABLE IF EXISTS guardians_kids;
DROP TABLE IF EXISTS kids_skills;
DROP TABLE IF EXISTS ad_comments;
DROP TABLE IF EXISTS ads;
DROP TABLE IF EXISTS kid_users;
DROP TABLE IF EXISTS guardian_users;
DROP TABLE IF EXISTS admin_users;
DROP TABLE IF EXISTS disabled_users;
DROP TABLE IF EXISTS app_user;
DROP TABLE IF EXISTS skills;

DROP SEQUENCE IF EXISTS seq_guardian_id;
DROP SEQUENCE IF EXISTS seq_ad_comment_id;
DROP SEQUENCE IF EXISTS seq_ad_id;
DROP SEQUENCE IF EXISTS seq_kid_id;
DROP SEQUENCE IF EXISTS seq_user_id;
DROP SEQUENCE IF EXISTS seq_skill_id;
DROP SEQUENCE IF EXISTS seq_pending_kid_request_id;
DROP SEQUENCE IF EXISTS seq_history_id;
DROP SEQUENCE IF EXISTS seq_transaction_id;
DROP SEQUENCE IF EXISTS seq_campaign_id;

CREATE SEQUENCE seq_guardian_id;
CREATE SEQUENCE seq_kid_id;
CREATE SEQUENCE seq_ad_id;
CREATE SEQUENCE seq_ad_comment_id;
CREATE SEQUENCE seq_user_id;
CREATE SEQUENCE seq_skill_id;
CREATE SEQUENCE seq_pending_kid_request_id;
CREATE SEQUENCE seq_history_id;
CREATE SEQUENCE seq_transaction_id;
CREATE SEQUENCE seq_campaign_id;

CREATE TABLE app_user (
        id integer PRIMARY KEY DEFAULT nextval('seq_user_id'),
        user_name varchar(32) NOT NULL UNIQUE,
        password varchar(32) NOT NULL,
        role varchar(32),
        salt varchar(255) NOT NULL,
        create_date timestamptz NOT NULL default Now(),
        last_modified timestamptz NOT NULL default Now()
);

--Writing the table like this necesitates that any disabled users remain on app_user and their
--other respective table. This means this table has to be checked every time in order for
--disabling or suspending accounts to work.
CREATE TABLE disabled_users (
        user_id integer NOT NULL,
        date_disabled date NOT NULL default Now(),
        suspended boolean,
        suspension_ends date,
        CONSTRAINT fk_disabled_user_id FOREIGN KEY (user_id) references app_user(id)
);

CREATE table admin_users(
        user_id integer NOT NULL,
        email varchar(320) NOT NULL,
        created timestamptz NOT NULL default Now(),
        last_modify_date timestamptz NOT NULL default Now(),
        CONSTRAINT pk_admin_users_user_id PRIMARY KEY (user_id),
        CONSTRAINT fk_app_users_user_id FOREIGN KEY (user_id) references app_user(id)
);

CREATE TABLE guardian_users (
        user_id integer NOT NULL,
        guardian_id integer DEFAULT nextval('seq_guardian_id'),
        first_name varchar(20) NOT NULL,
        last_name varchar(30) NOT NULL,
        date_of_birth date NOT NULL,
        email varchar(320) NOT NULL,
        created timestamptz NOT NULL default Now(),
        last_modified timestamptz NOT NULL default Now(),
        CONSTRAINT pk_guardian_users_guardian_id PRIMARY KEY (guardian_id),
        CONSTRAINT fk_guardian_users_user_id FOREIGN KEY (user_id) references app_user(id)
);

CREATE TABLE kid_users (
        user_id integer NOT NULL,
        kid_id integer DEFAULT nextval('seq_kid_id'),
        guardian_id integer NOT NULL,
        first_name varchar(20) NOT NULL,
        last_name varchar(30) NOT NULL,
        date_of_birth date NOT NULL,
        business_name varchar(128) NOT NULL,
        logo_file_name varchar(255),
        summary text,
        email varchar(320) NOT NULL,
        transactions_per_day_limit int,
        transaction_amount_limit decimal(19, 2),
        created timestamptz NOT NULL default Now(),
        last_modified timestamptz NOT NULL default Now(),
        CONSTRAINT pk_kid_users_kid_id PRIMARY KEY (kid_id),
        CONSTRAINT fk_kid_users_user_id FOREIGN KEY (user_id) references app_user(id),
        CONSTRAINT fk_kid_users_guardian_id FOREIGN KEY (guardian_id) references guardian_users(guardian_id)
);

CREATE TABLE kid_users_pending_changes (
        pending_request_id integer DEFAULT nextval('seq_pending_kid_request_id'),
        user_id integer NOT NULL,
        kid_id integer NOT NULL,
        guardian_id integer NOT NULL,
        changed_column varchar(50) NOT NULL,
        current_value text NOT NULL,
        requested_value text NOT NULL,
        approved boolean NOT NULL,
        created timestamptz NOT NULL default Now(),
        CONSTRAINT pk_kid_users_pending_changes_pending_request_id PRIMARY KEY (pending_request_id),
        CONSTRAINT fk_kid_users_pending_changes_kid_id FOREIGN KEY (kid_id) references kid_users(kid_id),
        CONSTRAINT fk_kid_users_pending_changes_user_id FOREIGN KEY (user_id) references app_user(id),
        CONSTRAINT fk_kid_users_pending_changes_guardian_id FOREIGN KEY (guardian_id) references guardian_users(guardian_id)
);       

CREATE TABLE guardians_kids (
        kid_id integer NOT NULL,
        guardian_id integer NOT NULL,
        CONSTRAINT pk_guardians_kids_kid_id_guardian_id PRIMARY KEY (kid_id, guardian_id),
        CONSTRAINT fk_guardians_kids_kid_id FOREIGN KEY (kid_id) references kid_users(kid_id),
        CONSTRAINT fk_guardians_kids_guardian_id FOREIGN KEY (guardian_id) references guardian_users(guardian_id)
);

CREATE TABLE skills (
        skill_id integer DEFAULT nextval('seq_skill_id'),
        name varchar(200) NOT NULL,
        created timestamptz NOT NULL default Now(),
        last_modified timestamptz NOT NULL default Now(),
        CONSTRAINT pk_skills_skill_id PRIMARY KEY (skill_id)
);

CREATE TABLE kids_skills (
        kid_id integer NOT NULL,
        skill_id integer NOT NULL,
        created timestamptz NOT NULL default Now(),
        CONSTRAINT pk_kids_skills_kid_id_skill_id PRIMARY KEY (kid_id, skill_id),
        CONSTRAINT fk_kids_skills_kid_id FOREIGN KEY (kid_id) references kid_users(kid_id),
        CONSTRAINT fk_kids_skills_skill_id FOREIGN KEY (skill_id) references skills(skill_id)
);

CREATE TABLE historical_changes (
        id integer DEFAULT nextval('seq_history_id'),
        change_initiated_by_id integer NOT NULL,
        change_to_user_id integer NOT NULL,
        changed_column varchar(50) NOT NULL,
        old_val text,
        new_val text NOT NULL,
        request_time timestamptz NOT NULL,
        complete_time timestamptz NOT NULL default Now(),
        CONSTRAINT pk_historical_changes_id PRIMARY KEY (id),
        CONSTRAINT fk_historical_changes_change_initiated_by_id FOREIGN KEY (change_initiated_by_id) references app_user(id),
        CONSTRAINT fk_historical_changes_change_to_user_id FOREIGN KEY (change_to_user_id) references app_user(id) 
);

CREATE TABLE ads (
        ad_id integer DEFAULT nextval('seq_ad_id'),
        kid_id integer NOT NULL,
        --ad_type ad NOT NULL,
        ad_type varchar(10) NOT NULL,
        title varchar(60) NOT NULL,
        description text NOT NULL,
        start_date date NOT NULL DEFAULT Now(),
        start_time time,
        end_date date,
        end_time time,
        created timestamptz NOT NULL default Now(),
        pay_dollars decimal(19,2),
        barter_desc varchar(255),
        --barter boolean NOT NULL,
        approved boolean NOT NULL DEFAULT false,
        CONSTRAINT pk_ads_ad_id PRIMARY KEY (ad_id),
        CONSTRAINT fk_ads_kid_id FOREIGN KEY (kid_id) references kid_users(kid_id)
);

CREATE TABLE ad_comments (
        comment_id integer DEFAULT nextval('seq_ad_comment_id'),
        ad_id integer NOT NULL,
        commenter_id integer NOT NULL,
        comment text NOT NULL,
        contact_email varchar(320) NOT NULL,
        approved boolean NOT NULL DEFAULT false,
        create_date timestamptz NOT NULL default Now(),
        CONSTRAINT pk_ad_comments_comment_id PRIMARY KEY (comment_id),
        CONSTRAINT fk_ad_comments_commenter_id FOREIGN KEY (commenter_id) references kid_users(kid_id)
);  

CREATE TABLE transactions (
        transaction_id integer DEFAULT nextval('seq_transaction_id'),
        from_kid_id integer NOT NULL,
        to_kid_id integer NOT NULL,
        amount decimal(19, 2) NOT NULL,
        transaction_type varchar(10) NOT NULL,
        approved boolean NOT NULL DEFAULT false,
        transaction_timestamp timestamptz NOT NULL default Now(),
        CONSTRAINT pk_transactions_transaction_id PRIMARY KEY (transaction_id),
        CONSTRAINT fk_transactions_from_kid_id FOREIGN KEY (from_kid_id) references kid_users(kid_id),
        CONSTRAINT fk_transactions_to_kid_id FOREIGN KEY (to_kid_id) references kid_users(kid_id)
);

CREATE TABLE campaigns (     
        campaign_id integer DEFAULT nextval('seq_campaign_id'),
        kid_id integer NOT NULL,
        title varchar NOT NULL,
        description text,
        current_amount decimal(19, 2) NOT NULL,
        goal decimal(19, 2) NOT NULL,
        approved boolean NOT NULL DEFAULT false,
        create_date timestamptz NOT NULL default Now(),
        CONSTRAINT pk_campaigns_campaign_id PRIMARY KEY (campaign_id),
        CONSTRAINT fk_campaigns_kid_id FOREIGN KEY (kid_id) references kid_users(kid_id)
);
        
COMMIT;