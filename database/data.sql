-- *****************************************************************************
-- This script contains INSERT statements for populating tables with seed data
-- *****************************************************************************

BEGIN TRANSACTION;

--***DELETES ALL TABLE DATA***---
DELETE FROM kids_skills;
DELETE FROM skills;
DELETE FROM disabled_users;
DELETE FROM guardians_kids;
DELETE FROM kid_users_pending_changes;
DELETE FROM ad_comments;
DELETE FROM ads;
DELETE FROM campaigns;
DELETE FROM kid_users;
DELETE FROM guardian_users;
DELETE FROM admin_users;
DELETE FROM historical_changes;
DELETE FROM app_user;
--***DELETES ALL TABLE DATA***---

INSERT INTO app_user VALUES (1, 'minecraftkid', 'password123', 'child', 'saltplease');
INSERT INTO app_user VALUES (2, 'minecraftmom', 'password321', 'guardian', 'saltplease');
INSERT INTO app_user VALUES (3, 'ADMINTEST', 'strongpassword', 'admin', 'saltplease');
INSERT INTO app_user VALUES (4, 'clubpenguinhomeschooler', 'anotherpassword', 'child', 'saltplease');
INSERT INTO app_user VALUES (5, 'Captainguardian', '123password', 'guardian', 'saltplease');
INSERT INTO app_user VALUES (6, 'troublekid', 'Password123!', 'child', 'saltplease');
ALTER SEQUENCE seq_user_id RESTART WITH 7;

INSERT INTO guardian_users VALUES (2, 1, 'Helga', 'Toothman', to_date('12-12-1970','MM-DD-YYYY'), 'helgatooth70@example.com');
INSERT INTO guardian_users VALUES (5, 2, 'William', 'Nye', to_date('11-12-1970','MM-DD-YYYY'), 'WillNye@example.com');
ALTER SEQUENCE seq_guardian_id RESTART WITH 3;

INSERT INTO kid_users VALUES (1, 1, 1, 'Timmy', 'Toothman', to_date('12-12-2005','MM-DD-YYYY'), 'Toothman Family Industries', '/Users/mclapper/Server/uploaded_logos/logo-default.png', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 'ttminecraft@example.com'); 
INSERT INTO kid_users VALUES (4, 2, 2, 'Billy', 'Nye', to_date('11-12-2005','MM-DD-YYYY'), 'Billy Nye, Science Consultant', '/Users/mclapper/Server/uploaded_logos/logo-default.png', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 'billypenguin@example.com'); 
ALTER SEQUENCE seq_kid_id RESTART WITH 3;

INSERT INTO guardians_kids VALUES (1, 1);
INSERT INTO guardians_kids VALUES (2, 2);

INSERT INTO admin_users VALUES (3, 'admin@example.com');

INSERT INTO kid_users_pending_changes VALUES (1, 1, 1, 1, 'email', 'ttminecraft@example.com', 'ttminecraftEXPERT@example.com', false);
INSERT INTO kid_users_pending_changes VALUES (2, 1, 1, 1, 'summary', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 'I will make your logo look great!', false);
INSERT INTO kid_users_pending_changes VALUES (3, 4, 2, 2, 'logo_file_name', 'square-logo-no-tagline.png', 'greatlogo1final_doNOTCHANGE.png', false);
INSERT INTO kid_users_pending_changes VALUES (4, 4, 2, 2, 'first_name', 'Billy', 'William', false);
ALTER SEQUENCE seq_pending_kid_request_id RESTART WITH 5;

INSERT INTO historical_changes VALUES (1, 3, 2, 'first_name', 'Brunhelga', 'Helga', '2018-10-11 16:16:07');
INSERT INTO historical_changes VALUES (2, 1, 1, 'first_name', 'Timothy', 'Timmy', '2018-10-11 16:28:52');
ALTER SEQUENCE seq_history_id RESTART WITH 3; 

INSERT INTO disabled_users VALUES (6, default, true, '2020-01-01');

INSERT INTO ads VALUES(1, 1, 'job', 'Minecraft help!', 'Hi, I am Johnny from  Shark Nado labs, I am looking for someone to teach me how to get to the 3rd level in Minecraft', '2018-10-31', '1400', '2018-10-31', '1800', default, null, 'I am willing to barter a lesson in :xyz', true);
INSERT INTO ads VALUES(2, 1, 'skill', 'Teach me mindcraft building!', 'PLACEHOLDER|DESCRIPTION', default, NULL, NULL, NULL, default, 2.25, NULL, default);
INSERT INTO ads VALUES(3, 2, 'skill', 'Explain [MATH CONCEPT], please!', 'PLACEHOLDER|DESCRIPTION', '2018-12-01', '1330', '2019-01-16', NULL, DEFAULT, 15, NULL, TRUE);
ALTER SEQUENCE seq_ad_id RESTART WITH 4;

INSERT INTO ad_comments VALUES(1, 1, 2, 'THIS IS A COMMENT!', (SELECT email FROM kid_users WHERE kid_id = 2), TRUE, default);
INSERT INTO ad_comments VALUES(2, 1, 2, 'THIS IS A COMMENT!', (SELECT email FROM kid_users WHERE kid_id = 2), TRUE, default);
ALTER SEQUENCE seq_ad_comment_id RESTART WITH 3;

INSERT INTO campaigns VALUES (1, 1, 'Buy a new calculator', 'I want to improve my skills at Maths and need a new calcium calculator to do this!', 18.00, 50.00, TRUE, default);
ALTER SEQUENCE seq_campaign_id RESTART WITH 2;

INSERT INTO skills VALUES(default, 'Zip, Button, Snap clothing');
INSERT INTO skills VALUES(default, 'Wash face and hands');
INSERT INTO skills VALUES(default, 'Comb or brush own hair');
INSERT INTO skills VALUES(default, 'Brush teeth'); 
INSERT INTO skills VALUES(default, 'Put on socks');
INSERT INTO skills VALUES(default, 'Tie your shoes');
INSERT INTO skills VALUES(default, 'Cut your nails');
INSERT INTO skills VALUES(default, 'Put on a tie');
INSERT INTO skills VALUES(default, 'Put on a belt');
INSERT INTO skills VALUES(default, 'Set the table/Clean off table');
INSERT INTO skills VALUES(default, 'Make the bed');
INSERT INTO skills VALUES(default, 'Do laundry');
INSERT INTO skills VALUES(default, 'Wash and dry dishes by hand');
INSERT INTO skills VALUES(default, 'Load/Unload the dishwasher');
INSERT INTO skills VALUES(default, 'Vacuum');
INSERT INTO skills VALUES(default, 'Mop');
INSERT INTO skills VALUES(default, 'Dust');
INSERT INTO skills VALUES(default, 'Take out the trash');
INSERT INTO skills VALUES(default, 'Feed the pet');
INSERT INTO skills VALUES(default, 'Wash out plastic trash cans');
INSERT INTO skills VALUES(default, 'Rake leaves');
INSERT INTO skills VALUES(default, 'Clean mirrors');
INSERT INTO skills VALUES(default, 'Clean windows');
INSERT INTO skills VALUES(default, 'Empty kitchen trash');
INSERT INTO skills VALUES(default, 'Clean pet cages and food bowls');
INSERT INTO skills VALUES(default, 'Use a broom and dustpan');
INSERT INTO skills VALUES(default, 'Sweep porches, decks, driveways and walkways');
INSERT INTO skills VALUES(default, 'Take a written phone message');
INSERT INTO skills VALUES(default, 'Water outside plants, flowers and garden');
INSERT INTO skills VALUES(default, 'Fold clothes neatly');
INSERT INTO skills VALUES(default, 'Fold blankets neatly');
INSERT INTO skills VALUES(default, 'Do simple mending and sew on buttons');
INSERT INTO skills VALUES(default, 'Wash the car');
INSERT INTO skills VALUES(default, 'Check and fill all car fluids');
INSERT INTO skills VALUES(default, 'Make a sandwich');
INSERT INTO skills VALUES(default, 'Pour milk into cereal');
INSERT INTO skills VALUES(default, 'Pour milk or juice into a cup');
INSERT INTO skills VALUES(default, 'Make macaroni');
INSERT INTO skills VALUES(default, 'Make a milkshake');
INSERT INTO skills VALUES(default, 'Make soup from a can');
INSERT INTO skills VALUES(default, 'Make grilled cheese sandwich');
INSERT INTO skills VALUES(default, 'Measure using a measuring cup');
INSERT INTO skills VALUES(default, 'Pack own sack lunch');
INSERT INTO skills VALUES(default, 'Understand ingredient and nutrient labeling');
INSERT INTO skills VALUES(default, 'Plan a balanced meal');
INSERT INTO skills VALUES(default, 'Know how to select and prepare fruits and vegetables');
INSERT INTO skills VALUES(default, 'Roller skate');
INSERT INTO skills VALUES(default, 'Jump rope');
INSERT INTO skills VALUES(default, 'Ride a bike');
INSERT INTO skills VALUES(default, 'Shoot a basketball');
INSERT INTO skills VALUES(default, 'Throw a ball');
INSERT INTO skills VALUES(default, 'Kick a ball');
INSERT INTO skills VALUES(default, 'Yoyo tricks');
INSERT INTO skills VALUES(default, 'Throw a Frisbee');
INSERT INTO skills VALUES(default, 'Hula hoop');
INSERT INTO skills VALUES(default, 'Repair bicycle tire and learn basic adjustments');
INSERT INTO skills VALUES(default, 'Put air into a ball');
INSERT INTO skills VALUES(default, 'Know a variety of knots');
INSERT INTO skills VALUES(default, 'Time Management (should be able to manage an entire day of activities/assignments)');
INSERT INTO skills VALUES(default, 'Make deposits and withdrawals at the bank');
INSERT INTO skills VALUES(default, 'Money Management: Know how to  Budget, Charitable Giving, Spending' );
INSERT INTO skills VALUES(default, 'Type with proficiency with 2 hands');
INSERT INTO skills VALUES(default, 'Type with proficiency using 1 handed typing method');
INSERT INTO skills VALUES(default, 'Write checks and balance a checkbook');
INSERT INTO skills VALUES(default, 'Fill out a job application');
INSERT INTO skills VALUES(default, 'Prepare a resume');
INSERT INTO skills VALUES(default, 'Compare quality and prices (unit pricing)');
INSERT INTO skills VALUES(default, 'Write a Business plan');
INSERT INTO skills VALUES(default, 'Coding');
INSERT INTO skills VALUES(default, 'How to write a Tech Reviews');
INSERT INTO skills VALUES(default, 'Excel spreadsheets');
INSERT INTO skills VALUES(default, 'Blogging');
INSERT INTO skills VALUES(default, 'Data analytics');
INSERT INTO skills VALUES(default, 'How to use an iPhone');
INSERT INTO skills VALUES(default, 'How to use an Android phone');
INSERT INTO skills VALUES(default, 'Social media : Instagram, Snapchat, Facebook, Twitter');
INSERT INTO skills VALUES(default, 'Marketing');
INSERT INTO skills VALUES(default, 'Videography');
INSERT INTO skills VALUES(default, 'Digital Photography and editing');
INSERT INTO skills VALUES(default, 'Graphic Design');
INSERT INTO skills VALUES(default, 'Product development');
INSERT INTO skills VALUES(default, 'Able to teach gaming skills ( how to get to next level of specific games)');
INSERT INTO skills VALUES(default, 'Know basic emergency first-aid procedures: CPR');
INSERT INTO skills VALUES(default, 'Take your temperature with a thermometer');
INSERT INTO skills VALUES(default, 'Academics: Able to teach');
INSERT INTO skills VALUES(default, 'English');
INSERT INTO skills VALUES(default, 'Spanish');
INSERT INTO skills VALUES(default, 'German');
INSERT INTO skills VALUES(default, 'Latin');
INSERT INTO skills VALUES(default, 'Russian');
INSERT INTO skills VALUES(default, 'Chinese');
INSERT INTO skills VALUES(default, 'Japanese');
INSERT INTO skills VALUES(default, 'Sign Language');
INSERT INTO skills VALUES(default, 'Math : Addition, subtraction, decimals , fractions');
INSERT INTO skills VALUES(default, 'Algebra');
INSERT INTO skills VALUES(default, 'Geometry');
INSERT INTO skills VALUES(default, 'Calculus');
INSERT INTO skills VALUES(default, 'Trigonometry');
INSERT INTO skills VALUES(default, 'Biology');
INSERT INTO skills VALUES(default, 'Chemistry');
INSERT INTO skills VALUES(default, 'Physics');
INSERT INTO skills VALUES(default, 'Entrepreneurship/leadership');
INSERT INTO skills VALUES(default, 'Painting and Drawing');
INSERT INTO skills VALUES(default, 'Sculpture');
INSERT INTO skills VALUES(default, 'Guitar');
INSERT INTO skills VALUES(default, 'Violin');
INSERT INTO skills VALUES(default, 'Piano');
INSERT INTO skills VALUES(default, 'Flute');
INSERT INTO skills VALUES(default, 'Clarinet');
INSERT INTO skills VALUES(default, 'Drums');
INSERT INTO skills VALUES(default, 'Tuba');
INSERT INTO skills VALUES(default, 'Saxophone');
INSERT INTO skills VALUES(default, 'Voice lessons');
ALTER SEQUENCE seq_skill_id RESTART WITH 114;

INSERT INTO kids_skills VALUES (1, 4);
INSERT INTO kids_skills VALUES (1, 2);
INSERT INTO kids_skills VALUES (1, 5);
INSERT INTO kids_skills VALUES (2, 1);
INSERT INTO kids_skills VALUES (2, 3);
INSERT INTO kids_skills VALUES (2, 2);

COMMIT;
--ROLLBACK;
